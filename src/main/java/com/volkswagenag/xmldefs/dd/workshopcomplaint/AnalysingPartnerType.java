
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;
import com.volkswagenag.xmldefs.dd.businesspartner.PartnerIdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für AnalysingPartnerType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AnalysingPartnerType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}BusinessPartnerRef" minOccurs="0"/>
 *         &lt;element name="AddressIDRef" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnalysingPartnerType", propOrder = {
    "businessPartnerRef",
    "addressIDRef"
})
public class AnalysingPartnerType {

    @XmlElement(name = "BusinessPartnerRef", namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner")
    protected PartnerIdentifierType businessPartnerRef;
    @XmlElement(name = "AddressIDRef")
    protected IdentifierType addressIDRef;

    /**
     * Ruft den Wert der businessPartnerRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public PartnerIdentifierType getBusinessPartnerRef() {
        return businessPartnerRef;
    }

    /**
     * Legt den Wert der businessPartnerRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public void setBusinessPartnerRef(PartnerIdentifierType value) {
        this.businessPartnerRef = value;
    }

    /**
     * Ruft den Wert der addressIDRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getAddressIDRef() {
        return addressIDRef;
    }

    /**
     * Legt den Wert der addressIDRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setAddressIDRef(IdentifierType value) {
        this.addressIDRef = value;
    }

}
