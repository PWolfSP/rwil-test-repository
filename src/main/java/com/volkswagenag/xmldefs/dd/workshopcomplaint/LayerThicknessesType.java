
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * List of layer thicknesses related to request
 * 
 * <p>Java-Klasse für LayerThicknessesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LayerThicknessesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LayerThickness" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}LayerThicknessType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LayerThicknessesType", propOrder = {
    "layerThickness"
})



public class LayerThicknessesType {
	
	
    @XmlElement(name = "LayerThickness", required = true)
    private List<LayerThicknessType> layerThickness;


		public void setLayerThicknessType(List<LayerThicknessType> layerThickness) {
			this.layerThickness = layerThickness;
		}
		
	    public List<LayerThicknessType> getLayerThickness() {
	        if (layerThickness == null) {
	            layerThickness = new ArrayList<LayerThicknessType>();
	        }
	        return this.layerThickness;
	    }

}
