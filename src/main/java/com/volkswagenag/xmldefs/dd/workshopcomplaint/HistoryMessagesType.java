
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für HistoryMessagesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="HistoryMessagesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HistoryMessage" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}HistoryMessageType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HistoryMessagesType", propOrder = {
    "historyMessage"
})



public class HistoryMessagesType {
	
	
    @XmlElement(name = "HistoryMessage", required = true)
    private List<HistoryMessageType> historyMessage;


		public void setHistoryMessageType(List<HistoryMessageType> historyMessage) {
			this.historyMessage = historyMessage;
		}
		
	    public List<HistoryMessageType> getHistoryMessage() {
	        if (historyMessage == null) {
	            historyMessage = new ArrayList<HistoryMessageType>();
	        }
	        return this.historyMessage;
	    }

}
