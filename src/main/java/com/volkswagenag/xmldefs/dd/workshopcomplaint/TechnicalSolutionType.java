
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;
import com.volkswagenag.xmldefs.dd.basictypes.NameType;

import lombok.Generated;


/**
 * Technical solution for the complaint.
 * 
 * <p>Java-Klasse für TechnicalSolutionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TechnicalSolutionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SolutionID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="SolutionTitle" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NameType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TechnicalSolutionType", propOrder = {
    "solutionID",
    "solutionTitle"
})
public class TechnicalSolutionType {

    @XmlElement(name = "SolutionID")
    protected IdentifierType solutionID;
    @XmlElement(name = "SolutionTitle")
    protected NameType solutionTitle;

    /**
     * Ruft den Wert der solutionID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getSolutionID() {
        return solutionID;
    }

    /**
     * Legt den Wert der solutionID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setSolutionID(IdentifierType value) {
        this.solutionID = value;
    }

    /**
     * Ruft den Wert der solutionTitle-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NameType }
     *     
     */
    public NameType getSolutionTitle() {
        return solutionTitle;
    }

    /**
     * Legt den Wert der solutionTitle-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NameType }
     *     
     */
    public void setSolutionTitle(NameType value) {
        this.solutionTitle = value;
    }

}
