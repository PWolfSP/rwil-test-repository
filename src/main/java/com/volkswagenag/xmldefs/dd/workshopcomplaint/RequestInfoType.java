
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse f�r RequestInfoType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RequestInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FeedbackRequiredIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="ReplyType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="DamageType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="DamageDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="TeleDiagnosisIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="SelfDiagnosisIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="GuidedFaultDiagnosticsIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="MotorInjectionLightOnIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="SeriesProductionStatusIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="PriorDamageIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="PriorDamageDescription" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestInfoType", propOrder = {
    "feedbackRequiredIND",
    "replyType",
    "damageType",
    "damageDate",
    "teleDiagnosisIND",
    "selfDiagnosisIND",
    "guidedFaultDiagnosticsIND",
    "motorInjectionLightOnIND",
    "seriesProductionStatusIND",
    "priorDamageIND",
    "priorDamageDescription"
})
public class RequestInfoType {

    @XmlElement(name = "FeedbackRequiredIND")
    protected Boolean feedbackRequiredIND;
    @XmlElement(name = "ReplyType")
    protected CodeType replyType;
    @XmlElement(name = "DamageType")
    protected CodeType damageType;
    @XmlElement(name = "DamageDate")
    protected String damageDate;
    @XmlElement(name = "TeleDiagnosisIND")
    protected Boolean teleDiagnosisIND;
    @XmlElement(name = "SelfDiagnosisIND")
    protected Boolean selfDiagnosisIND;
    @XmlElement(name = "GuidedFaultDiagnosticsIND")
    protected Boolean guidedFaultDiagnosticsIND;
    @XmlElement(name = "MotorInjectionLightOnIND")
    protected Boolean motorInjectionLightOnIND;
    @XmlElement(name = "SeriesProductionStatusIND")
    protected Boolean seriesProductionStatusIND;
    @XmlElement(name = "PriorDamageIND")
    protected Boolean priorDamageIND;
    @XmlElement(name = "PriorDamageDescription")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String priorDamageDescription;

    /**
     * Ruft den Wert der feedbackRequiredIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFeedbackRequiredIND() {
        return feedbackRequiredIND;
    }

    /**
     * Legt den Wert der feedbackRequiredIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFeedbackRequiredIND(Boolean value) {
        this.feedbackRequiredIND = value;
    }

    /**
     * Ruft den Wert der replyType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getReplyType() {
        return replyType;
    }

    /**
     * Legt den Wert der replyType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setReplyType(CodeType value) {
        this.replyType = value;
    }

    /**
     * Ruft den Wert der damageType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getDamageType() {
        return damageType;
    }

    /**
     * Legt den Wert der damageType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setDamageType(CodeType value) {
        this.damageType = value;
    }

    /**
     * Ruft den Wert der damageDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDamageDate() {
        return damageDate;
    }

    /**
     * Legt den Wert der damageDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDamageDate(String value) {
        this.damageDate = value;
    }

    /**
     * Ruft den Wert der teleDiagnosisIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTeleDiagnosisIND() {
        return teleDiagnosisIND;
    }

    /**
     * Legt den Wert der teleDiagnosisIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTeleDiagnosisIND(Boolean value) {
        this.teleDiagnosisIND = value;
    }

    /**
     * Ruft den Wert der selfDiagnosisIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSelfDiagnosisIND() {
        return selfDiagnosisIND;
    }

    /**
     * Legt den Wert der selfDiagnosisIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSelfDiagnosisIND(Boolean value) {
        this.selfDiagnosisIND = value;
    }

    /**
     * Ruft den Wert der guidedFaultDiagnosticsIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGuidedFaultDiagnosticsIND() {
        return guidedFaultDiagnosticsIND;
    }

    /**
     * Legt den Wert der guidedFaultDiagnosticsIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGuidedFaultDiagnosticsIND(Boolean value) {
        this.guidedFaultDiagnosticsIND = value;
    }

    /**
     * Ruft den Wert der motorInjectionLightOnIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMotorInjectionLightOnIND() {
        return motorInjectionLightOnIND;
    }

    /**
     * Legt den Wert der motorInjectionLightOnIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMotorInjectionLightOnIND(Boolean value) {
        this.motorInjectionLightOnIND = value;
    }

    /**
     * Ruft den Wert der seriesProductionStatusIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSeriesProductionStatusIND() {
        return seriesProductionStatusIND;
    }

    /**
     * Legt den Wert der seriesProductionStatusIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSeriesProductionStatusIND(Boolean value) {
        this.seriesProductionStatusIND = value;
    }

    /**
     * Ruft den Wert der priorDamageIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPriorDamageIND() {
        return priorDamageIND;
    }

    /**
     * Legt den Wert der priorDamageIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPriorDamageIND(Boolean value) {
        this.priorDamageIND = value;
    }

    /**
     * Ruft den Wert der priorDamageDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriorDamageDescription() {
        return priorDamageDescription;
    }

    /**
     * Legt den Wert der priorDamageDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriorDamageDescription(String value) {
        this.priorDamageDescription = value;
    }

}
