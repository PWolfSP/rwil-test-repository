
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * Describes Transfer relevant data
 * 
 * <p>Java-Klasse für VehicleTransferType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VehicleTransferType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequentialNumber" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *         &lt;element name="OriginDealer" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}OriginDealerType" minOccurs="0"/>
 *         &lt;element name="DestinationDealer" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}DestinationDealerType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleTransferType", propOrder = {
    "sequentialNumber",
    "originDealer",
    "destinationDealer"
})
public class VehicleTransferType {

    @XmlElement(name = "SequentialNumber")
    protected BigDecimal sequentialNumber;
    @XmlElement(name = "OriginDealer")
    protected OriginDealerType originDealer;
    @XmlElement(name = "DestinationDealer")
    protected DestinationDealerType destinationDealer;

    /**
     * Ruft den Wert der sequentialNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSequentialNumber() {
        return sequentialNumber;
    }

    /**
     * Legt den Wert der sequentialNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSequentialNumber(BigDecimal value) {
        this.sequentialNumber = value;
    }

    /**
     * Ruft den Wert der originDealer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OriginDealerType }
     *     
     */
    public OriginDealerType getOriginDealer() {
        return originDealer;
    }

    /**
     * Legt den Wert der originDealer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginDealerType }
     *     
     */
    public void setOriginDealer(OriginDealerType value) {
        this.originDealer = value;
    }

    /**
     * Ruft den Wert der destinationDealer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DestinationDealerType }
     *     
     */
    public DestinationDealerType getDestinationDealer() {
        return destinationDealer;
    }

    /**
     * Legt den Wert der destinationDealer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DestinationDealerType }
     *     
     */
    public void setDestinationDealer(DestinationDealerType value) {
        this.destinationDealer = value;
    }

}
