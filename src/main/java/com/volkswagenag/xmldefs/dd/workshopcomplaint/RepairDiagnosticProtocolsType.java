
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.repairdiagnosticprotocol.RepairDiagnosticProtocolIdentifierType;

import lombok.Generated;


/**
 * List of diagnostic protocols references related to complaint
 * 
 * <p>Java-Klasse für RepairDiagnosticProtocolsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RepairDiagnosticProtocolsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/RepairDiagnosticProtocol}RepairDiagnosticProtocolRef" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RepairDiagnosticProtocolsType", propOrder = {
    "repairDiagnosticProtocolRef"
})



public class RepairDiagnosticProtocolsType {
	
	
    @XmlElement(name = "RepairDiagnosticProtocolRef", namespace = "http://xmldefs.volkswagenag.com/DD/RepairDiagnosticProtocol", required = true)
    private List<RepairDiagnosticProtocolIdentifierType> repairDiagnosticProtocolRef;


		public void setRepairDiagnosticProtocolIdentifierType(List<RepairDiagnosticProtocolIdentifierType> repairDiagnosticProtocolRef) {
			this.repairDiagnosticProtocolRef = repairDiagnosticProtocolRef;
		}
		
	    public List<RepairDiagnosticProtocolIdentifierType> getRepairDiagnosticProtocolRef() {
	        if (repairDiagnosticProtocolRef == null) {
	            repairDiagnosticProtocolRef = new ArrayList<RepairDiagnosticProtocolIdentifierType>();
	        }
	        return this.repairDiagnosticProtocolRef;
	    }

}
