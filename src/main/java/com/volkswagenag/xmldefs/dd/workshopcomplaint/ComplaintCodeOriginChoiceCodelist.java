
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ComplaintCodeOriginChoiceCodelist.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ComplaintCodeOriginChoiceCodelist">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EFA"/>
 *     &lt;enumeration value="DISS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@Generated
@XmlType(name = "ComplaintCodeOriginChoiceCodelist")
@XmlEnum
public enum ComplaintCodeOriginChoiceCodelist {

    EFA,
    DISS;

    public String value() {
        return name();
    }

    public static ComplaintCodeOriginChoiceCodelist fromValue(String v) {
        return valueOf(v);
    }

}
