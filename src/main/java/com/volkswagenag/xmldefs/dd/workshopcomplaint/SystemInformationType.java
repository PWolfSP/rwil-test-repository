
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für SystemInformationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SystemInformationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FieldMonitoringID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="RepairRetriedIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="WarrantyIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemInformationType", propOrder = {
    "fieldMonitoringID",
    "repairRetriedIND",
    "warrantyIND"
})
public class SystemInformationType {

    @XmlElement(name = "FieldMonitoringID")
    protected IdentifierType fieldMonitoringID;
    @XmlElement(name = "RepairRetriedIND")
    protected Boolean repairRetriedIND;
    @XmlElement(name = "WarrantyIND")
    protected Boolean warrantyIND;

    /**
     * Ruft den Wert der fieldMonitoringID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getFieldMonitoringID() {
        return fieldMonitoringID;
    }

    /**
     * Legt den Wert der fieldMonitoringID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setFieldMonitoringID(IdentifierType value) {
        this.fieldMonitoringID = value;
    }

    /**
     * Ruft den Wert der repairRetriedIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRepairRetriedIND() {
        return repairRetriedIND;
    }

    /**
     * Legt den Wert der repairRetriedIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRepairRetriedIND(Boolean value) {
        this.repairRetriedIND = value;
    }

    /**
     * Ruft den Wert der warrantyIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWarrantyIND() {
        return warrantyIND;
    }

    /**
     * Legt den Wert der warrantyIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWarrantyIND(Boolean value) {
        this.warrantyIND = value;
    }

}
