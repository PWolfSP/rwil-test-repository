
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für WorkshopInformationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="WorkshopInformationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ComplaintComprehensibleIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="VehicleComparison" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="DamageRepairedIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="OriginalAccessoryIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkshopInformationType", propOrder = {
    "complaintComprehensibleIND",
    "vehicleComparison",
    "damageRepairedIND",
    "originalAccessoryIND"
})
public class WorkshopInformationType {

    @XmlElement(name = "ComplaintComprehensibleIND")
    protected Boolean complaintComprehensibleIND;
    @XmlElement(name = "VehicleComparison")
    protected CodeType vehicleComparison;
    @XmlElement(name = "DamageRepairedIND")
    protected Boolean damageRepairedIND;
    @XmlElement(name = "OriginalAccessoryIND")
    protected Boolean originalAccessoryIND;

    /**
     * Ruft den Wert der complaintComprehensibleIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isComplaintComprehensibleIND() {
        return complaintComprehensibleIND;
    }

    /**
     * Legt den Wert der complaintComprehensibleIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComplaintComprehensibleIND(Boolean value) {
        this.complaintComprehensibleIND = value;
    }

    /**
     * Ruft den Wert der vehicleComparison-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getVehicleComparison() {
        return vehicleComparison;
    }

    /**
     * Legt den Wert der vehicleComparison-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setVehicleComparison(CodeType value) {
        this.vehicleComparison = value;
    }

    /**
     * Ruft den Wert der damageRepairedIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDamageRepairedIND() {
        return damageRepairedIND;
    }

    /**
     * Legt den Wert der damageRepairedIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDamageRepairedIND(Boolean value) {
        this.damageRepairedIND = value;
    }

    /**
     * Ruft den Wert der originalAccessoryIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOriginalAccessoryIND() {
        return originalAccessoryIND;
    }

    /**
     * Legt den Wert der originalAccessoryIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOriginalAccessoryIND(Boolean value) {
        this.originalAccessoryIND = value;
    }

}
