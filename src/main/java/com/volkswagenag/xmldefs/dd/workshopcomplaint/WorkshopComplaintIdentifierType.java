
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für WorkshopComplaintIdentifierType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="WorkshopComplaintIdentifierType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WorkshopComplaintUID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="ApplicationIdentifiers" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}ApplicationIdentifiersType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkshopComplaintIdentifierType", propOrder = {
    "workshopComplaintUID",
    "applicationIdentifiers"
})
public class WorkshopComplaintIdentifierType {

    @XmlElement(name = "WorkshopComplaintUID")
    protected IdentifierType workshopComplaintUID;
    @XmlElement(name = "ApplicationIdentifiers")
    protected ApplicationIdentifiersType applicationIdentifiers;

    /**
     * Ruft den Wert der workshopComplaintUID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getWorkshopComplaintUID() {
        return workshopComplaintUID;
    }

    /**
     * Legt den Wert der workshopComplaintUID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setWorkshopComplaintUID(IdentifierType value) {
        this.workshopComplaintUID = value;
    }

    /**
     * Ruft den Wert der applicationIdentifiers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationIdentifiersType }
     *     
     */
    public ApplicationIdentifiersType getApplicationIdentifiers() {
        return applicationIdentifiers;
    }

    /**
     * Legt den Wert der applicationIdentifiers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationIdentifiersType }
     *     
     */
    public void setApplicationIdentifiers(ApplicationIdentifiersType value) {
        this.applicationIdentifiers = value;
    }

}
