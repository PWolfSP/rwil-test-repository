
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import lombok.Generated;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.volkswagenag.xmldefs.dd.workshopcomplaint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@Generated
@XmlRegistry
public class ObjectFactory {

    private final static QName _WorkshopComplaintRef_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/WorkshopComplaint", "WorkshopComplaintRef");
    private final static QName _WorkshopComplaint_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/WorkshopComplaint", "WorkshopComplaint");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.volkswagenag.xmldefs.dd.workshopcomplaint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WorkshopComplaintType }
     * 
     */
    public WorkshopComplaintType createWorkshopComplaintType() {
        return new WorkshopComplaintType();
    }

    /**
     * Create an instance of {@link WorkshopComplaintIdentifierType }
     * 
     */
    public WorkshopComplaintIdentifierType createWorkshopComplaintIdentifierType() {
        return new WorkshopComplaintIdentifierType();
    }

    /**
     * Create an instance of {@link TimestampsType }
     * 
     */
    public TimestampsType createTimestampsType() {
        return new TimestampsType();
    }

    /**
     * Create an instance of {@link RequestTimestampsType }
     * 
     */
    public RequestTimestampsType createRequestTimestampsType() {
        return new RequestTimestampsType();
    }

    /**
     * Create an instance of {@link WorkshopInformationType }
     * 
     */
    public WorkshopInformationType createWorkshopInformationType() {
        return new WorkshopInformationType();
    }

    /**
     * Create an instance of {@link DISSCodeType }
     * 
     */
    public DISSCodeType createDISSCodeType() {
        return new DISSCodeType();
    }

    /**
     * Create an instance of {@link ComplaintEventType }
     * 
     */
    public ComplaintEventType createComplaintEventType() {
        return new ComplaintEventType();
    }

    /**
     * Create an instance of {@link OldSparePartType }
     * 
     */
    public OldSparePartType createOldSparePartType() {
        return new OldSparePartType();
    }

    /**
     * Create an instance of {@link ComplaintEventsType }
     * 
     */
    public ComplaintEventsType createComplaintEventsType() {
        return new ComplaintEventsType();
    }

    /**
     * Create an instance of {@link HistoryMessageType }
     * 
     */
    public HistoryMessageType createHistoryMessageType() {
        return new HistoryMessageType();
    }

    /**
     * Create an instance of {@link ComplaintRequestType }
     * 
     */
    public ComplaintRequestType createComplaintRequestType() {
        return new ComplaintRequestType();
    }

    /**
     * Create an instance of {@link ClassificationsType }
     * 
     */
    public ClassificationsType createClassificationsType() {
        return new ClassificationsType();
    }

    /**
     * Create an instance of {@link RequestRefusalReasonsType }
     * 
     */
    public RequestRefusalReasonsType createRequestRefusalReasonsType() {
        return new RequestRefusalReasonsType();
    }

    /**
     * Create an instance of {@link RepairDiagnosticProtocolsType }
     * 
     */
    public RepairDiagnosticProtocolsType createRepairDiagnosticProtocolsType() {
        return new RepairDiagnosticProtocolsType();
    }

    /**
     * Create an instance of {@link HistoryMessagesType }
     * 
     */
    public HistoryMessagesType createHistoryMessagesType() {
        return new HistoryMessagesType();
    }

    /**
     * Create an instance of {@link MessageTimestampsType }
     * 
     */
    public MessageTimestampsType createMessageTimestampsType() {
        return new MessageTimestampsType();
    }

    /**
     * Create an instance of {@link OriginDealerType }
     * 
     */
    public OriginDealerType createOriginDealerType() {
        return new OriginDealerType();
    }

    /**
     * Create an instance of {@link DestinationDealerType }
     * 
     */
    public DestinationDealerType createDestinationDealerType() {
        return new DestinationDealerType();
    }

    /**
     * Create an instance of {@link SystemInformationType }
     * 
     */
    public SystemInformationType createSystemInformationType() {
        return new SystemInformationType();
    }

    /**
     * Create an instance of {@link AttachmentType }
     * 
     */
    public AttachmentType createAttachmentType() {
        return new AttachmentType();
    }

    /**
     * Create an instance of {@link ApplicationIdentifiersType }
     * 
     */
    public ApplicationIdentifiersType createApplicationIdentifiersType() {
        return new ApplicationIdentifiersType();
    }

    /**
     * Create an instance of {@link LayerThicknessType }
     * 
     */
    public LayerThicknessType createLayerThicknessType() {
        return new LayerThicknessType();
    }

    /**
     * Create an instance of {@link RequestInfoType }
     * 
     */
    public RequestInfoType createRequestInfoType() {
        return new RequestInfoType();
    }

    /**
     * Create an instance of {@link LayerThicknessesType }
     * 
     */
    public LayerThicknessesType createLayerThicknessesType() {
        return new LayerThicknessesType();
    }

    /**
     * Create an instance of {@link AttachmentsType }
     * 
     */
    public AttachmentsType createAttachmentsType() {
        return new AttachmentsType();
    }

    /**
     * Create an instance of {@link ClassificationType }
     * 
     */
    public ClassificationType createClassificationType() {
        return new ClassificationType();
    }

    /**
     * Create an instance of {@link RefusalReasonType }
     * 
     */
    public RefusalReasonType createRefusalReasonType() {
        return new RefusalReasonType();
    }

    /**
     * Create an instance of {@link UserInfoType }
     * 
     */
    public UserInfoType createUserInfoType() {
        return new UserInfoType();
    }

    /**
     * Create an instance of {@link KeywordsType }
     * 
     */
    public KeywordsType createKeywordsType() {
        return new KeywordsType();
    }

    /**
     * Create an instance of {@link AnalysingPartnerType }
     * 
     */
    public AnalysingPartnerType createAnalysingPartnerType() {
        return new AnalysingPartnerType();
    }

    /**
     * Create an instance of {@link PartnerFeedbackType }
     * 
     */
    public PartnerFeedbackType createPartnerFeedbackType() {
        return new PartnerFeedbackType();
    }

    /**
     * Create an instance of {@link VehicleTransferType }
     * 
     */
    public VehicleTransferType createVehicleTransferType() {
        return new VehicleTransferType();
    }

    /**
     * Create an instance of {@link TechnicalSolutionType }
     * 
     */
    public TechnicalSolutionType createTechnicalSolutionType() {
        return new TechnicalSolutionType();
    }

    /**
     * Create an instance of {@link ComplaintCodeChoiceType }
     * 
     */
    public ComplaintCodeChoiceType createComplaintCodeChoiceType() {
        return new ComplaintCodeChoiceType();
    }

    /**
     * Create an instance of {@link ConditionsType }
     * 
     */
    public ConditionsType createConditionsType() {
        return new ConditionsType();
    }

    /**
     * Create an instance of {@link CustomerInformationType }
     * 
     */
    public CustomerInformationType createCustomerInformationType() {
        return new CustomerInformationType();
    }

    /**
     * Create an instance of {@link AggregateExchangeType }
     * 
     */
    public AggregateExchangeType createAggregateExchangeType() {
        return new AggregateExchangeType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WorkshopComplaintIdentifierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/WorkshopComplaint", name = "WorkshopComplaintRef")
    public JAXBElement<WorkshopComplaintIdentifierType> createWorkshopComplaintRef(WorkshopComplaintIdentifierType value) {
        return new JAXBElement<WorkshopComplaintIdentifierType>(_WorkshopComplaintRef_QNAME, WorkshopComplaintIdentifierType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WorkshopComplaintType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/WorkshopComplaint", name = "WorkshopComplaint")
    public JAXBElement<WorkshopComplaintType> createWorkshopComplaint(WorkshopComplaintType value) {
        return new JAXBElement<WorkshopComplaintType>(_WorkshopComplaint_QNAME, WorkshopComplaintType.class, null, value);
    }

}
