
package com.volkswagenag.xmldefs.dd.workshopcomplaint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.volkswagenag.xmldefs.dd.attachment.AttachmentIdentifierType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * A complaint attachment.
 * 
 * <p>Java-Klasse f�r AttachmentType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AttachmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DocumentType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="InternDocumentIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="DocumentCreator" type="{http://xmldefs.volkswagenag.com/DD/WorkshopComplaint}UserInfoType" minOccurs="0"/>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/Attachment}AttachmentRef" minOccurs="0"/>
 *         &lt;element name="LastUpdate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttachmentType", propOrder = {
    "documentType",
    "title",
    "internDocumentIND",
    "documentCreator",
    "attachmentRef",
    "lastUpdate"
})
public class AttachmentType {

    @XmlElement(name = "DocumentType")
    protected CodeType documentType;
    @XmlElement(name = "Title")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String title;
    @XmlElement(name = "InternDocumentIND")
    protected Boolean internDocumentIND;
    @XmlElement(name = "DocumentCreator")
    protected UserInfoType documentCreator;
    @XmlElement(name = "AttachmentRef", namespace = "http://xmldefs.volkswagenag.com/DD/Attachment")
    protected AttachmentIdentifierType attachmentRef;
    @XmlElement(name = "LastUpdate")
    protected String lastUpdate;

    /**
     * Ruft den Wert der documentType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getDocumentType() {
        return documentType;
    }

    /**
     * Legt den Wert der documentType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setDocumentType(CodeType value) {
        this.documentType = value;
    }

    /**
     * Ruft den Wert der title-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Legt den Wert der title-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Ruft den Wert der internDocumentIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInternDocumentIND() {
        return internDocumentIND;
    }

    /**
     * Legt den Wert der internDocumentIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInternDocumentIND(Boolean value) {
        this.internDocumentIND = value;
    }

    /**
     * Ruft den Wert der documentCreator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserInfoType }
     *     
     */
    public UserInfoType getDocumentCreator() {
        return documentCreator;
    }

    /**
     * Legt den Wert der documentCreator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserInfoType }
     *     
     */
    public void setDocumentCreator(UserInfoType value) {
        this.documentCreator = value;
    }

    /**
     * Ruft den Wert der attachmentRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttachmentIdentifierType }
     *     
     */
    public AttachmentIdentifierType getAttachmentRef() {
        return attachmentRef;
    }

    /**
     * Legt den Wert der attachmentRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachmentIdentifierType }
     *     
     */
    public void setAttachmentRef(AttachmentIdentifierType value) {
        this.attachmentRef = value;
    }

    /**
     * Ruft den Wert der lastUpdate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastUpdate() {
        return lastUpdate;
    }

    /**
     * Legt den Wert der lastUpdate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastUpdate(String value) {
        this.lastUpdate = value;
    }

}
