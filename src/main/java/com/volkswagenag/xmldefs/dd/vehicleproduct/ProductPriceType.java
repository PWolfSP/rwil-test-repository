
package com.volkswagenag.xmldefs.dd.vehicleproduct;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.AmountType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ProductPriceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProductPriceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CalculationAmount" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}AmountType" minOccurs="0"/>
 *         &lt;element name="DisplayAmount" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}AmountType" minOccurs="0"/>
 *         &lt;element name="PriceMeasure" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="PriceType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="PriceKind" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductPriceType", propOrder = {
    "calculationAmount",
    "displayAmount",
    "priceMeasure",
    "priceType",
    "priceKind"
})
public class ProductPriceType {

    @XmlElement(name = "CalculationAmount")
    protected AmountType calculationAmount;
    @XmlElement(name = "DisplayAmount")
    protected AmountType displayAmount;
    @XmlElement(name = "PriceMeasure")
    protected CodeType priceMeasure;
    @XmlElement(name = "PriceType")
    protected CodeType priceType;
    @XmlElement(name = "PriceKind")
    protected CodeType priceKind;

    /**
     * Ruft den Wert der calculationAmount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getCalculationAmount() {
        return calculationAmount;
    }

    /**
     * Legt den Wert der calculationAmount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setCalculationAmount(AmountType value) {
        this.calculationAmount = value;
    }

    /**
     * Ruft den Wert der displayAmount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getDisplayAmount() {
        return displayAmount;
    }

    /**
     * Legt den Wert der displayAmount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setDisplayAmount(AmountType value) {
        this.displayAmount = value;
    }

    /**
     * Ruft den Wert der priceMeasure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getPriceMeasure() {
        return priceMeasure;
    }

    /**
     * Legt den Wert der priceMeasure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setPriceMeasure(CodeType value) {
        this.priceMeasure = value;
    }

    /**
     * Ruft den Wert der priceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getPriceType() {
        return priceType;
    }

    /**
     * Legt den Wert der priceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setPriceType(CodeType value) {
        this.priceType = value;
    }

    /**
     * Ruft den Wert der priceKind-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getPriceKind() {
        return priceKind;
    }

    /**
     * Legt den Wert der priceKind-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setPriceKind(CodeType value) {
        this.priceKind = value;
    }

}
