
package com.volkswagenag.xmldefs.dd.vehicleproduct;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ADIType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ADIType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Finish" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}ColorsType" minOccurs="0"/>
 *         &lt;element name="Top" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}ColorsType" minOccurs="0"/>
 *         &lt;element name="Interior" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}InteriorType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ADIType", propOrder = {
    "finish",
    "top",
    "interior"
})
public class ADIType {

    @XmlElement(name = "Finish")
    protected ColorsType finish;
    @XmlElement(name = "Top")
    protected ColorsType top;
    @XmlElement(name = "Interior")
    protected InteriorType interior;

    /**
     * Ruft den Wert der finish-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ColorsType }
     *     
     */
    public ColorsType getFinish() {
        return finish;
    }

    /**
     * Legt den Wert der finish-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ColorsType }
     *     
     */
    public void setFinish(ColorsType value) {
        this.finish = value;
    }

    /**
     * Ruft den Wert der top-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ColorsType }
     *     
     */
    public ColorsType getTop() {
        return top;
    }

    /**
     * Legt den Wert der top-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ColorsType }
     *     
     */
    public void setTop(ColorsType value) {
        this.top = value;
    }

    /**
     * Ruft den Wert der interior-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InteriorType }
     *     
     */
    public InteriorType getInterior() {
        return interior;
    }

    /**
     * Legt den Wert der interior-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InteriorType }
     *     
     */
    public void setInterior(InteriorType value) {
        this.interior = value;
    }

}
