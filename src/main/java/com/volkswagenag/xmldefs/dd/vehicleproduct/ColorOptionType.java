
package com.volkswagenag.xmldefs.dd.vehicleproduct;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ColorOptionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ColorOptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ColorCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="Hue" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}ProductDescriptionType" minOccurs="0"/>
 *         &lt;element name="Prices" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}ProductPricesType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ColorOptionType", propOrder = {
    "colorCode",
    "hue",
    "description",
    "prices"
})
public class ColorOptionType {

    @XmlElement(name = "ColorCode")
    protected CodeType colorCode;
    @XmlElement(name = "Hue")
    protected CodeType hue;
    @XmlElement(name = "Description")
    protected ProductDescriptionType description;
    @XmlElement(name = "Prices")
    protected ProductPricesType prices;

    /**
     * Ruft den Wert der colorCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getColorCode() {
        return colorCode;
    }

    /**
     * Legt den Wert der colorCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setColorCode(CodeType value) {
        this.colorCode = value;
    }

    /**
     * Ruft den Wert der hue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getHue() {
        return hue;
    }

    /**
     * Legt den Wert der hue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setHue(CodeType value) {
        this.hue = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProductDescriptionType }
     *     
     */
    public ProductDescriptionType getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductDescriptionType }
     *     
     */
    public void setDescription(ProductDescriptionType value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der prices-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProductPricesType }
     *     
     */
    public ProductPricesType getPrices() {
        return prices;
    }

    /**
     * Legt den Wert der prices-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductPricesType }
     *     
     */
    public void setPrices(ProductPricesType value) {
        this.prices = value;
    }

}
