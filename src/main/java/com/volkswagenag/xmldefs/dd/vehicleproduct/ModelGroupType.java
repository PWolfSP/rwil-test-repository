
package com.volkswagenag.xmldefs.dd.vehicleproduct;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import lombok.Generated;


/**
 * <p>Java-Klasse für ModelGroupType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ModelGroupType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ModelGroup" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="ModelGroupDescriptions" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}DescriptionsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModelGroupType", propOrder = {
    "modelGroup",
    "modelGroupDescriptions"
})
public class ModelGroupType {

    @XmlElement(name = "ModelGroup")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String modelGroup;
    @XmlElement(name = "ModelGroupDescriptions")
    protected DescriptionsType modelGroupDescriptions;

    /**
     * Ruft den Wert der modelGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelGroup() {
        return modelGroup;
    }

    /**
     * Legt den Wert der modelGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelGroup(String value) {
        this.modelGroup = value;
    }

    /**
     * Ruft den Wert der modelGroupDescriptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DescriptionsType }
     *     
     */
    public DescriptionsType getModelGroupDescriptions() {
        return modelGroupDescriptions;
    }

    /**
     * Legt den Wert der modelGroupDescriptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DescriptionsType }
     *     
     */
    public void setModelGroupDescriptions(DescriptionsType value) {
        this.modelGroupDescriptions = value;
    }

}
