
package com.volkswagenag.xmldefs.dd.vehicleproduct;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ColorsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ColorsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ColorOption" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}ColorOptionType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ColorsType", propOrder = {
    "colorOption"
})



public class ColorsType {
	
	
    @XmlElement(name = "ColorOption", required = true)
    private List<ColorOptionType> colorOption;


		public void setColorOptionType(List<ColorOptionType> colorOption) {
			this.colorOption = colorOption;
		}
		
	    public List<ColorOptionType> getColorOption() {
	        if (colorOption == null) {
	            colorOption = new ArrayList<ColorOptionType>();
	        }
	        return this.colorOption;
	    }

}
