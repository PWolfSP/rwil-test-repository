
package com.volkswagenag.xmldefs.dd.vehicleproduct;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.TextType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ProductDescriptionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProductDescriptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DescriptionCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="StandardDescription" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}TextType" minOccurs="0"/>
 *         &lt;element name="MarketDescriptions" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}MarketDescriptionsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductDescriptionType", propOrder = {
    "descriptionCode",
    "standardDescription",
    "marketDescriptions"
})
public class ProductDescriptionType {

    @XmlElement(name = "DescriptionCode")
    protected CodeType descriptionCode;
    @XmlElement(name = "StandardDescription")
    protected TextType standardDescription;
    @XmlElement(name = "MarketDescriptions")
    protected MarketDescriptionsType marketDescriptions;

    /**
     * Ruft den Wert der descriptionCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getDescriptionCode() {
        return descriptionCode;
    }

    /**
     * Legt den Wert der descriptionCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setDescriptionCode(CodeType value) {
        this.descriptionCode = value;
    }

    /**
     * Ruft den Wert der standardDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getStandardDescription() {
        return standardDescription;
    }

    /**
     * Legt den Wert der standardDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setStandardDescription(TextType value) {
        this.standardDescription = value;
    }

    /**
     * Ruft den Wert der marketDescriptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MarketDescriptionsType }
     *     
     */
    public MarketDescriptionsType getMarketDescriptions() {
        return marketDescriptions;
    }

    /**
     * Legt den Wert der marketDescriptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MarketDescriptionsType }
     *     
     */
    public void setMarketDescriptions(MarketDescriptionsType value) {
        this.marketDescriptions = value;
    }

}
