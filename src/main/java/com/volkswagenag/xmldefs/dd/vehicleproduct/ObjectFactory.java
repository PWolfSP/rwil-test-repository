
package com.volkswagenag.xmldefs.dd.vehicleproduct;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import lombok.Generated;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.volkswagenag.xmldefs.dd.vehicleproduct package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@Generated
@XmlRegistry
public class ObjectFactory {

    private final static QName _VehicleProduct_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/VehicleProduct", "VehicleProduct");
    private final static QName _VehicleProductRef_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/VehicleProduct", "VehicleProductRef");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.volkswagenag.xmldefs.dd.vehicleproduct
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link VehicleProductType }
     * 
     */
    public VehicleProductType createVehicleProductType() {
        return new VehicleProductType();
    }

    /**
     * Create an instance of {@link VehicleProductIdentifierType }
     * 
     */
    public VehicleProductIdentifierType createVehicleProductIdentifierType() {
        return new VehicleProductIdentifierType();
    }

    /**
     * Create an instance of {@link SalesGroupType }
     * 
     */
    public SalesGroupType createSalesGroupType() {
        return new SalesGroupType();
    }

    /**
     * Create an instance of {@link MarketDescriptionsType }
     * 
     */
    public MarketDescriptionsType createMarketDescriptionsType() {
        return new MarketDescriptionsType();
    }

    /**
     * Create an instance of {@link ADIType }
     * 
     */
    public ADIType createADIType() {
        return new ADIType();
    }

    /**
     * Create an instance of {@link EquipmentType }
     * 
     */
    public EquipmentType createEquipmentType() {
        return new EquipmentType();
    }

    /**
     * Create an instance of {@link EquipmentsType }
     * 
     */
    public EquipmentsType createEquipmentsType() {
        return new EquipmentsType();
    }

    /**
     * Create an instance of {@link EquipmentOptionType }
     * 
     */
    public EquipmentOptionType createEquipmentOptionType() {
        return new EquipmentOptionType();
    }

    /**
     * Create an instance of {@link ColorsType }
     * 
     */
    public ColorsType createColorsType() {
        return new ColorsType();
    }

    /**
     * Create an instance of {@link TechnicalSpecificationType }
     * 
     */
    public TechnicalSpecificationType createTechnicalSpecificationType() {
        return new TechnicalSpecificationType();
    }

    /**
     * Create an instance of {@link ProductPriceType }
     * 
     */
    public ProductPriceType createProductPriceType() {
        return new ProductPriceType();
    }

    /**
     * Create an instance of {@link ColorOptionType }
     * 
     */
    public ColorOptionType createColorOptionType() {
        return new ColorOptionType();
    }

    /**
     * Create an instance of {@link ApplicationIdentifiersType }
     * 
     */
    public ApplicationIdentifiersType createApplicationIdentifiersType() {
        return new ApplicationIdentifiersType();
    }

    /**
     * Create an instance of {@link TechnicalSpecificationsType }
     * 
     */
    public TechnicalSpecificationsType createTechnicalSpecificationsType() {
        return new TechnicalSpecificationsType();
    }

    /**
     * Create an instance of {@link EquipmentOptionsType }
     * 
     */
    public EquipmentOptionsType createEquipmentOptionsType() {
        return new EquipmentOptionsType();
    }

    /**
     * Create an instance of {@link EquipmentDescriptionsType }
     * 
     */
    public EquipmentDescriptionsType createEquipmentDescriptionsType() {
        return new EquipmentDescriptionsType();
    }

    /**
     * Create an instance of {@link ProductDescriptionType }
     * 
     */
    public ProductDescriptionType createProductDescriptionType() {
        return new ProductDescriptionType();
    }

    /**
     * Create an instance of {@link InteriorType }
     * 
     */
    public InteriorType createInteriorType() {
        return new InteriorType();
    }

    /**
     * Create an instance of {@link InteriorOptionType }
     * 
     */
    public InteriorOptionType createInteriorOptionType() {
        return new InteriorOptionType();
    }

    /**
     * Create an instance of {@link PriceCatalogType }
     * 
     */
    public PriceCatalogType createPriceCatalogType() {
        return new PriceCatalogType();
    }

    /**
     * Create an instance of {@link EquipmentDescriptionType }
     * 
     */
    public EquipmentDescriptionType createEquipmentDescriptionType() {
        return new EquipmentDescriptionType();
    }

    /**
     * Create an instance of {@link MarketDescriptionType }
     * 
     */
    public MarketDescriptionType createMarketDescriptionType() {
        return new MarketDescriptionType();
    }

    /**
     * Create an instance of {@link DescriptionsType }
     * 
     */
    public DescriptionsType createDescriptionsType() {
        return new DescriptionsType();
    }

    /**
     * Create an instance of {@link ProductPricesType }
     * 
     */
    public ProductPricesType createProductPricesType() {
        return new ProductPricesType();
    }

    /**
     * Create an instance of {@link CarLineType }
     * 
     */
    public CarLineType createCarLineType() {
        return new CarLineType();
    }

    /**
     * Create an instance of {@link ModelGroupType }
     * 
     */
    public ModelGroupType createModelGroupType() {
        return new ModelGroupType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VehicleProductType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/VehicleProduct", name = "VehicleProduct")
    public JAXBElement<VehicleProductType> createVehicleProduct(VehicleProductType value) {
        return new JAXBElement<VehicleProductType>(_VehicleProduct_QNAME, VehicleProductType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VehicleProductIdentifierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/VehicleProduct", name = "VehicleProductRef")
    public JAXBElement<VehicleProductIdentifierType> createVehicleProductRef(VehicleProductIdentifierType value) {
        return new JAXBElement<VehicleProductIdentifierType>(_VehicleProductRef_QNAME, VehicleProductIdentifierType.class, null, value);
    }

}
