
package com.volkswagenag.xmldefs.dd.vehicleproduct;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für TechnicalSpecificationsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TechnicalSpecificationsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TechnicalSpecification" type="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}TechnicalSpecificationType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TechnicalSpecificationsType", propOrder = {
    "technicalSpecification"
})



public class TechnicalSpecificationsType {
	
	
    @XmlElement(name = "TechnicalSpecification", required = true)
    private List<TechnicalSpecificationType> technicalSpecification;


		public void setTechnicalSpecificationType(List<TechnicalSpecificationType> technicalSpecification) {
			this.technicalSpecification = technicalSpecification;
		}
		
	    public List<TechnicalSpecificationType> getTechnicalSpecification() {
	        if (technicalSpecification == null) {
	            technicalSpecification = new ArrayList<TechnicalSpecificationType>();
	        }
	        return this.technicalSpecification;
	    }

}
