
package com.volkswagenag.xmldefs.dd.commons;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.volkswagenag.xmldefs.dd.basictypes.TextType;

import lombok.Generated;


/**
 * <p>Java-Klasse f�r OldFaultType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OldFaultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FaultCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String"/>
 *         &lt;element name="FaultDescription" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}TextType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FaultID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="FaultClass" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="FaultTimestamp" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateTimeType" minOccurs="0"/>
 *         &lt;element name="FaultSystem" type="{http://xmldefs.volkswagenag.com/DD/Commons}OldFaultSystemType" minOccurs="0"/>
 *         &lt;element name="FaultReason" type="{http://xmldefs.volkswagenag.com/DD/Commons}OldFaultType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OldFaultType", propOrder = {
    "faultCode",
    "faultDescription",
    "faultID",
    "faultClass",
    "faultTimestamp",
    "faultSystem",
    "faultReason"
})



public class OldFaultType {
	
	
    @XmlElement(name = "FaultCode", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String faultCode;
    @XmlElement(name = "FaultDescription")
    private List<TextType> faultDescription;
    @XmlElement(name = "FaultID")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String faultID;
    @XmlElement(name = "FaultClass")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String faultClass;
    @XmlElement(name = "FaultTimestamp")
    protected String faultTimestamp;
    @XmlElement(name = "FaultSystem")
    protected OldFaultSystemType faultSystem;
    @XmlElement(name = "FaultReason")
    protected OldFaultType faultReason;

    /**
     * Ruft den Wert der faultCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaultCode() {
        return faultCode;
    }

    /**
     * Legt den Wert der faultCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaultCode(String value) {
        this.faultCode = value;
    }


		public void setTextType(List<TextType> faultDescription) {
			this.faultDescription = faultDescription;
		}
		
	    public List<TextType> getFaultDescription() {
	        if (faultDescription == null) {
	            faultDescription = new ArrayList<TextType>();
	        }
	        return this.faultDescription;
	    }

}
