
package com.volkswagenag.xmldefs.dd.commons;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für FaultSystemDetailType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="FaultSystemDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Param" type="{http://xmldefs.volkswagenag.com/DD/Commons}SystemParamType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaultSystemDetailType", propOrder = {
    "param"
})



public class FaultSystemDetailType {
	
	
    @XmlElement(name = "Param", required = true)
    private List<SystemParamType> param;


		public void setSystemParamType(List<SystemParamType> param) {
			this.param = param;
		}
		
	    public List<SystemParamType> getParam() {
	        if (param == null) {
	            param = new ArrayList<SystemParamType>();
	        }
	        return this.param;
	    }

}
