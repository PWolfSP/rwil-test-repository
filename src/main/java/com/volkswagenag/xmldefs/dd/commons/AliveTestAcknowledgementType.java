
package com.volkswagenag.xmldefs.dd.commons;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für AliveTestAcknowledgementType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AliveTestAcknowledgementType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ServiceProvider" type="{http://xmldefs.volkswagenag.com/DD/Commons}ServiceProviderType"/>
 *         &lt;element name="ServiceInfo" type="{http://xmldefs.volkswagenag.com/DD/Commons}ServiceInfoType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AliveTestAcknowledgementType", propOrder = {
    "serviceProvider",
    "serviceInfo"
})
public class AliveTestAcknowledgementType {

    @XmlElement(name = "ServiceProvider", required = true)
    protected ServiceProviderType serviceProvider;
    @XmlElement(name = "ServiceInfo", required = true)
    protected ServiceInfoType serviceInfo;

    /**
     * Ruft den Wert der serviceProvider-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderType }
     *     
     */
    public ServiceProviderType getServiceProvider() {
        return serviceProvider;
    }

    /**
     * Legt den Wert der serviceProvider-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderType }
     *     
     */
    public void setServiceProvider(ServiceProviderType value) {
        this.serviceProvider = value;
    }

    /**
     * Ruft den Wert der serviceInfo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInfoType }
     *     
     */
    public ServiceInfoType getServiceInfo() {
        return serviceInfo;
    }

    /**
     * Legt den Wert der serviceInfo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInfoType }
     *     
     */
    public void setServiceInfo(ServiceInfoType value) {
        this.serviceInfo = value;
    }

}
