
package com.volkswagenag.xmldefs.dd.commons;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für FaultCodeDetailType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="FaultCodeDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Param" type="{http://xmldefs.volkswagenag.com/DD/Commons}CodeParamType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaultCodeDetailType", propOrder = {
    "param"
})



public class FaultCodeDetailType {
	
	
    @XmlElement(name = "Param", required = true)
    private List<CodeParamType> param;


		public void setCodeParamType(List<CodeParamType> param) {
			this.param = param;
		}
		
	    public List<CodeParamType> getParam() {
	        if (param == null) {
	            param = new ArrayList<CodeParamType>();
	        }
	        return this.param;
	    }

}
