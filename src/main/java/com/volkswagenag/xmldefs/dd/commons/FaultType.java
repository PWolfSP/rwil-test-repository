
package com.volkswagenag.xmldefs.dd.commons;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für FaultType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="FaultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/Commons}FaultBasic"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaultType", propOrder = {
    "faultBasic"
})
public class FaultType {

    @XmlElement(name = "FaultBasic", required = true)
    protected FaultBasicType faultBasic;

    /**
     * Ruft den Wert der faultBasic-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FaultBasicType }
     *     
     */
    public FaultBasicType getFaultBasic() {
        return faultBasic;
    }

    /**
     * Legt den Wert der faultBasic-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FaultBasicType }
     *     
     */
    public void setFaultBasic(FaultBasicType value) {
        this.faultBasic = value;
    }

}
