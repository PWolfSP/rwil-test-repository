
package com.volkswagenag.xmldefs.dd.sparepart;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.CountryCodeType;
import com.volkswagenag.xmldefs.dd.basictypes.MeasureType;
import com.volkswagenag.xmldefs.dd.basictypes.TextType;

import lombok.Generated;


/**
 * <p>Java-Klasse für SparePartType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SparePartType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SparePartIdentifier" type="{http://xmldefs.volkswagenag.com/DD/SparePart}SparePartIdentifierType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}TextType" minOccurs="0"/>
 *         &lt;element name="InvoiceText" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}TextType"/>
 *         &lt;element name="Weight" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *         &lt;element name="Dimension" type="{http://xmldefs.volkswagenag.com/DD/SparePart}DimensionsType" minOccurs="0"/>
 *         &lt;element name="SparePartCategory" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="SparePartGroup" type="{http://xmldefs.volkswagenag.com/DD/SparePart}PartGroupType" minOccurs="0"/>
 *         &lt;element name="DisposalCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="HazardClass" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="VINRelated" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="Discontinuation" type="{http://xmldefs.volkswagenag.com/DD/SparePart}DiscontinuationType" minOccurs="0"/>
 *         &lt;element name="Note" type="{http://xmldefs.volkswagenag.com/DD/SparePart}NoteType" minOccurs="0"/>
 *         &lt;element name="Origin" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="Type" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="SID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String"/>
 *         &lt;element name="SIFRef" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String"/>
 *         &lt;element name="CountryCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CountryCodeType"/>
 *         &lt;element name="Importer" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String"/>
 *         &lt;element name="Brand" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="Value" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SparePartType", propOrder = {
    "sparePartIdentifier",
    "description",
    "invoiceText",
    "weight",
    "dimension",
    "sparePartCategory",
    "sparePartGroup",
    "disposalCode",
    "hazardClass",
    "vinRelated",
    "discontinuation",
    "note",
    "origin",
    "type",
    "sid",
    "sifRef",
    "countryCode",
    "importer",
    "brand",
    "value"
})
public class SparePartType {

    @XmlElement(name = "SparePartIdentifier")
    protected SparePartIdentifierType sparePartIdentifier;
    @XmlElement(name = "Description")
    protected TextType description;
    @XmlElement(name = "InvoiceText", required = true)
    protected TextType invoiceText;
    @XmlElement(name = "Weight")
    protected MeasureType weight;
    @XmlElement(name = "Dimension")
    protected DimensionsType dimension;
    @XmlElement(name = "SparePartCategory")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String sparePartCategory;
    @XmlElement(name = "SparePartGroup")
    protected PartGroupType sparePartGroup;
    @XmlElement(name = "DisposalCode")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String disposalCode;
    @XmlElement(name = "HazardClass")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String hazardClass;
    @XmlElement(name = "VINRelated")
    protected Boolean vinRelated;
    @XmlElement(name = "Discontinuation")
    protected DiscontinuationType discontinuation;
    @XmlElement(name = "Note")
    protected NoteType note;
    @XmlElement(name = "Origin")
    protected CodeType origin;
    @XmlElement(name = "Type")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String type;
    @XmlElement(name = "SID", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String sid;
    @XmlElement(name = "SIFRef", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String sifRef;
    @XmlElement(name = "CountryCode", required = true)
    protected CountryCodeType countryCode;
    @XmlElement(name = "Importer", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String importer;
    @XmlElement(name = "Brand")
    protected CodeType brand;
    @XmlElement(name = "Value", required = true)
    protected CodeType value;

    /**
     * Ruft den Wert der sparePartIdentifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SparePartIdentifierType }
     *     
     */
    public SparePartIdentifierType getSparePartIdentifier() {
        return sparePartIdentifier;
    }

    /**
     * Legt den Wert der sparePartIdentifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SparePartIdentifierType }
     *     
     */
    public void setSparePartIdentifier(SparePartIdentifierType value) {
        this.sparePartIdentifier = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setDescription(TextType value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der invoiceText-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getInvoiceText() {
        return invoiceText;
    }

    /**
     * Legt den Wert der invoiceText-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setInvoiceText(TextType value) {
        this.invoiceText = value;
    }

    /**
     * Ruft den Wert der weight-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getWeight() {
        return weight;
    }

    /**
     * Legt den Wert der weight-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setWeight(MeasureType value) {
        this.weight = value;
    }

    /**
     * Ruft den Wert der dimension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DimensionsType }
     *     
     */
    public DimensionsType getDimension() {
        return dimension;
    }

    /**
     * Legt den Wert der dimension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DimensionsType }
     *     
     */
    public void setDimension(DimensionsType value) {
        this.dimension = value;
    }

    /**
     * Ruft den Wert der sparePartCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSparePartCategory() {
        return sparePartCategory;
    }

    /**
     * Legt den Wert der sparePartCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSparePartCategory(String value) {
        this.sparePartCategory = value;
    }

    /**
     * Ruft den Wert der sparePartGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartGroupType }
     *     
     */
    public PartGroupType getSparePartGroup() {
        return sparePartGroup;
    }

    /**
     * Legt den Wert der sparePartGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartGroupType }
     *     
     */
    public void setSparePartGroup(PartGroupType value) {
        this.sparePartGroup = value;
    }

    /**
     * Ruft den Wert der disposalCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisposalCode() {
        return disposalCode;
    }

    /**
     * Legt den Wert der disposalCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisposalCode(String value) {
        this.disposalCode = value;
    }

    /**
     * Ruft den Wert der hazardClass-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazardClass() {
        return hazardClass;
    }

    /**
     * Legt den Wert der hazardClass-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazardClass(String value) {
        this.hazardClass = value;
    }

    /**
     * Ruft den Wert der vinRelated-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVINRelated() {
        return vinRelated;
    }

    /**
     * Legt den Wert der vinRelated-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVINRelated(Boolean value) {
        this.vinRelated = value;
    }

    /**
     * Ruft den Wert der discontinuation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DiscontinuationType }
     *     
     */
    public DiscontinuationType getDiscontinuation() {
        return discontinuation;
    }

    /**
     * Legt den Wert der discontinuation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DiscontinuationType }
     *     
     */
    public void setDiscontinuation(DiscontinuationType value) {
        this.discontinuation = value;
    }

    /**
     * Ruft den Wert der note-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NoteType }
     *     
     */
    public NoteType getNote() {
        return note;
    }

    /**
     * Legt den Wert der note-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NoteType }
     *     
     */
    public void setNote(NoteType value) {
        this.note = value;
    }

    /**
     * Ruft den Wert der origin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getOrigin() {
        return origin;
    }

    /**
     * Legt den Wert der origin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setOrigin(CodeType value) {
        this.origin = value;
    }

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Ruft den Wert der sid-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSID() {
        return sid;
    }

    /**
     * Legt den Wert der sid-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSID(String value) {
        this.sid = value;
    }

    /**
     * Ruft den Wert der sifRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSIFRef() {
        return sifRef;
    }

    /**
     * Legt den Wert der sifRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSIFRef(String value) {
        this.sifRef = value;
    }

    /**
     * Ruft den Wert der countryCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CountryCodeType }
     *     
     */
    public CountryCodeType getCountryCode() {
        return countryCode;
    }

    /**
     * Legt den Wert der countryCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryCodeType }
     *     
     */
    public void setCountryCode(CountryCodeType value) {
        this.countryCode = value;
    }

    /**
     * Ruft den Wert der importer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImporter() {
        return importer;
    }

    /**
     * Legt den Wert der importer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImporter(String value) {
        this.importer = value;
    }

    /**
     * Ruft den Wert der brand-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getBrand() {
        return brand;
    }

    /**
     * Legt den Wert der brand-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setBrand(CodeType value) {
        this.brand = value;
    }

    /**
     * Ruft den Wert der value-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getValue() {
        return value;
    }

    /**
     * Legt den Wert der value-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setValue(CodeType value) {
        this.value = value;
    }

}
