
package com.volkswagenag.xmldefs.dd.sparepart;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import lombok.Generated;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.volkswagenag.xmldefs.dd.sparepart package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@Generated
@XmlRegistry
public class ObjectFactory {

    private final static QName _SparePartRef_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/SparePart", "SparePartRef");
    private final static QName _SparePart_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/SparePart", "SparePart");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.volkswagenag.xmldefs.dd.sparepart
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SparePartIdentifierType }
     * 
     */
    public SparePartIdentifierType createSparePartIdentifierType() {
        return new SparePartIdentifierType();
    }

    /**
     * Create an instance of {@link SparePartType }
     * 
     */
    public SparePartType createSparePartType() {
        return new SparePartType();
    }

    /**
     * Create an instance of {@link NoteType }
     * 
     */
    public NoteType createNoteType() {
        return new NoteType();
    }

    /**
     * Create an instance of {@link DimensionsType }
     * 
     */
    public DimensionsType createDimensionsType() {
        return new DimensionsType();
    }

    /**
     * Create an instance of {@link ApplicationIdentifiersType }
     * 
     */
    public ApplicationIdentifiersType createApplicationIdentifiersType() {
        return new ApplicationIdentifiersType();
    }

    /**
     * Create an instance of {@link PartGroupType }
     * 
     */
    public PartGroupType createPartGroupType() {
        return new PartGroupType();
    }

    /**
     * Create an instance of {@link DiscontinuationType }
     * 
     */
    public DiscontinuationType createDiscontinuationType() {
        return new DiscontinuationType();
    }

    /**
     * Create an instance of {@link SparePartRefType }
     * 
     */
    public SparePartRefType createSparePartRefType() {
        return new SparePartRefType();
    }

    /**
     * Create an instance of {@link NotesType }
     * 
     */
    public NotesType createNotesType() {
        return new NotesType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SparePartIdentifierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/SparePart", name = "SparePartRef")
    public JAXBElement<SparePartIdentifierType> createSparePartRef(SparePartIdentifierType value) {
        return new JAXBElement<SparePartIdentifierType>(_SparePartRef_QNAME, SparePartIdentifierType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SparePartType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/SparePart", name = "SparePart")
    public JAXBElement<SparePartType> createSparePart(SparePartType value) {
        return new JAXBElement<SparePartType>(_SparePart_QNAME, SparePartType.class, null, value);
    }

}
