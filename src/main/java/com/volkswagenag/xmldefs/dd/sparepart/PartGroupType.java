
package com.volkswagenag.xmldefs.dd.sparepart;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für PartGroupType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PartGroupType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PartGroupCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="MaterialGroup" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartGroupType", propOrder = {
    "partGroupCode",
    "materialGroup"
})
public class PartGroupType {

    @XmlElement(name = "PartGroupCode")
    protected CodeType partGroupCode;
    @XmlElement(name = "MaterialGroup")
    protected CodeType materialGroup;

    /**
     * Ruft den Wert der partGroupCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getPartGroupCode() {
        return partGroupCode;
    }

    /**
     * Legt den Wert der partGroupCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setPartGroupCode(CodeType value) {
        this.partGroupCode = value;
    }

    /**
     * Ruft den Wert der materialGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getMaterialGroup() {
        return materialGroup;
    }

    /**
     * Legt den Wert der materialGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setMaterialGroup(CodeType value) {
        this.materialGroup = value;
    }

}
