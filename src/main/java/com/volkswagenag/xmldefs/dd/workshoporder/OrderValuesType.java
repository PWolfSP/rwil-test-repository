
package com.volkswagenag.xmldefs.dd.workshoporder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für OrderValuesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OrderValuesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderValue" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}OrderValueType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderValuesType", propOrder = {
    "orderValue"
})



public class OrderValuesType {
	
	
    @XmlElement(name = "OrderValue", required = true)
    private List<OrderValueType> orderValue;


		public void setOrderValueType(List<OrderValueType> orderValue) {
			this.orderValue = orderValue;
		}
		
	    public List<OrderValueType> getOrderValue() {
	        if (orderValue == null) {
	            orderValue = new ArrayList<OrderValueType>();
	        }
	        return this.orderValue;
	    }

}
