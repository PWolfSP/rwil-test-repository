
package com.volkswagenag.xmldefs.dd.workshoporder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für AlertRefsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AlertRefsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AlertRef" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}DataRefType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlertRefsType", propOrder = {
    "alertRef"
})



public class AlertRefsType {
	
	
    @XmlElement(name = "AlertRef", required = true)
    private List<DataRefType> alertRef;


		public void setDataRefType(List<DataRefType> alertRef) {
			this.alertRef = alertRef;
		}
		
	    public List<DataRefType> getAlertRef() {
	        if (alertRef == null) {
	            alertRef = new ArrayList<DataRefType>();
	        }
	        return this.alertRef;
	    }

}
