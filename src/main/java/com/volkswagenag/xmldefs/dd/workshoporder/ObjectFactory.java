
package com.volkswagenag.xmldefs.dd.workshoporder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import lombok.Generated;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.volkswagenag.xmldefs.dd.workshoporder package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@Generated
@XmlRegistry
public class ObjectFactory {

    private final static QName _WorkshopOrder_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/WorkshopOrder", "WorkshopOrder");
    private final static QName _WorkshopOrderRef_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/WorkshopOrder", "WorkshopOrderRef");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.volkswagenag.xmldefs.dd.workshoporder
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WorkshopOrderType }
     * 
     */
    public WorkshopOrderType createWorkshopOrderType() {
        return new WorkshopOrderType();
    }

    /**
     * Create an instance of {@link WorkshopOrderIdentifierType }
     * 
     */
    public WorkshopOrderIdentifierType createWorkshopOrderIdentifierType() {
        return new WorkshopOrderIdentifierType();
    }

    /**
     * Create an instance of {@link ApplicationIdentifiersType }
     * 
     */
    public ApplicationIdentifiersType createApplicationIdentifiersType() {
        return new ApplicationIdentifiersType();
    }

    /**
     * Create an instance of {@link DataRefType }
     * 
     */
    public DataRefType createDataRefType() {
        return new DataRefType();
    }

    /**
     * Create an instance of {@link ServiceAdvisorType }
     * 
     */
    public ServiceAdvisorType createServiceAdvisorType() {
        return new ServiceAdvisorType();
    }

    /**
     * Create an instance of {@link CustomerType }
     * 
     */
    public CustomerType createCustomerType() {
        return new CustomerType();
    }

    /**
     * Create an instance of {@link AgencyGoodsAmountsType }
     * 
     */
    public AgencyGoodsAmountsType createAgencyGoodsAmountsType() {
        return new AgencyGoodsAmountsType();
    }

    /**
     * Create an instance of {@link ExchangePartsAmountsType }
     * 
     */
    public ExchangePartsAmountsType createExchangePartsAmountsType() {
        return new ExchangePartsAmountsType();
    }

    /**
     * Create an instance of {@link DealerType }
     * 
     */
    public DealerType createDealerType() {
        return new DealerType();
    }

    /**
     * Create an instance of {@link InvoiceType }
     * 
     */
    public InvoiceType createInvoiceType() {
        return new InvoiceType();
    }

    /**
     * Create an instance of {@link ClaimType }
     * 
     */
    public ClaimType createClaimType() {
        return new ClaimType();
    }

    /**
     * Create an instance of {@link OrderValueType }
     * 
     */
    public OrderValueType createOrderValueType() {
        return new OrderValueType();
    }

    /**
     * Create an instance of {@link ReceiptType }
     * 
     */
    public ReceiptType createReceiptType() {
        return new ReceiptType();
    }

    /**
     * Create an instance of {@link TaxAmountsType }
     * 
     */
    public TaxAmountsType createTaxAmountsType() {
        return new TaxAmountsType();
    }

    /**
     * Create an instance of {@link WagesAmountsType }
     * 
     */
    public WagesAmountsType createWagesAmountsType() {
        return new WagesAmountsType();
    }

    /**
     * Create an instance of {@link OrderValuesType }
     * 
     */
    public OrderValuesType createOrderValuesType() {
        return new OrderValuesType();
    }

    /**
     * Create an instance of {@link TaxAmountType }
     * 
     */
    public TaxAmountType createTaxAmountType() {
        return new TaxAmountType();
    }

    /**
     * Create an instance of {@link MaterialAmountsType }
     * 
     */
    public MaterialAmountsType createMaterialAmountsType() {
        return new MaterialAmountsType();
    }

    /**
     * Create an instance of {@link VehiclesType }
     * 
     */
    public VehiclesType createVehiclesType() {
        return new VehiclesType();
    }

    /**
     * Create an instance of {@link OrderTypeType }
     * 
     */
    public OrderTypeType createOrderTypeType() {
        return new OrderTypeType();
    }

    /**
     * Create an instance of {@link AmountsType }
     * 
     */
    public AmountsType createAmountsType() {
        return new AmountsType();
    }

    /**
     * Create an instance of {@link ExchangePartsTaxAmountsType }
     * 
     */
    public ExchangePartsTaxAmountsType createExchangePartsTaxAmountsType() {
        return new ExchangePartsTaxAmountsType();
    }

    /**
     * Create an instance of {@link AlertRefsType }
     * 
     */
    public AlertRefsType createAlertRefsType() {
        return new AlertRefsType();
    }

    /**
     * Create an instance of {@link PriceAmountType }
     * 
     */
    public PriceAmountType createPriceAmountType() {
        return new PriceAmountType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WorkshopOrderType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/WorkshopOrder", name = "WorkshopOrder")
    public JAXBElement<WorkshopOrderType> createWorkshopOrder(WorkshopOrderType value) {
        return new JAXBElement<WorkshopOrderType>(_WorkshopOrder_QNAME, WorkshopOrderType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WorkshopOrderIdentifierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/WorkshopOrder", name = "WorkshopOrderRef")
    public JAXBElement<WorkshopOrderIdentifierType> createWorkshopOrderRef(WorkshopOrderIdentifierType value) {
        return new JAXBElement<WorkshopOrderIdentifierType>(_WorkshopOrderRef_QNAME, WorkshopOrderIdentifierType.class, null, value);
    }

}
