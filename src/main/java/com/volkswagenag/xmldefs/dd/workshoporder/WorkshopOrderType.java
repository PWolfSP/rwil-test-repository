
package com.volkswagenag.xmldefs.dd.workshoporder;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für WorkshopOrderType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="WorkshopOrderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WorkshopOrderIdentifier" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}WorkshopOrderIdentifierType"/>
 *         &lt;element name="OrderType" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}OrderTypeType" minOccurs="0"/>
 *         &lt;element name="OrderStatus" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ServiceAdvisor" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}ServiceAdvisorType" minOccurs="0"/>
 *         &lt;element name="ScheduleCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="WagesShare" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *         &lt;element name="PartsShare" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *         &lt;element name="AlertRefs" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}AlertRefsType" minOccurs="0"/>
 *         &lt;element name="RelatedOrder" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}WorkshopOrderIdentifierType" minOccurs="0"/>
 *         &lt;element name="Customer" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}CustomerType" minOccurs="0"/>
 *         &lt;element name="Vehicles" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}VehiclesType" minOccurs="0"/>
 *         &lt;element name="Receipt" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}ReceiptType" minOccurs="0"/>
 *         &lt;element name="OrderValues" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}OrderValuesType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkshopOrderType", propOrder = {
    "workshopOrderIdentifier",
    "orderType",
    "orderStatus",
    "serviceAdvisor",
    "scheduleCode",
    "wagesShare",
    "partsShare",
    "alertRefs",
    "relatedOrder",
    "customer",
    "vehicles",
    "receipt",
    "orderValues"
})
public class WorkshopOrderType {

    @XmlElement(name = "WorkshopOrderIdentifier", required = true)
    protected WorkshopOrderIdentifierType workshopOrderIdentifier;
    @XmlElement(name = "OrderType")
    protected OrderTypeType orderType;
    @XmlElement(name = "OrderStatus")
    protected CodeType orderStatus;
    @XmlElement(name = "ServiceAdvisor")
    protected ServiceAdvisorType serviceAdvisor;
    @XmlElement(name = "ScheduleCode")
    protected CodeType scheduleCode;
    @XmlElement(name = "WagesShare")
    protected BigDecimal wagesShare;
    @XmlElement(name = "PartsShare")
    protected BigDecimal partsShare;
    @XmlElement(name = "AlertRefs")
    protected AlertRefsType alertRefs;
    @XmlElement(name = "RelatedOrder")
    protected WorkshopOrderIdentifierType relatedOrder;
    @XmlElement(name = "Customer")
    protected CustomerType customer;
    @XmlElement(name = "Vehicles")
    protected VehiclesType vehicles;
    @XmlElement(name = "Receipt")
    protected ReceiptType receipt;
    @XmlElement(name = "OrderValues")
    protected OrderValuesType orderValues;

    /**
     * Ruft den Wert der workshopOrderIdentifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WorkshopOrderIdentifierType }
     *     
     */
    public WorkshopOrderIdentifierType getWorkshopOrderIdentifier() {
        return workshopOrderIdentifier;
    }

    /**
     * Legt den Wert der workshopOrderIdentifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkshopOrderIdentifierType }
     *     
     */
    public void setWorkshopOrderIdentifier(WorkshopOrderIdentifierType value) {
        this.workshopOrderIdentifier = value;
    }

    /**
     * Ruft den Wert der orderType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OrderTypeType }
     *     
     */
    public OrderTypeType getOrderType() {
        return orderType;
    }

    /**
     * Legt den Wert der orderType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderTypeType }
     *     
     */
    public void setOrderType(OrderTypeType value) {
        this.orderType = value;
    }

    /**
     * Ruft den Wert der orderStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getOrderStatus() {
        return orderStatus;
    }

    /**
     * Legt den Wert der orderStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setOrderStatus(CodeType value) {
        this.orderStatus = value;
    }

    /**
     * Ruft den Wert der serviceAdvisor-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceAdvisorType }
     *     
     */
    public ServiceAdvisorType getServiceAdvisor() {
        return serviceAdvisor;
    }

    /**
     * Legt den Wert der serviceAdvisor-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceAdvisorType }
     *     
     */
    public void setServiceAdvisor(ServiceAdvisorType value) {
        this.serviceAdvisor = value;
    }

    /**
     * Ruft den Wert der scheduleCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getScheduleCode() {
        return scheduleCode;
    }

    /**
     * Legt den Wert der scheduleCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setScheduleCode(CodeType value) {
        this.scheduleCode = value;
    }

    /**
     * Ruft den Wert der wagesShare-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWagesShare() {
        return wagesShare;
    }

    /**
     * Legt den Wert der wagesShare-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWagesShare(BigDecimal value) {
        this.wagesShare = value;
    }

    /**
     * Ruft den Wert der partsShare-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPartsShare() {
        return partsShare;
    }

    /**
     * Legt den Wert der partsShare-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPartsShare(BigDecimal value) {
        this.partsShare = value;
    }

    /**
     * Ruft den Wert der alertRefs-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlertRefsType }
     *     
     */
    public AlertRefsType getAlertRefs() {
        return alertRefs;
    }

    /**
     * Legt den Wert der alertRefs-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlertRefsType }
     *     
     */
    public void setAlertRefs(AlertRefsType value) {
        this.alertRefs = value;
    }

    /**
     * Ruft den Wert der relatedOrder-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WorkshopOrderIdentifierType }
     *     
     */
    public WorkshopOrderIdentifierType getRelatedOrder() {
        return relatedOrder;
    }

    /**
     * Legt den Wert der relatedOrder-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkshopOrderIdentifierType }
     *     
     */
    public void setRelatedOrder(WorkshopOrderIdentifierType value) {
        this.relatedOrder = value;
    }

    /**
     * Ruft den Wert der customer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CustomerType }
     *     
     */
    public CustomerType getCustomer() {
        return customer;
    }

    /**
     * Legt den Wert der customer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerType }
     *     
     */
    public void setCustomer(CustomerType value) {
        this.customer = value;
    }

    /**
     * Ruft den Wert der vehicles-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VehiclesType }
     *     
     */
    public VehiclesType getVehicles() {
        return vehicles;
    }

    /**
     * Legt den Wert der vehicles-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VehiclesType }
     *     
     */
    public void setVehicles(VehiclesType value) {
        this.vehicles = value;
    }

    /**
     * Ruft den Wert der receipt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReceiptType }
     *     
     */
    public ReceiptType getReceipt() {
        return receipt;
    }

    /**
     * Legt den Wert der receipt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReceiptType }
     *     
     */
    public void setReceipt(ReceiptType value) {
        this.receipt = value;
    }

    /**
     * Ruft den Wert der orderValues-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OrderValuesType }
     *     
     */
    public OrderValuesType getOrderValues() {
        return orderValues;
    }

    /**
     * Legt den Wert der orderValues-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderValuesType }
     *     
     */
    public void setOrderValues(OrderValuesType value) {
        this.orderValues = value;
    }

}
