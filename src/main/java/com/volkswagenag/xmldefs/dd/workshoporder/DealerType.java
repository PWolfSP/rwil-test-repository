
package com.volkswagenag.xmldefs.dd.workshoporder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.businesspartner.PartnerIdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für DealerType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DealerType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}DealerRef"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DealerType", propOrder = {
    "dealerRef"
})
public class DealerType {

    @XmlElement(name = "DealerRef", namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", required = true)
    protected PartnerIdentifierType dealerRef;

    /**
     * Ruft den Wert der dealerRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public PartnerIdentifierType getDealerRef() {
        return dealerRef;
    }

    /**
     * Legt den Wert der dealerRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public void setDealerRef(PartnerIdentifierType value) {
        this.dealerRef = value;
    }

}
