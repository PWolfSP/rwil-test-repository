
package com.volkswagenag.xmldefs.dd.workshoporder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für WagesAmountsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="WagesAmountsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WagesAmount" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}PriceAmountType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WagesAmountsType", propOrder = {
    "wagesAmount"
})



public class WagesAmountsType {
	
	
    @XmlElement(name = "WagesAmount", required = true)
    private List<PriceAmountType> wagesAmount;


		public void setPriceAmountType(List<PriceAmountType> wagesAmount) {
			this.wagesAmount = wagesAmount;
		}
		
	    public List<PriceAmountType> getWagesAmount() {
	        if (wagesAmount == null) {
	            wagesAmount = new ArrayList<PriceAmountType>();
	        }
	        return this.wagesAmount;
	    }

}
