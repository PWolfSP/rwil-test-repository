
package com.volkswagenag.xmldefs.dd.workshoporder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * List of application specific identifiers.
 * 
 * <p>Java-Klasse für ApplicationIdentifiersType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ApplicationIdentifiersType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplicationIdentifiersType", propOrder = {
    "id"
})



public class ApplicationIdentifiersType {
	
	
    @XmlElement(name = "ID", required = true)
    private List<IdentifierType> id;


		public void setIdentifierType(List<IdentifierType> id) {
			this.id = id;
		}
		
	    public List<IdentifierType> getID() {
	        if (id == null) {
	            id = new ArrayList<IdentifierType>();
	        }
	        return this.id;
	    }

}
