
package com.volkswagenag.xmldefs.dd.workshoporder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für OrderValueType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OrderValueType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Amounts" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}AmountsType" minOccurs="0"/>
 *         &lt;element name="TaxAmounts" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}TaxAmountsType" minOccurs="0"/>
 *         &lt;element name="WagesAmounts" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}WagesAmountsType" minOccurs="0"/>
 *         &lt;element name="MaterialAmounts" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}MaterialAmountsType" minOccurs="0"/>
 *         &lt;element name="ExchangePartsAmounts" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}ExchangePartsAmountsType" minOccurs="0"/>
 *         &lt;element name="ExchangePartsTaxAmounts" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}ExchangePartsTaxAmountsType" minOccurs="0"/>
 *         &lt;element name="AgencyGoodsAmounts" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}AgencyGoodsAmountsType" minOccurs="0"/>
 *         &lt;element name="Invoice" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}InvoiceType" minOccurs="0"/>
 *         &lt;element name="Claim" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}ClaimType" minOccurs="0"/>
 *         &lt;element name="Type" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderValueType", propOrder = {
    "amounts",
    "taxAmounts",
    "wagesAmounts",
    "materialAmounts",
    "exchangePartsAmounts",
    "exchangePartsTaxAmounts",
    "agencyGoodsAmounts",
    "invoice",
    "claim",
    "type"
})
public class OrderValueType {

    @XmlElement(name = "Amounts")
    protected AmountsType amounts;
    @XmlElement(name = "TaxAmounts")
    protected TaxAmountsType taxAmounts;
    @XmlElement(name = "WagesAmounts")
    protected WagesAmountsType wagesAmounts;
    @XmlElement(name = "MaterialAmounts")
    protected MaterialAmountsType materialAmounts;
    @XmlElement(name = "ExchangePartsAmounts")
    protected ExchangePartsAmountsType exchangePartsAmounts;
    @XmlElement(name = "ExchangePartsTaxAmounts")
    protected ExchangePartsTaxAmountsType exchangePartsTaxAmounts;
    @XmlElement(name = "AgencyGoodsAmounts")
    protected AgencyGoodsAmountsType agencyGoodsAmounts;
    @XmlElement(name = "Invoice")
    protected InvoiceType invoice;
    @XmlElement(name = "Claim")
    protected ClaimType claim;
    @XmlElement(name = "Type")
    protected CodeType type;

    /**
     * Ruft den Wert der amounts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AmountsType }
     *     
     */
    public AmountsType getAmounts() {
        return amounts;
    }

    /**
     * Legt den Wert der amounts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountsType }
     *     
     */
    public void setAmounts(AmountsType value) {
        this.amounts = value;
    }

    /**
     * Ruft den Wert der taxAmounts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TaxAmountsType }
     *     
     */
    public TaxAmountsType getTaxAmounts() {
        return taxAmounts;
    }

    /**
     * Legt den Wert der taxAmounts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxAmountsType }
     *     
     */
    public void setTaxAmounts(TaxAmountsType value) {
        this.taxAmounts = value;
    }

    /**
     * Ruft den Wert der wagesAmounts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WagesAmountsType }
     *     
     */
    public WagesAmountsType getWagesAmounts() {
        return wagesAmounts;
    }

    /**
     * Legt den Wert der wagesAmounts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WagesAmountsType }
     *     
     */
    public void setWagesAmounts(WagesAmountsType value) {
        this.wagesAmounts = value;
    }

    /**
     * Ruft den Wert der materialAmounts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MaterialAmountsType }
     *     
     */
    public MaterialAmountsType getMaterialAmounts() {
        return materialAmounts;
    }

    /**
     * Legt den Wert der materialAmounts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MaterialAmountsType }
     *     
     */
    public void setMaterialAmounts(MaterialAmountsType value) {
        this.materialAmounts = value;
    }

    /**
     * Ruft den Wert der exchangePartsAmounts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExchangePartsAmountsType }
     *     
     */
    public ExchangePartsAmountsType getExchangePartsAmounts() {
        return exchangePartsAmounts;
    }

    /**
     * Legt den Wert der exchangePartsAmounts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangePartsAmountsType }
     *     
     */
    public void setExchangePartsAmounts(ExchangePartsAmountsType value) {
        this.exchangePartsAmounts = value;
    }

    /**
     * Ruft den Wert der exchangePartsTaxAmounts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExchangePartsTaxAmountsType }
     *     
     */
    public ExchangePartsTaxAmountsType getExchangePartsTaxAmounts() {
        return exchangePartsTaxAmounts;
    }

    /**
     * Legt den Wert der exchangePartsTaxAmounts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangePartsTaxAmountsType }
     *     
     */
    public void setExchangePartsTaxAmounts(ExchangePartsTaxAmountsType value) {
        this.exchangePartsTaxAmounts = value;
    }

    /**
     * Ruft den Wert der agencyGoodsAmounts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AgencyGoodsAmountsType }
     *     
     */
    public AgencyGoodsAmountsType getAgencyGoodsAmounts() {
        return agencyGoodsAmounts;
    }

    /**
     * Legt den Wert der agencyGoodsAmounts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AgencyGoodsAmountsType }
     *     
     */
    public void setAgencyGoodsAmounts(AgencyGoodsAmountsType value) {
        this.agencyGoodsAmounts = value;
    }

    /**
     * Ruft den Wert der invoice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InvoiceType }
     *     
     */
    public InvoiceType getInvoice() {
        return invoice;
    }

    /**
     * Legt den Wert der invoice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InvoiceType }
     *     
     */
    public void setInvoice(InvoiceType value) {
        this.invoice = value;
    }

    /**
     * Ruft den Wert der claim-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ClaimType }
     *     
     */
    public ClaimType getClaim() {
        return claim;
    }

    /**
     * Legt den Wert der claim-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimType }
     *     
     */
    public void setClaim(ClaimType value) {
        this.claim = value;
    }

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setType(CodeType value) {
        this.type = value;
    }

}
