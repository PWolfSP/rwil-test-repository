
package com.volkswagenag.xmldefs.dd.workshoporder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ExchangePartsAmountsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ExchangePartsAmountsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExchangePartsAmount" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}PriceAmountType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExchangePartsAmountsType", propOrder = {
    "exchangePartsAmount"
})



public class ExchangePartsAmountsType {
	
	
    @XmlElement(name = "ExchangePartsAmount", required = true)
    private List<PriceAmountType> exchangePartsAmount;


		public void setPriceAmountType(List<PriceAmountType> exchangePartsAmount) {
			this.exchangePartsAmount = exchangePartsAmount;
		}
		
	    public List<PriceAmountType> getExchangePartsAmount() {
	        if (exchangePartsAmount == null) {
	            exchangePartsAmount = new ArrayList<PriceAmountType>();
	        }
	        return this.exchangePartsAmount;
	    }

}
