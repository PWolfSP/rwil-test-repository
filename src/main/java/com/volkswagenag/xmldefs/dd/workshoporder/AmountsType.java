
package com.volkswagenag.xmldefs.dd.workshoporder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für AmountsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AmountsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Amount" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}PriceAmountType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AmountsType", propOrder = {
    "amount"
})



public class AmountsType {
	
	
    @XmlElement(name = "Amount", required = true)
    private List<PriceAmountType> amount;


		public void setPriceAmountType(List<PriceAmountType> amount) {
			this.amount = amount;
		}
		
	    public List<PriceAmountType> getAmount() {
	        if (amount == null) {
	            amount = new ArrayList<PriceAmountType>();
	        }
	        return this.amount;
	    }

}
