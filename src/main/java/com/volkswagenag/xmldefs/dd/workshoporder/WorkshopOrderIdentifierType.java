
package com.volkswagenag.xmldefs.dd.workshoporder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse f�r WorkshopOrderIdentifierType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="WorkshopOrderIdentifierType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WorkshopOrderUID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="ApplicationIdentifiers" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}ApplicationIdentifiersType" minOccurs="0"/>
 *         &lt;element name="Dealer" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}DealerType" minOccurs="0"/>
 *         &lt;element name="AcceptanceDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkshopOrderIdentifierType", propOrder = {
    "workshopOrderUID",
    "applicationIdentifiers",
    "dealer",
    "acceptanceDate"
})
public class WorkshopOrderIdentifierType {

    @XmlElement(name = "WorkshopOrderUID")
    protected IdentifierType workshopOrderUID;
    @XmlElement(name = "ApplicationIdentifiers")
    protected ApplicationIdentifiersType applicationIdentifiers;
    @XmlElement(name = "Dealer")
    protected DealerType dealer;
    @XmlElement(name = "AcceptanceDate")
    protected String acceptanceDate;

    /**
     * Ruft den Wert der workshopOrderUID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getWorkshopOrderUID() {
        return workshopOrderUID;
    }

    /**
     * Legt den Wert der workshopOrderUID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setWorkshopOrderUID(IdentifierType value) {
        this.workshopOrderUID = value;
    }

    /**
     * Ruft den Wert der applicationIdentifiers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationIdentifiersType }
     *     
     */
    public ApplicationIdentifiersType getApplicationIdentifiers() {
        return applicationIdentifiers;
    }

    /**
     * Legt den Wert der applicationIdentifiers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationIdentifiersType }
     *     
     */
    public void setApplicationIdentifiers(ApplicationIdentifiersType value) {
        this.applicationIdentifiers = value;
    }

    /**
     * Ruft den Wert der dealer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DealerType }
     *     
     */
    public DealerType getDealer() {
        return dealer;
    }

    /**
     * Legt den Wert der dealer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DealerType }
     *     
     */
    public void setDealer(DealerType value) {
        this.dealer = value;
    }

    /**
     * Ruft den Wert der acceptanceDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcceptanceDate() {
        return acceptanceDate;
    }

    /**
     * Legt den Wert der acceptanceDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcceptanceDate(String value) {
        this.acceptanceDate = value;
    }

}
