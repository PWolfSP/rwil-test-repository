
package com.volkswagenag.xmldefs.dd.workshoporder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für MaterialAmountsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MaterialAmountsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MaterialAmount" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}PriceAmountType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MaterialAmountsType", propOrder = {
    "materialAmount"
})



public class MaterialAmountsType {
	
	
    @XmlElement(name = "MaterialAmount", required = true)
    private List<PriceAmountType> materialAmount;


		public void setPriceAmountType(List<PriceAmountType> materialAmount) {
			this.materialAmount = materialAmount;
		}
		
	    public List<PriceAmountType> getMaterialAmount() {
	        if (materialAmount == null) {
	            materialAmount = new ArrayList<PriceAmountType>();
	        }
	        return this.materialAmount;
	    }

}
