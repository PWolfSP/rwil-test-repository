
package com.volkswagenag.xmldefs.dd.workshoporder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.vehicle.VehicleIdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für VehiclesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VehiclesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/Vehicle}VehicleRef" maxOccurs="2"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehiclesType", propOrder = {
    "vehicleRef"
})



public class VehiclesType {
	
	
    @XmlElement(name = "VehicleRef", namespace = "http://xmldefs.volkswagenag.com/DD/Vehicle", required = true)
    private List<VehicleIdentifierType> vehicleRef;


		public void setVehicleIdentifierType(List<VehicleIdentifierType> vehicleRef) {
			this.vehicleRef = vehicleRef;
		}
		
	    public List<VehicleIdentifierType> getVehicleRef() {
	        if (vehicleRef == null) {
	            vehicleRef = new ArrayList<VehicleIdentifierType>();
	        }
	        return this.vehicleRef;
	    }

}
