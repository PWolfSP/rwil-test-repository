
package com.volkswagenag.xmldefs.dd.workshoporder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für TaxAmountsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TaxAmountsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TaxAmount" type="{http://xmldefs.volkswagenag.com/DD/WorkshopOrder}TaxAmountType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxAmountsType", propOrder = {
    "taxAmount"
})



public class TaxAmountsType {
	
	
    @XmlElement(name = "TaxAmount", required = true)
    private List<TaxAmountType> taxAmount;


		public void setTaxAmountType(List<TaxAmountType> taxAmount) {
			this.taxAmount = taxAmount;
		}
		
	    public List<TaxAmountType> getTaxAmount() {
	        if (taxAmount == null) {
	            taxAmount = new ArrayList<TaxAmountType>();
	        }
	        return this.taxAmount;
	    }

}
