
package com.volkswagenag.xmldefs.dd.repairdiagnosticprotocol;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * Time stamps of diagnostic protocol
 * 
 * <p>Java-Klasse f�r ProtocolTimestampsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProtocolTimestampsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreationDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateTimeType" minOccurs="0"/>
 *         &lt;element name="DiagnosticStartDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateTimeType" minOccurs="0"/>
 *         &lt;element name="DiagnosticElapsedTimeUnits" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProtocolTimestampsType", propOrder = {
    "creationDate",
    "diagnosticStartDate",
    "diagnosticElapsedTimeUnits"
})
public class ProtocolTimestampsType {

    @XmlElement(name = "CreationDate")
    protected String creationDate;
    @XmlElement(name = "DiagnosticStartDate")
    protected String diagnosticStartDate;
    @XmlElement(name = "DiagnosticElapsedTimeUnits")
    protected BigDecimal diagnosticElapsedTimeUnits;

    /**
     * Ruft den Wert der creationDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * Legt den Wert der creationDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreationDate(String value) {
        this.creationDate = value;
    }

    /**
     * Ruft den Wert der diagnosticStartDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiagnosticStartDate() {
        return diagnosticStartDate;
    }

    /**
     * Legt den Wert der diagnosticStartDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiagnosticStartDate(String value) {
        this.diagnosticStartDate = value;
    }

    /**
     * Ruft den Wert der diagnosticElapsedTimeUnits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiagnosticElapsedTimeUnits() {
        return diagnosticElapsedTimeUnits;
    }

    /**
     * Legt den Wert der diagnosticElapsedTimeUnits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiagnosticElapsedTimeUnits(BigDecimal value) {
        this.diagnosticElapsedTimeUnits = value;
    }

}
