
package com.volkswagenag.xmldefs.dd.efacode;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für PictureMappingType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PictureMappingType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PictureID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType"/>
 *         &lt;element name="Position" type="{http://xmldefs.volkswagenag.com/DD/EFACode}PositionType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PictureTitle" type="{http://xmldefs.volkswagenag.com/DD/EFACode}TitleType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PictureMappingType", propOrder = {
    "pictureID",
    "position",
    "pictureTitle"
})



public class PictureMappingType {
	
	
    @XmlElement(name = "PictureID", required = true)
    protected IdentifierType pictureID;
    @XmlElement(name = "Position")
    private List<PositionType> position;
    @XmlElement(name = "PictureTitle")
    private List<TitleType> pictureTitle;

    /**
     * Ruft den Wert der pictureID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getPictureID() {
        return pictureID;
    }

    /**
     * Legt den Wert der pictureID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setPictureID(IdentifierType value) {
        this.pictureID = value;
    }

    /**
     * Gets the value of the position property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the position property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPosition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PositionType }
     * 
     * 
     */
    public List<PositionType> getPosition() {
        if (position == null) {
            position = new ArrayList<PositionType>();
        }
        return this.position;
    }


		public void setTitleType(List<TitleType> pictureTitle) {
			this.pictureTitle = pictureTitle;
		}
		
	    public List<TitleType> getPictureTitle() {
	        if (pictureTitle == null) {
	            pictureTitle = new ArrayList<TitleType>();
	        }
	        return this.pictureTitle;
	    }

}
