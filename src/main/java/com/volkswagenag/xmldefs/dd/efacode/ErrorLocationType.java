
package com.volkswagenag.xmldefs.dd.efacode;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ErrorLocationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ErrorLocationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LocationID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType"/>
 *         &lt;element name="LocationTitle" type="{http://xmldefs.volkswagenag.com/DD/EFACode}TitleType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErrorLocationType", propOrder = {
    "locationID",
    "locationTitle"
})



public class ErrorLocationType {
	
	
    @XmlElement(name = "LocationID", required = true)
    protected IdentifierType locationID;
    @XmlElement(name = "LocationTitle")
    private List<TitleType> locationTitle;

    /**
     * Ruft den Wert der locationID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getLocationID() {
        return locationID;
    }

    /**
     * Legt den Wert der locationID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setLocationID(IdentifierType value) {
        this.locationID = value;
    }


		public void setTitleType(List<TitleType> locationTitle) {
			this.locationTitle = locationTitle;
		}
		
	    public List<TitleType> getLocationTitle() {
	        if (locationTitle == null) {
	            locationTitle = new ArrayList<TitleType>();
	        }
	        return this.locationTitle;
	    }

}
