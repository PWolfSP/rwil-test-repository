
package com.volkswagenag.xmldefs.dd.efacode;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * CodeType identifiert die Typen der Kodierung. Zur Zeit unterscheiden wir zwischen -
 *           Kundencodierung - Werkstattcodierung - vorlaeufiger Werkstattcodierung und - vorlaeufiger Kodierung. Die
 *           Kundencodierung und Werkstattcodierung sind vollstaendige Kodierungen aus Kunden- bzw. Werkstattsicht. Die
 *           vorlaeufige Werkstattcodierung ist eine unvollstaendige Kodierung, die in einigen Prozessschritten benoetigt wird.
 *           Die vorlaeufige Kodierung ist eine unvollstaendige Kodierung, die alle uebrigen Kodierungsvarianten
 *           abdeckt.
 * 
 * <p>Java-Klasse f�r EFACodeType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EFACodeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EFACodeIdentifier" type="{http://xmldefs.volkswagenag.com/DD/EFACode}EFACodeIdentifierType" minOccurs="0"/>
 *         &lt;element name="CodeType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType"/>
 *         &lt;element name="ErrorObject" type="{http://xmldefs.volkswagenag.com/DD/EFACode}ErrorObjectType" minOccurs="0"/>
 *         &lt;element name="Condition" type="{http://xmldefs.volkswagenag.com/DD/EFACode}ConditionType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ReleaseDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EFACodeType", propOrder = {
    "efaCodeIdentifier",
    "codeType",
    "errorObject",
    "condition",
    "releaseDate"
})



public class EFACodeType {
	
	
    @XmlElement(name = "EFACodeIdentifier")
    protected EFACodeIdentifierType efaCodeIdentifier;
    @XmlElement(name = "CodeType", required = true)
    protected CodeType codeType;
    @XmlElement(name = "ErrorObject")
    protected ErrorObjectType errorObject;
    @XmlElement(name = "Condition")
    private List<ConditionType> condition;
    @XmlElement(name = "ReleaseDate")
    protected String releaseDate;

    /**
     * Ruft den Wert der efaCodeIdentifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EFACodeIdentifierType }
     *     
     */
    public EFACodeIdentifierType getEFACodeIdentifier() {
        return efaCodeIdentifier;
    }

    /**
     * Legt den Wert der efaCodeIdentifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EFACodeIdentifierType }
     *     
     */
    public void setEFACodeIdentifier(EFACodeIdentifierType value) {
        this.efaCodeIdentifier = value;
    }

    /**
     * Ruft den Wert der codeType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getCodeType() {
        return codeType;
    }

    /**
     * Legt den Wert der codeType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setCodeType(CodeType value) {
        this.codeType = value;
    }

    /**
     * Ruft den Wert der errorObject-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ErrorObjectType }
     *     
     */
    public ErrorObjectType getErrorObject() {
        return errorObject;
    }

    /**
     * Legt den Wert der errorObject-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorObjectType }
     *     
     */
    public void setErrorObject(ErrorObjectType value) {
        this.errorObject = value;
    }


		public void setConditionType(List<ConditionType> condition) {
			this.condition = condition;
		}
		
	    public List<ConditionType> getCondition() {
	        if (condition == null) {
	            condition = new ArrayList<ConditionType>();
	        }
	        return this.condition;
	    }

}
