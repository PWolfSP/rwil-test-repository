
package com.volkswagenag.xmldefs.dd.efacode;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ErrorTypeType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ErrorTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TypeID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType"/>
 *         &lt;element name="TypeTitle" type="{http://xmldefs.volkswagenag.com/DD/EFACode}TitleType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErrorTypeType", propOrder = {
    "typeID",
    "typeTitle"
})



public class ErrorTypeType {
	
	
    @XmlElement(name = "TypeID", required = true)
    protected IdentifierType typeID;
    @XmlElement(name = "TypeTitle")
    private List<TitleType> typeTitle;

    /**
     * Ruft den Wert der typeID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getTypeID() {
        return typeID;
    }

    /**
     * Legt den Wert der typeID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setTypeID(IdentifierType value) {
        this.typeID = value;
    }


		public void setTitleType(List<TitleType> typeTitle) {
			this.typeTitle = typeTitle;
		}
		
	    public List<TitleType> getTypeTitle() {
	        if (typeTitle == null) {
	            typeTitle = new ArrayList<TitleType>();
	        }
	        return this.typeTitle;
	    }

}
