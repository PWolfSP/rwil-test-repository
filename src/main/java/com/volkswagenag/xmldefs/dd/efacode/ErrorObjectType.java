
package com.volkswagenag.xmldefs.dd.efacode;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ErrorObjectType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ErrorObjectType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObjectID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType"/>
 *         &lt;element name="ObjectTitle" type="{http://xmldefs.volkswagenag.com/DD/EFACode}ObjectTitleType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ErrorType" type="{http://xmldefs.volkswagenag.com/DD/EFACode}ErrorTypeType" minOccurs="0"/>
 *         &lt;element name="ErrorLocation" type="{http://xmldefs.volkswagenag.com/DD/EFACode}ErrorLocationType" minOccurs="0"/>
 *         &lt;element name="ErrorCondition" type="{http://xmldefs.volkswagenag.com/DD/EFACode}ConditionType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PictureMapping" type="{http://xmldefs.volkswagenag.com/DD/EFACode}PictureMappingType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErrorObjectType", propOrder = {
    "objectID",
    "objectTitle",
    "errorType",
    "errorLocation",
    "errorCondition",
    "pictureMapping"
})



public class ErrorObjectType {
	
	
    @XmlElement(name = "ObjectID", required = true)
    protected IdentifierType objectID;
    @XmlElement(name = "ObjectTitle")
    private List<ObjectTitleType> objectTitle;
    @XmlElement(name = "ErrorType")
    protected ErrorTypeType errorType;
    @XmlElement(name = "ErrorLocation")
    protected ErrorLocationType errorLocation;
    @XmlElement(name = "ErrorCondition")
    private List<ConditionType> errorCondition;
    @XmlElement(name = "PictureMapping")
    protected PictureMappingType pictureMapping;

    /**
     * Ruft den Wert der objectID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getObjectID() {
        return objectID;
    }

    /**
     * Legt den Wert der objectID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setObjectID(IdentifierType value) {
        this.objectID = value;
    }

    /**
     * Gets the value of the objectTitle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the objectTitle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObjectTitle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectTitleType }
     * 
     * 
     */
    public List<ObjectTitleType> getObjectTitle() {
        if (objectTitle == null) {
            objectTitle = new ArrayList<ObjectTitleType>();
        }
        return this.objectTitle;
    }

    /**
     * Ruft den Wert der errorType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ErrorTypeType }
     *     
     */
    public ErrorTypeType getErrorType() {
        return errorType;
    }

    /**
     * Legt den Wert der errorType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorTypeType }
     *     
     */
    public void setErrorType(ErrorTypeType value) {
        this.errorType = value;
    }

    /**
     * Ruft den Wert der errorLocation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ErrorLocationType }
     *     
     */
    public ErrorLocationType getErrorLocation() {
        return errorLocation;
    }

    /**
     * Legt den Wert der errorLocation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorLocationType }
     *     
     */
    public void setErrorLocation(ErrorLocationType value) {
        this.errorLocation = value;
    }


		public void setConditionType(List<ConditionType> errorCondition) {
			this.errorCondition = errorCondition;
		}
		
	    public List<ConditionType> getErrorCondition() {
	        if (errorCondition == null) {
	            errorCondition = new ArrayList<ConditionType>();
	        }
	        return this.errorCondition;
	    }

}
