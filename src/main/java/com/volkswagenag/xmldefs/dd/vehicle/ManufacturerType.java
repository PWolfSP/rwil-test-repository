
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ManufacturerType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ManufacturerType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BrandCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ManufacturerIdents" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ManufacturerIdentsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ManufacturerType", propOrder = {
    "brandCode",
    "manufacturerIdents"
})
public class ManufacturerType {

    @XmlElement(name = "BrandCode")
    protected CodeType brandCode;
    @XmlElement(name = "ManufacturerIdents")
    protected ManufacturerIdentsType manufacturerIdents;

    /**
     * Ruft den Wert der brandCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getBrandCode() {
        return brandCode;
    }

    /**
     * Legt den Wert der brandCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setBrandCode(CodeType value) {
        this.brandCode = value;
    }

    /**
     * Ruft den Wert der manufacturerIdents-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ManufacturerIdentsType }
     *     
     */
    public ManufacturerIdentsType getManufacturerIdents() {
        return manufacturerIdents;
    }

    /**
     * Legt den Wert der manufacturerIdents-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ManufacturerIdentsType }
     *     
     */
    public void setManufacturerIdents(ManufacturerIdentsType value) {
        this.manufacturerIdents = value;
    }

}
