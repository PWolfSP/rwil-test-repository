
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für TracksType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TracksType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Track" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}TrackType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TracksType", propOrder = {
    "track"
})



public class TracksType {
	
	
    @XmlElement(name = "Track", required = true)
    private List<TrackType> track;


		public void setTrackType(List<TrackType> track) {
			this.track = track;
		}
		
	    public List<TrackType> getTrack() {
	        if (track == null) {
	            track = new ArrayList<TrackType>();
	        }
	        return this.track;
	    }

}
