
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;
import com.volkswagenag.xmldefs.dd.basictypes.TextType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ComponentType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ComponentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType"/>
 *         &lt;element name="SerialNo" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}TextType" minOccurs="0"/>
 *         &lt;element name="Type" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ParametrizeIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="FlashingType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ClampStateIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="HWUpdateNeededIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="SWUpdateNeededIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="CodingNeededIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="ParametrizingNeededIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="ECUPart" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}PartType" minOccurs="0"/>
 *         &lt;element name="HWPart" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}PartType" minOccurs="0"/>
 *         &lt;element name="SWPart" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}PartType" minOccurs="0"/>
 *         &lt;element name="HWVersion" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="SWVersion" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="Protocol" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="Codings" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}CodingsType" minOccurs="0"/>
 *         &lt;element name="Parametrizings" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ParametrizingsType" minOccurs="0"/>
 *         &lt;element name="Flashings" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}FlashingsType" minOccurs="0"/>
 *         &lt;element name="Notes" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}NotesType" minOccurs="0"/>
 *         &lt;element name="LastModified" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}QualifiedDateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComponentType", propOrder = {
    "id",
    "serialNo",
    "description",
    "type",
    "parametrizeIND",
    "flashingType",
    "clampStateIND",
    "hwUpdateNeededIND",
    "swUpdateNeededIND",
    "codingNeededIND",
    "parametrizingNeededIND",
    "ecuPart",
    "hwPart",
    "swPart",
    "hwVersion",
    "swVersion",
    "protocol",
    "codings",
    "parametrizings",
    "flashings",
    "notes",
    "lastModified"
})
public class ComponentType {

    @XmlElement(name = "ID", required = true)
    protected IdentifierType id;
    @XmlElement(name = "SerialNo")
    protected IdentifierType serialNo;
    @XmlElement(name = "Description")
    protected TextType description;
    @XmlElement(name = "Type")
    protected CodeType type;
    @XmlElement(name = "ParametrizeIND", defaultValue = "false")
    protected Boolean parametrizeIND;
    @XmlElement(name = "FlashingType")
    protected CodeType flashingType;
    @XmlElement(name = "ClampStateIND")
    protected Boolean clampStateIND;
    @XmlElement(name = "HWUpdateNeededIND")
    protected Boolean hwUpdateNeededIND;
    @XmlElement(name = "SWUpdateNeededIND")
    protected Boolean swUpdateNeededIND;
    @XmlElement(name = "CodingNeededIND")
    protected Boolean codingNeededIND;
    @XmlElement(name = "ParametrizingNeededIND")
    protected Boolean parametrizingNeededIND;
    @XmlElement(name = "ECUPart")
    protected PartType ecuPart;
    @XmlElement(name = "HWPart")
    protected PartType hwPart;
    @XmlElement(name = "SWPart")
    protected PartType swPart;
    @XmlElement(name = "HWVersion")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String hwVersion;
    @XmlElement(name = "SWVersion")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String swVersion;
    @XmlElement(name = "Protocol")
    protected CodeType protocol;
    @XmlElement(name = "Codings")
    protected CodingsType codings;
    @XmlElement(name = "Parametrizings")
    protected ParametrizingsType parametrizings;
    @XmlElement(name = "Flashings")
    protected FlashingsType flashings;
    @XmlElement(name = "Notes")
    protected NotesType notes;
    @XmlElement(name = "LastModified")
    protected QualifiedDateType lastModified;

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getID() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setID(IdentifierType value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der serialNo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getSerialNo() {
        return serialNo;
    }

    /**
     * Legt den Wert der serialNo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setSerialNo(IdentifierType value) {
        this.serialNo = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setDescription(TextType value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setType(CodeType value) {
        this.type = value;
    }

    /**
     * Ruft den Wert der parametrizeIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isParametrizeIND() {
        return parametrizeIND;
    }

    /**
     * Legt den Wert der parametrizeIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setParametrizeIND(Boolean value) {
        this.parametrizeIND = value;
    }

    /**
     * Ruft den Wert der flashingType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getFlashingType() {
        return flashingType;
    }

    /**
     * Legt den Wert der flashingType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setFlashingType(CodeType value) {
        this.flashingType = value;
    }

    /**
     * Ruft den Wert der clampStateIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isClampStateIND() {
        return clampStateIND;
    }

    /**
     * Legt den Wert der clampStateIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setClampStateIND(Boolean value) {
        this.clampStateIND = value;
    }

    /**
     * Ruft den Wert der hwUpdateNeededIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHWUpdateNeededIND() {
        return hwUpdateNeededIND;
    }

    /**
     * Legt den Wert der hwUpdateNeededIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHWUpdateNeededIND(Boolean value) {
        this.hwUpdateNeededIND = value;
    }

    /**
     * Ruft den Wert der swUpdateNeededIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSWUpdateNeededIND() {
        return swUpdateNeededIND;
    }

    /**
     * Legt den Wert der swUpdateNeededIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSWUpdateNeededIND(Boolean value) {
        this.swUpdateNeededIND = value;
    }

    /**
     * Ruft den Wert der codingNeededIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCodingNeededIND() {
        return codingNeededIND;
    }

    /**
     * Legt den Wert der codingNeededIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCodingNeededIND(Boolean value) {
        this.codingNeededIND = value;
    }

    /**
     * Ruft den Wert der parametrizingNeededIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isParametrizingNeededIND() {
        return parametrizingNeededIND;
    }

    /**
     * Legt den Wert der parametrizingNeededIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setParametrizingNeededIND(Boolean value) {
        this.parametrizingNeededIND = value;
    }

    /**
     * Ruft den Wert der ecuPart-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartType }
     *     
     */
    public PartType getECUPart() {
        return ecuPart;
    }

    /**
     * Legt den Wert der ecuPart-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartType }
     *     
     */
    public void setECUPart(PartType value) {
        this.ecuPart = value;
    }

    /**
     * Ruft den Wert der hwPart-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartType }
     *     
     */
    public PartType getHWPart() {
        return hwPart;
    }

    /**
     * Legt den Wert der hwPart-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartType }
     *     
     */
    public void setHWPart(PartType value) {
        this.hwPart = value;
    }

    /**
     * Ruft den Wert der swPart-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartType }
     *     
     */
    public PartType getSWPart() {
        return swPart;
    }

    /**
     * Legt den Wert der swPart-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartType }
     *     
     */
    public void setSWPart(PartType value) {
        this.swPart = value;
    }

    /**
     * Ruft den Wert der hwVersion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHWVersion() {
        return hwVersion;
    }

    /**
     * Legt den Wert der hwVersion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHWVersion(String value) {
        this.hwVersion = value;
    }

    /**
     * Ruft den Wert der swVersion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSWVersion() {
        return swVersion;
    }

    /**
     * Legt den Wert der swVersion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSWVersion(String value) {
        this.swVersion = value;
    }

    /**
     * Ruft den Wert der protocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getProtocol() {
        return protocol;
    }

    /**
     * Legt den Wert der protocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setProtocol(CodeType value) {
        this.protocol = value;
    }

    /**
     * Ruft den Wert der codings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodingsType }
     *     
     */
    public CodingsType getCodings() {
        return codings;
    }

    /**
     * Legt den Wert der codings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodingsType }
     *     
     */
    public void setCodings(CodingsType value) {
        this.codings = value;
    }

    /**
     * Ruft den Wert der parametrizings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ParametrizingsType }
     *     
     */
    public ParametrizingsType getParametrizings() {
        return parametrizings;
    }

    /**
     * Legt den Wert der parametrizings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ParametrizingsType }
     *     
     */
    public void setParametrizings(ParametrizingsType value) {
        this.parametrizings = value;
    }

    /**
     * Ruft den Wert der flashings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FlashingsType }
     *     
     */
    public FlashingsType getFlashings() {
        return flashings;
    }

    /**
     * Legt den Wert der flashings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FlashingsType }
     *     
     */
    public void setFlashings(FlashingsType value) {
        this.flashings = value;
    }

    /**
     * Ruft den Wert der notes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NotesType }
     *     
     */
    public NotesType getNotes() {
        return notes;
    }

    /**
     * Legt den Wert der notes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NotesType }
     *     
     */
    public void setNotes(NotesType value) {
        this.notes = value;
    }

    /**
     * Ruft den Wert der lastModified-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QualifiedDateType }
     *     
     */
    public QualifiedDateType getLastModified() {
        return lastModified;
    }

    /**
     * Legt den Wert der lastModified-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QualifiedDateType }
     *     
     */
    public void setLastModified(QualifiedDateType value) {
        this.lastModified = value;
    }

}
