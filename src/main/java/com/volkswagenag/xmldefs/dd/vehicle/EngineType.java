
package com.volkswagenag.xmldefs.dd.vehicle;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;
import com.volkswagenag.xmldefs.dd.basictypes.MeasureType;
import com.volkswagenag.xmldefs.dd.basictypes.TextType;

import lombok.Generated;


/**
 * <p>Java-Klasse für EngineType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EngineType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EngineNo" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="EngineCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="EngineCodeExtension" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="EngineType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ProductionDates" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ProductionDatesType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}TextType" minOccurs="0"/>
 *         &lt;element name="CylinderCNT" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *         &lt;element name="Capacity" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *         &lt;element name="Power" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *         &lt;element name="PollutantClasses" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}PollutantClassesType" minOccurs="0"/>
 *         &lt;element name="Emissions" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}EmissionsType" minOccurs="0"/>
 *         &lt;element name="Consumptions" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ConsumptionsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EngineType", propOrder = {
    "engineNo",
    "engineCode",
    "engineCodeExtension",
    "engineType",
    "productionDates",
    "description",
    "cylinderCNT",
    "capacity",
    "power",
    "pollutantClasses",
    "emissions",
    "consumptions"
})
public class EngineType {

    @XmlElement(name = "EngineNo")
    protected IdentifierType engineNo;
    @XmlElement(name = "EngineCode")
    protected CodeType engineCode;
    @XmlElement(name = "EngineCodeExtension")
    protected CodeType engineCodeExtension;
    @XmlElement(name = "EngineType")
    protected CodeType engineType;
    @XmlElement(name = "ProductionDates")
    protected ProductionDatesType productionDates;
    @XmlElement(name = "Description")
    protected TextType description;
    @XmlElement(name = "CylinderCNT")
    protected BigDecimal cylinderCNT;
    @XmlElement(name = "Capacity")
    protected MeasureType capacity;
    @XmlElement(name = "Power")
    protected MeasureType power;
    @XmlElement(name = "PollutantClasses")
    protected PollutantClassesType pollutantClasses;
    @XmlElement(name = "Emissions")
    protected EmissionsType emissions;
    @XmlElement(name = "Consumptions")
    protected ConsumptionsType consumptions;

    /**
     * Ruft den Wert der engineNo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getEngineNo() {
        return engineNo;
    }

    /**
     * Legt den Wert der engineNo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setEngineNo(IdentifierType value) {
        this.engineNo = value;
    }

    /**
     * Ruft den Wert der engineCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getEngineCode() {
        return engineCode;
    }

    /**
     * Legt den Wert der engineCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setEngineCode(CodeType value) {
        this.engineCode = value;
    }

    /**
     * Ruft den Wert der engineCodeExtension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getEngineCodeExtension() {
        return engineCodeExtension;
    }

    /**
     * Legt den Wert der engineCodeExtension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setEngineCodeExtension(CodeType value) {
        this.engineCodeExtension = value;
    }

    /**
     * Ruft den Wert der engineType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getEngineType() {
        return engineType;
    }

    /**
     * Legt den Wert der engineType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setEngineType(CodeType value) {
        this.engineType = value;
    }

    /**
     * Ruft den Wert der productionDates-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProductionDatesType }
     *     
     */
    public ProductionDatesType getProductionDates() {
        return productionDates;
    }

    /**
     * Legt den Wert der productionDates-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductionDatesType }
     *     
     */
    public void setProductionDates(ProductionDatesType value) {
        this.productionDates = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setDescription(TextType value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der cylinderCNT-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCylinderCNT() {
        return cylinderCNT;
    }

    /**
     * Legt den Wert der cylinderCNT-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCylinderCNT(BigDecimal value) {
        this.cylinderCNT = value;
    }

    /**
     * Ruft den Wert der capacity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getCapacity() {
        return capacity;
    }

    /**
     * Legt den Wert der capacity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setCapacity(MeasureType value) {
        this.capacity = value;
    }

    /**
     * Ruft den Wert der power-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getPower() {
        return power;
    }

    /**
     * Legt den Wert der power-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setPower(MeasureType value) {
        this.power = value;
    }

    /**
     * Ruft den Wert der pollutantClasses-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PollutantClassesType }
     *     
     */
    public PollutantClassesType getPollutantClasses() {
        return pollutantClasses;
    }

    /**
     * Legt den Wert der pollutantClasses-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PollutantClassesType }
     *     
     */
    public void setPollutantClasses(PollutantClassesType value) {
        this.pollutantClasses = value;
    }

    /**
     * Ruft den Wert der emissions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmissionsType }
     *     
     */
    public EmissionsType getEmissions() {
        return emissions;
    }

    /**
     * Legt den Wert der emissions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmissionsType }
     *     
     */
    public void setEmissions(EmissionsType value) {
        this.emissions = value;
    }

    /**
     * Ruft den Wert der consumptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ConsumptionsType }
     *     
     */
    public ConsumptionsType getConsumptions() {
        return consumptions;
    }

    /**
     * Legt den Wert der consumptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsumptionsType }
     *     
     */
    public void setConsumptions(ConsumptionsType value) {
        this.consumptions = value;
    }

}
