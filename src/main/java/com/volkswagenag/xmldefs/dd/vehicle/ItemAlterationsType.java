
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ItemAlterationsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ItemAlterationsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemAlteration" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ItemAlterationType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemAlterationsType", propOrder = {
    "itemAlteration"
})



public class ItemAlterationsType {
	
	
    @XmlElement(name = "ItemAlteration", required = true)
    private List<ItemAlterationType> itemAlteration;


		public void setItemAlterationType(List<ItemAlterationType> itemAlteration) {
			this.itemAlteration = itemAlteration;
		}
		
	    public List<ItemAlterationType> getItemAlteration() {
	        if (itemAlteration == null) {
	            itemAlteration = new ArrayList<ItemAlterationType>();
	        }
	        return this.itemAlteration;
	    }

}
