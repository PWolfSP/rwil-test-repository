
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für EquipmentOptionsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EquipmentOptionsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EquipmentOption" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}EquipmentOptionType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentOptionsType", propOrder = {
    "equipmentOption"
})



public class EquipmentOptionsType {
	
	
    @XmlElement(name = "EquipmentOption", required = true)
    private List<EquipmentOptionType> equipmentOption;


		public void setEquipmentOptionType(List<EquipmentOptionType> equipmentOption) {
			this.equipmentOption = equipmentOption;
		}
		
	    public List<EquipmentOptionType> getEquipmentOption() {
	        if (equipmentOption == null) {
	            equipmentOption = new ArrayList<EquipmentOptionType>();
	        }
	        return this.equipmentOption;
	    }

}
