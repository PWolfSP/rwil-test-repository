
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ParametrizingsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ParametrizingsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Parametrizing" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ParametrizingType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParametrizingsType", propOrder = {
    "parametrizing"
})



public class ParametrizingsType {
	
	
    @XmlElement(name = "Parametrizing", required = true)
    private List<ParametrizingType> parametrizing;


		public void setParametrizingType(List<ParametrizingType> parametrizing) {
			this.parametrizing = parametrizing;
		}
		
	    public List<ParametrizingType> getParametrizing() {
	        if (parametrizing == null) {
	            parametrizing = new ArrayList<ParametrizingType>();
	        }
	        return this.parametrizing;
	    }

}
