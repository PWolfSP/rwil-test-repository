
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse f�r InspectionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="InspectionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GeneralInspection" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}YearMonthType" minOccurs="0"/>
 *         &lt;element name="ExhaustTest" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}YearMonthType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InspectionType", propOrder = {
    "generalInspection",
    "exhaustTest"
})
public class InspectionType {

    @XmlElement(name = "GeneralInspection")
    protected String generalInspection;
    @XmlElement(name = "ExhaustTest")
    protected String exhaustTest;

    /**
     * Ruft den Wert der generalInspection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralInspection() {
        return generalInspection;
    }

    /**
     * Legt den Wert der generalInspection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralInspection(String value) {
        this.generalInspection = value;
    }

    /**
     * Ruft den Wert der exhaustTest-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExhaustTest() {
        return exhaustTest;
    }

    /**
     * Legt den Wert der exhaustTest-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExhaustTest(String value) {
        this.exhaustTest = value;
    }

}
