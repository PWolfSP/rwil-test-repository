
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für VehicleConditionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VehicleConditionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Damages" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}DamagesType" minOccurs="0"/>
 *         &lt;element name="Preservations" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}PreservationsType" minOccurs="0"/>
 *         &lt;element name="DrivingReadyIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleConditionType", propOrder = {
    "damages",
    "preservations",
    "drivingReadyIND"
})
public class VehicleConditionType {

    @XmlElement(name = "Damages")
    protected DamagesType damages;
    @XmlElement(name = "Preservations")
    protected PreservationsType preservations;
    @XmlElement(name = "DrivingReadyIND")
    protected Boolean drivingReadyIND;

    /**
     * Ruft den Wert der damages-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DamagesType }
     *     
     */
    public DamagesType getDamages() {
        return damages;
    }

    /**
     * Legt den Wert der damages-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DamagesType }
     *     
     */
    public void setDamages(DamagesType value) {
        this.damages = value;
    }

    /**
     * Ruft den Wert der preservations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PreservationsType }
     *     
     */
    public PreservationsType getPreservations() {
        return preservations;
    }

    /**
     * Legt den Wert der preservations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PreservationsType }
     *     
     */
    public void setPreservations(PreservationsType value) {
        this.preservations = value;
    }

    /**
     * Ruft den Wert der drivingReadyIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDrivingReadyIND() {
        return drivingReadyIND;
    }

    /**
     * Legt den Wert der drivingReadyIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDrivingReadyIND(Boolean value) {
        this.drivingReadyIND = value;
    }

}
