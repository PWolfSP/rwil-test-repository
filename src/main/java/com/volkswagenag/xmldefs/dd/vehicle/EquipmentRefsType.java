
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für EquipmentRefsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EquipmentRefsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EquipmentOptionRef" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}EquipmentRefType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentRefsType", propOrder = {
    "equipmentOptionRef"
})



public class EquipmentRefsType {
	
	
    @XmlElement(name = "EquipmentOptionRef", required = true)
    private List<EquipmentRefType> equipmentOptionRef;


		public void setEquipmentRefType(List<EquipmentRefType> equipmentOptionRef) {
			this.equipmentOptionRef = equipmentOptionRef;
		}
		
	    public List<EquipmentRefType> getEquipmentOptionRef() {
	        if (equipmentOptionRef == null) {
	            equipmentOptionRef = new ArrayList<EquipmentRefType>();
	        }
	        return this.equipmentOptionRef;
	    }

}
