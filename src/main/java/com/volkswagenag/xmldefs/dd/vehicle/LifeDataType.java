
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für LifeDataType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LifeDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Status" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="TimeLine" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}TimeLineType" minOccurs="0"/>
 *         &lt;element name="Registrations" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}RegistrationsType" minOccurs="0"/>
 *         &lt;element name="Drivers" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}DriversType" minOccurs="0"/>
 *         &lt;element name="Keepers" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}KeepersType" minOccurs="0"/>
 *         &lt;element name="ContractData" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ContractDataType" minOccurs="0"/>
 *         &lt;element name="VehicleCondition" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}VehicleConditionType" minOccurs="0"/>
 *         &lt;element name="VehicleUsage" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}VehicleUsageType" minOccurs="0"/>
 *         &lt;element name="OdometerReadings" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}OdometerReadingsType" minOccurs="0"/>
 *         &lt;element name="Inspections" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}InspectionType" minOccurs="0"/>
 *         &lt;element name="Maintenances" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}MaintenancesType" minOccurs="0"/>
 *         &lt;element name="Alterations" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}AlterationsType" minOccurs="0"/>
 *         &lt;element name="Dealers" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}DealersType" minOccurs="0"/>
 *         &lt;element name="MediaData" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}MediaDataType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LifeDataType", propOrder = {
    "status",
    "timeLine",
    "registrations",
    "drivers",
    "keepers",
    "contractData",
    "vehicleCondition",
    "vehicleUsage",
    "odometerReadings",
    "inspections",
    "maintenances",
    "alterations",
    "dealers",
    "mediaData"
})
public class LifeDataType {

    @XmlElement(name = "Status")
    protected CodeType status;
    @XmlElement(name = "TimeLine")
    protected TimeLineType timeLine;
    @XmlElement(name = "Registrations")
    protected RegistrationsType registrations;
    @XmlElement(name = "Drivers")
    protected DriversType drivers;
    @XmlElement(name = "Keepers")
    protected KeepersType keepers;
    @XmlElement(name = "ContractData")
    protected ContractDataType contractData;
    @XmlElement(name = "VehicleCondition")
    protected VehicleConditionType vehicleCondition;
    @XmlElement(name = "VehicleUsage")
    protected VehicleUsageType vehicleUsage;
    @XmlElement(name = "OdometerReadings")
    protected OdometerReadingsType odometerReadings;
    @XmlElement(name = "Inspections")
    protected InspectionType inspections;
    @XmlElement(name = "Maintenances")
    protected MaintenancesType maintenances;
    @XmlElement(name = "Alterations")
    protected AlterationsType alterations;
    @XmlElement(name = "Dealers")
    protected DealersType dealers;
    @XmlElement(name = "MediaData")
    protected MediaDataType mediaData;

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setStatus(CodeType value) {
        this.status = value;
    }

    /**
     * Ruft den Wert der timeLine-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeLineType }
     *     
     */
    public TimeLineType getTimeLine() {
        return timeLine;
    }

    /**
     * Legt den Wert der timeLine-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeLineType }
     *     
     */
    public void setTimeLine(TimeLineType value) {
        this.timeLine = value;
    }

    /**
     * Ruft den Wert der registrations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationsType }
     *     
     */
    public RegistrationsType getRegistrations() {
        return registrations;
    }

    /**
     * Legt den Wert der registrations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationsType }
     *     
     */
    public void setRegistrations(RegistrationsType value) {
        this.registrations = value;
    }

    /**
     * Ruft den Wert der drivers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DriversType }
     *     
     */
    public DriversType getDrivers() {
        return drivers;
    }

    /**
     * Legt den Wert der drivers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DriversType }
     *     
     */
    public void setDrivers(DriversType value) {
        this.drivers = value;
    }

    /**
     * Ruft den Wert der keepers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link KeepersType }
     *     
     */
    public KeepersType getKeepers() {
        return keepers;
    }

    /**
     * Legt den Wert der keepers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link KeepersType }
     *     
     */
    public void setKeepers(KeepersType value) {
        this.keepers = value;
    }

    /**
     * Ruft den Wert der contractData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ContractDataType }
     *     
     */
    public ContractDataType getContractData() {
        return contractData;
    }

    /**
     * Legt den Wert der contractData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractDataType }
     *     
     */
    public void setContractData(ContractDataType value) {
        this.contractData = value;
    }

    /**
     * Ruft den Wert der vehicleCondition-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VehicleConditionType }
     *     
     */
    public VehicleConditionType getVehicleCondition() {
        return vehicleCondition;
    }

    /**
     * Legt den Wert der vehicleCondition-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleConditionType }
     *     
     */
    public void setVehicleCondition(VehicleConditionType value) {
        this.vehicleCondition = value;
    }

    /**
     * Ruft den Wert der vehicleUsage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VehicleUsageType }
     *     
     */
    public VehicleUsageType getVehicleUsage() {
        return vehicleUsage;
    }

    /**
     * Legt den Wert der vehicleUsage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleUsageType }
     *     
     */
    public void setVehicleUsage(VehicleUsageType value) {
        this.vehicleUsage = value;
    }

    /**
     * Ruft den Wert der odometerReadings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OdometerReadingsType }
     *     
     */
    public OdometerReadingsType getOdometerReadings() {
        return odometerReadings;
    }

    /**
     * Legt den Wert der odometerReadings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OdometerReadingsType }
     *     
     */
    public void setOdometerReadings(OdometerReadingsType value) {
        this.odometerReadings = value;
    }

    /**
     * Ruft den Wert der inspections-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InspectionType }
     *     
     */
    public InspectionType getInspections() {
        return inspections;
    }

    /**
     * Legt den Wert der inspections-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InspectionType }
     *     
     */
    public void setInspections(InspectionType value) {
        this.inspections = value;
    }

    /**
     * Ruft den Wert der maintenances-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MaintenancesType }
     *     
     */
    public MaintenancesType getMaintenances() {
        return maintenances;
    }

    /**
     * Legt den Wert der maintenances-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenancesType }
     *     
     */
    public void setMaintenances(MaintenancesType value) {
        this.maintenances = value;
    }

    /**
     * Ruft den Wert der alterations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlterationsType }
     *     
     */
    public AlterationsType getAlterations() {
        return alterations;
    }

    /**
     * Legt den Wert der alterations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlterationsType }
     *     
     */
    public void setAlterations(AlterationsType value) {
        this.alterations = value;
    }

    /**
     * Ruft den Wert der dealers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DealersType }
     *     
     */
    public DealersType getDealers() {
        return dealers;
    }

    /**
     * Legt den Wert der dealers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DealersType }
     *     
     */
    public void setDealers(DealersType value) {
        this.dealers = value;
    }

    /**
     * Ruft den Wert der mediaData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MediaDataType }
     *     
     */
    public MediaDataType getMediaData() {
        return mediaData;
    }

    /**
     * Legt den Wert der mediaData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MediaDataType }
     *     
     */
    public void setMediaData(MediaDataType value) {
        this.mediaData = value;
    }

}
