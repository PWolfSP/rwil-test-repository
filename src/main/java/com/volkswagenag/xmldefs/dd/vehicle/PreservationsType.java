
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für PreservationsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PreservationsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Preservation" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}PreservationType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PreservationsType", propOrder = {
    "preservation"
})



public class PreservationsType {
	
	
    @XmlElement(name = "Preservation", required = true)
    private List<PreservationType> preservation;


		public void setPreservationType(List<PreservationType> preservation) {
			this.preservation = preservation;
		}
		
	    public List<PreservationType> getPreservation() {
	        if (preservation == null) {
	            preservation = new ArrayList<PreservationType>();
	        }
	        return this.preservation;
	    }

}
