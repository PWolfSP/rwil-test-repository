
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * Model codes. List of Model codes.
 * 
 * <p>Java-Klasse für ModelCodesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ModelCodesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ModelCode" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModelCodesType", propOrder = {
    "modelCode"
})



public class ModelCodesType {
	
	
    @XmlElement(name = "ModelCode", required = true)
    private List<CodeType> modelCode;


		public void setCodeType(List<CodeType> modelCode) {
			this.modelCode = modelCode;
		}
		
	    public List<CodeType> getModelCode() {
	        if (modelCode == null) {
	            modelCode = new ArrayList<CodeType>();
	        }
	        return this.modelCode;
	    }

}
