
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für WarrantiesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="WarrantiesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Warranty" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}WarrantyType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WarrantiesType", propOrder = {
    "warranty"
})



public class WarrantiesType {
	
	
    @XmlElement(name = "Warranty", required = true)
    private List<WarrantyType> warranty;


		public void setWarrantyType(List<WarrantyType> warranty) {
			this.warranty = warranty;
		}
		
	    public List<WarrantyType> getWarranty() {
	        if (warranty == null) {
	            warranty = new ArrayList<WarrantyType>();
	        }
	        return this.warranty;
	    }

}
