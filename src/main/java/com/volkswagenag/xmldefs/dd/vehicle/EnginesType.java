
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für EnginesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EnginesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Engine" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}EngineType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnginesType", propOrder = {
    "engine"
})



public class EnginesType {
	
	
    @XmlElement(name = "Engine", required = true)
    private List<EngineType> engine;


		public void setEngineType(List<EngineType> engine) {
			this.engine = engine;
		}
		
	    public List<EngineType> getEngine() {
	        if (engine == null) {
	            engine = new ArrayList<EngineType>();
	        }
	        return this.engine;
	    }

}
