
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für FlashingsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="FlashingsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Flashing" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}FlashingType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlashingsType", propOrder = {
    "flashing"
})



public class FlashingsType {
	
	
    @XmlElement(name = "Flashing", required = true)
    private List<FlashingType> flashing;


		public void setFlashingType(List<FlashingType> flashing) {
			this.flashing = flashing;
		}
		
	    public List<FlashingType> getFlashing() {
	        if (flashing == null) {
	            flashing = new ArrayList<FlashingType>();
	        }
	        return this.flashing;
	    }

}
