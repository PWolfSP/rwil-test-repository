
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.vehicleproduct.VehicleProductIdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für BaseProductType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BaseProductType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/VehicleProduct}VehicleProductRef" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseProductType", propOrder = {
    "vehicleProductRef"
})
public class BaseProductType {

    @XmlElement(name = "VehicleProductRef", namespace = "http://xmldefs.volkswagenag.com/DD/VehicleProduct")
    protected VehicleProductIdentifierType vehicleProductRef;

    /**
     * Ruft den Wert der vehicleProductRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VehicleProductIdentifierType }
     *     
     */
    public VehicleProductIdentifierType getVehicleProductRef() {
        return vehicleProductRef;
    }

    /**
     * Legt den Wert der vehicleProductRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleProductIdentifierType }
     *     
     */
    public void setVehicleProductRef(VehicleProductIdentifierType value) {
        this.vehicleProductRef = value;
    }

}
