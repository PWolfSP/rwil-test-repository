
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CountryCodeType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für LicensePlateType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LicensePlateType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LicensePlateNumber" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="CountryOfLicense" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CountryCodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LicensePlateType", propOrder = {
    "licensePlateNumber",
    "countryOfLicense"
})
public class LicensePlateType {

    @XmlElement(name = "LicensePlateNumber")
    protected IdentifierType licensePlateNumber;
    @XmlElement(name = "CountryOfLicense")
    protected CountryCodeType countryOfLicense;

    /**
     * Ruft den Wert der licensePlateNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getLicensePlateNumber() {
        return licensePlateNumber;
    }

    /**
     * Legt den Wert der licensePlateNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setLicensePlateNumber(IdentifierType value) {
        this.licensePlateNumber = value;
    }

    /**
     * Ruft den Wert der countryOfLicense-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CountryCodeType }
     *     
     */
    public CountryCodeType getCountryOfLicense() {
        return countryOfLicense;
    }

    /**
     * Legt den Wert der countryOfLicense-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryCodeType }
     *     
     */
    public void setCountryOfLicense(CountryCodeType value) {
        this.countryOfLicense = value;
    }

}
