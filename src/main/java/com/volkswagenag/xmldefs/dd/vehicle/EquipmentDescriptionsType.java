
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für EquipmentDescriptionsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EquipmentDescriptionsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Description" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}EquipmentDescriptionType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentDescriptionsType", propOrder = {
    "description"
})



public class EquipmentDescriptionsType {
	
	
    @XmlElement(name = "Description", required = true)
    private List<EquipmentDescriptionType> description;


		public void setEquipmentDescriptionType(List<EquipmentDescriptionType> description) {
			this.description = description;
		}
		
	    public List<EquipmentDescriptionType> getDescription() {
	        if (description == null) {
	            description = new ArrayList<EquipmentDescriptionType>();
	        }
	        return this.description;
	    }

}
