
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import lombok.Generated;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.volkswagenag.xmldefs.dd.vehicle package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@Generated
@XmlRegistry
public class ObjectFactory {

    private final static QName _VehicleRef_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/Vehicle", "VehicleRef");
    private final static QName _Vehicle_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/Vehicle", "Vehicle");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.volkswagenag.xmldefs.dd.vehicle
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link VehicleType }
     * 
     */
    public VehicleType createVehicleType() {
        return new VehicleType();
    }

    /**
     * Create an instance of {@link VehicleIdentifierType }
     * 
     */
    public VehicleIdentifierType createVehicleIdentifierType() {
        return new VehicleIdentifierType();
    }

    /**
     * Create an instance of {@link InspectionType }
     * 
     */
    public InspectionType createInspectionType() {
        return new InspectionType();
    }

    /**
     * Create an instance of {@link LoadCompartmentsType }
     * 
     */
    public LoadCompartmentsType createLoadCompartmentsType() {
        return new LoadCompartmentsType();
    }

    /**
     * Create an instance of {@link TracksType }
     * 
     */
    public TracksType createTracksType() {
        return new TracksType();
    }

    /**
     * Create an instance of {@link ModelCodesType }
     * 
     */
    public ModelCodesType createModelCodesType() {
        return new ModelCodesType();
    }

    /**
     * Create an instance of {@link LifeDataType }
     * 
     */
    public LifeDataType createLifeDataType() {
        return new LifeDataType();
    }

    /**
     * Create an instance of {@link OwnerType }
     * 
     */
    public OwnerType createOwnerType() {
        return new OwnerType();
    }

    /**
     * Create an instance of {@link ServiceType }
     * 
     */
    public ServiceType createServiceType() {
        return new ServiceType();
    }

    /**
     * Create an instance of {@link BaseProductType }
     * 
     */
    public BaseProductType createBaseProductType() {
        return new BaseProductType();
    }

    /**
     * Create an instance of {@link ReportingDealerType }
     * 
     */
    public ReportingDealerType createReportingDealerType() {
        return new ReportingDealerType();
    }

    /**
     * Create an instance of {@link ItemAlterationType }
     * 
     */
    public ItemAlterationType createItemAlterationType() {
        return new ItemAlterationType();
    }

    /**
     * Create an instance of {@link MediaType }
     * 
     */
    public MediaType createMediaType() {
        return new MediaType();
    }

    /**
     * Create an instance of {@link EngineType }
     * 
     */
    public EngineType createEngineType() {
        return new EngineType();
    }

    /**
     * Create an instance of {@link ContactDealerType }
     * 
     */
    public ContactDealerType createContactDealerType() {
        return new ContactDealerType();
    }

    /**
     * Create an instance of {@link AlterationsType }
     * 
     */
    public AlterationsType createAlterationsType() {
        return new AlterationsType();
    }

    /**
     * Create an instance of {@link VehicleConditionType }
     * 
     */
    public VehicleConditionType createVehicleConditionType() {
        return new VehicleConditionType();
    }

    /**
     * Create an instance of {@link MaintenancesType }
     * 
     */
    public MaintenancesType createMaintenancesType() {
        return new MaintenancesType();
    }

    /**
     * Create an instance of {@link AdditionalConditionsType }
     * 
     */
    public AdditionalConditionsType createAdditionalConditionsType() {
        return new AdditionalConditionsType();
    }

    /**
     * Create an instance of {@link KeepersType }
     * 
     */
    public KeepersType createKeepersType() {
        return new KeepersType();
    }

    /**
     * Create an instance of {@link DriversType }
     * 
     */
    public DriversType createDriversType() {
        return new DriversType();
    }

    /**
     * Create an instance of {@link DriveType }
     * 
     */
    public DriveType createDriveType() {
        return new DriveType();
    }

    /**
     * Create an instance of {@link ParametrizingType }
     * 
     */
    public ParametrizingType createParametrizingType() {
        return new ParametrizingType();
    }

    /**
     * Create an instance of {@link DealersType }
     * 
     */
    public DealersType createDealersType() {
        return new DealersType();
    }

    /**
     * Create an instance of {@link CodingType }
     * 
     */
    public CodingType createCodingType() {
        return new CodingType();
    }

    /**
     * Create an instance of {@link ConsumptionsType }
     * 
     */
    public ConsumptionsType createConsumptionsType() {
        return new ConsumptionsType();
    }

    /**
     * Create an instance of {@link PartType }
     * 
     */
    public PartType createPartType() {
        return new PartType();
    }

    /**
     * Create an instance of {@link WarrantyType }
     * 
     */
    public WarrantyType createWarrantyType() {
        return new WarrantyType();
    }

    /**
     * Create an instance of {@link DeliveryDateType }
     * 
     */
    public DeliveryDateType createDeliveryDateType() {
        return new DeliveryDateType();
    }

    /**
     * Create an instance of {@link ServiceProvisionDateType }
     * 
     */
    public ServiceProvisionDateType createServiceProvisionDateType() {
        return new ServiceProvisionDateType();
    }

    /**
     * Create an instance of {@link ManufacturerIdentsType }
     * 
     */
    public ManufacturerIdentsType createManufacturerIdentsType() {
        return new ManufacturerIdentsType();
    }

    /**
     * Create an instance of {@link TransmissionsType }
     * 
     */
    public TransmissionsType createTransmissionsType() {
        return new TransmissionsType();
    }

    /**
     * Create an instance of {@link LicenseTagsType }
     * 
     */
    public LicenseTagsType createLicenseTagsType() {
        return new LicenseTagsType();
    }

    /**
     * Create an instance of {@link ADIType }
     * 
     */
    public ADIType createADIType() {
        return new ADIType();
    }

    /**
     * Create an instance of {@link TimeLineType }
     * 
     */
    public TimeLineType createTimeLineType() {
        return new TimeLineType();
    }

    /**
     * Create an instance of {@link ColorsType }
     * 
     */
    public ColorsType createColorsType() {
        return new ColorsType();
    }

    /**
     * Create an instance of {@link AxleLoadType }
     * 
     */
    public AxleLoadType createAxleLoadType() {
        return new AxleLoadType();
    }

    /**
     * Create an instance of {@link TelematicsDataType }
     * 
     */
    public TelematicsDataType createTelematicsDataType() {
        return new TelematicsDataType();
    }

    /**
     * Create an instance of {@link LicensePlateType }
     * 
     */
    public LicensePlateType createLicensePlateType() {
        return new LicensePlateType();
    }

    /**
     * Create an instance of {@link LoadCompartmentType }
     * 
     */
    public LoadCompartmentType createLoadCompartmentType() {
        return new LoadCompartmentType();
    }

    /**
     * Create an instance of {@link EquipmentOptionsType }
     * 
     */
    public EquipmentOptionsType createEquipmentOptionsType() {
        return new EquipmentOptionsType();
    }

    /**
     * Create an instance of {@link TechnicalDataType }
     * 
     */
    public TechnicalDataType createTechnicalDataType() {
        return new TechnicalDataType();
    }

    /**
     * Create an instance of {@link DamageType }
     * 
     */
    public DamageType createDamageType() {
        return new DamageType();
    }

    /**
     * Create an instance of {@link ComponentsType }
     * 
     */
    public ComponentsType createComponentsType() {
        return new ComponentsType();
    }

    /**
     * Create an instance of {@link NoteType }
     * 
     */
    public NoteType createNoteType() {
        return new NoteType();
    }

    /**
     * Create an instance of {@link TransmissionType }
     * 
     */
    public TransmissionType createTransmissionType() {
        return new TransmissionType();
    }

    /**
     * Create an instance of {@link EquipmentDescriptionType }
     * 
     */
    public EquipmentDescriptionType createEquipmentDescriptionType() {
        return new EquipmentDescriptionType();
    }

    /**
     * Create an instance of {@link ModelType }
     * 
     */
    public ModelType createModelType() {
        return new ModelType();
    }

    /**
     * Create an instance of {@link ProductionDateType }
     * 
     */
    public ProductionDateType createProductionDateType() {
        return new ProductionDateType();
    }

    /**
     * Create an instance of {@link TrailerLoadType }
     * 
     */
    public TrailerLoadType createTrailerLoadType() {
        return new TrailerLoadType();
    }

    /**
     * Create an instance of {@link FlashingType }
     * 
     */
    public FlashingType createFlashingType() {
        return new FlashingType();
    }

    /**
     * Create an instance of {@link QualifiedDateType }
     * 
     */
    public QualifiedDateType createQualifiedDateType() {
        return new QualifiedDateType();
    }

    /**
     * Create an instance of {@link EquipmentRefsType }
     * 
     */
    public EquipmentRefsType createEquipmentRefsType() {
        return new EquipmentRefsType();
    }

    /**
     * Create an instance of {@link InteriorsType }
     * 
     */
    public InteriorsType createInteriorsType() {
        return new InteriorsType();
    }

    /**
     * Create an instance of {@link ManufacturerType }
     * 
     */
    public ManufacturerType createManufacturerType() {
        return new ManufacturerType();
    }

    /**
     * Create an instance of {@link RegistrationsType }
     * 
     */
    public RegistrationsType createRegistrationsType() {
        return new RegistrationsType();
    }

    /**
     * Create an instance of {@link EquipmentsType }
     * 
     */
    public EquipmentsType createEquipmentsType() {
        return new EquipmentsType();
    }

    /**
     * Create an instance of {@link EquipmentType }
     * 
     */
    public EquipmentType createEquipmentType() {
        return new EquipmentType();
    }

    /**
     * Create an instance of {@link OdometerReadingsType }
     * 
     */
    public OdometerReadingsType createOdometerReadingsType() {
        return new OdometerReadingsType();
    }

    /**
     * Create an instance of {@link WarrantiesType }
     * 
     */
    public WarrantiesType createWarrantiesType() {
        return new WarrantiesType();
    }

    /**
     * Create an instance of {@link PollutantClassesType }
     * 
     */
    public PollutantClassesType createPollutantClassesType() {
        return new PollutantClassesType();
    }

    /**
     * Create an instance of {@link ServicesType }
     * 
     */
    public ServicesType createServicesType() {
        return new ServicesType();
    }

    /**
     * Create an instance of {@link AdditionalConditionType }
     * 
     */
    public AdditionalConditionType createAdditionalConditionType() {
        return new AdditionalConditionType();
    }

    /**
     * Create an instance of {@link DriverType }
     * 
     */
    public DriverType createDriverType() {
        return new DriverType();
    }

    /**
     * Create an instance of {@link DamagesType }
     * 
     */
    public DamagesType createDamagesType() {
        return new DamagesType();
    }

    /**
     * Create an instance of {@link LabeledDatesType }
     * 
     */
    public LabeledDatesType createLabeledDatesType() {
        return new LabeledDatesType();
    }

    /**
     * Create an instance of {@link PreservationType }
     * 
     */
    public PreservationType createPreservationType() {
        return new PreservationType();
    }

    /**
     * Create an instance of {@link AxleLoadsType }
     * 
     */
    public AxleLoadsType createAxleLoadsType() {
        return new AxleLoadsType();
    }

    /**
     * Create an instance of {@link EquipmentDescriptionsType }
     * 
     */
    public EquipmentDescriptionsType createEquipmentDescriptionsType() {
        return new EquipmentDescriptionsType();
    }

    /**
     * Create an instance of {@link DealerType }
     * 
     */
    public DealerType createDealerType() {
        return new DealerType();
    }

    /**
     * Create an instance of {@link ContractRefType }
     * 
     */
    public ContractRefType createContractRefType() {
        return new ContractRefType();
    }

    /**
     * Create an instance of {@link KeeperType }
     * 
     */
    public KeeperType createKeeperType() {
        return new KeeperType();
    }

    /**
     * Create an instance of {@link ParametrizingsType }
     * 
     */
    public ParametrizingsType createParametrizingsType() {
        return new ParametrizingsType();
    }

    /**
     * Create an instance of {@link MediaDataType }
     * 
     */
    public MediaDataType createMediaDataType() {
        return new MediaDataType();
    }

    /**
     * Create an instance of {@link TuningType }
     * 
     */
    public TuningType createTuningType() {
        return new TuningType();
    }

    /**
     * Create an instance of {@link EquipmentRefType }
     * 
     */
    public EquipmentRefType createEquipmentRefType() {
        return new EquipmentRefType();
    }

    /**
     * Create an instance of {@link LeasingType }
     * 
     */
    public LeasingType createLeasingType() {
        return new LeasingType();
    }

    /**
     * Create an instance of {@link FlashingsType }
     * 
     */
    public FlashingsType createFlashingsType() {
        return new FlashingsType();
    }

    /**
     * Create an instance of {@link LoadType }
     * 
     */
    public LoadType createLoadType() {
        return new LoadType();
    }

    /**
     * Create an instance of {@link ItemAlterationsType }
     * 
     */
    public ItemAlterationsType createItemAlterationsType() {
        return new ItemAlterationsType();
    }

    /**
     * Create an instance of {@link TransmissionTypesType }
     * 
     */
    public TransmissionTypesType createTransmissionTypesType() {
        return new TransmissionTypesType();
    }

    /**
     * Create an instance of {@link LabeledDateType }
     * 
     */
    public LabeledDateType createLabeledDateType() {
        return new LabeledDateType();
    }

    /**
     * Create an instance of {@link MaintenanceType }
     * 
     */
    public MaintenanceType createMaintenanceType() {
        return new MaintenanceType();
    }

    /**
     * Create an instance of {@link EquipmentOptionType }
     * 
     */
    public EquipmentOptionType createEquipmentOptionType() {
        return new EquipmentOptionType();
    }

    /**
     * Create an instance of {@link EmissionType }
     * 
     */
    public EmissionType createEmissionType() {
        return new EmissionType();
    }

    /**
     * Create an instance of {@link ConsumptionType }
     * 
     */
    public ConsumptionType createConsumptionType() {
        return new ConsumptionType();
    }

    /**
     * Create an instance of {@link RegistrationType }
     * 
     */
    public RegistrationType createRegistrationType() {
        return new RegistrationType();
    }

    /**
     * Create an instance of {@link EmissionsType }
     * 
     */
    public EmissionsType createEmissionsType() {
        return new EmissionsType();
    }

    /**
     * Create an instance of {@link TrailerLoadsType }
     * 
     */
    public TrailerLoadsType createTrailerLoadsType() {
        return new TrailerLoadsType();
    }

    /**
     * Create an instance of {@link NotesType }
     * 
     */
    public NotesType createNotesType() {
        return new NotesType();
    }

    /**
     * Create an instance of {@link ComponentType }
     * 
     */
    public ComponentType createComponentType() {
        return new ComponentType();
    }

    /**
     * Create an instance of {@link ApplicationIdentifiersType }
     * 
     */
    public ApplicationIdentifiersType createApplicationIdentifiersType() {
        return new ApplicationIdentifiersType();
    }

    /**
     * Create an instance of {@link VehicleUsageType }
     * 
     */
    public VehicleUsageType createVehicleUsageType() {
        return new VehicleUsageType();
    }

    /**
     * Create an instance of {@link EnginesType }
     * 
     */
    public EnginesType createEnginesType() {
        return new EnginesType();
    }

    /**
     * Create an instance of {@link OdometerReadingType }
     * 
     */
    public OdometerReadingType createOdometerReadingType() {
        return new OdometerReadingType();
    }

    /**
     * Create an instance of {@link ContractDataType }
     * 
     */
    public ContractDataType createContractDataType() {
        return new ContractDataType();
    }

    /**
     * Create an instance of {@link ContractPartyType }
     * 
     */
    public ContractPartyType createContractPartyType() {
        return new ContractPartyType();
    }

    /**
     * Create an instance of {@link DimensionsType }
     * 
     */
    public DimensionsType createDimensionsType() {
        return new DimensionsType();
    }

    /**
     * Create an instance of {@link CodingsType }
     * 
     */
    public CodingsType createCodingsType() {
        return new CodingsType();
    }

    /**
     * Create an instance of {@link TrackType }
     * 
     */
    public TrackType createTrackType() {
        return new TrackType();
    }

    /**
     * Create an instance of {@link ProductionDatesType }
     * 
     */
    public ProductionDatesType createProductionDatesType() {
        return new ProductionDatesType();
    }

    /**
     * Create an instance of {@link PreservationsType }
     * 
     */
    public PreservationsType createPreservationsType() {
        return new PreservationsType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VehicleIdentifierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/Vehicle", name = "VehicleRef")
    public JAXBElement<VehicleIdentifierType> createVehicleRef(VehicleIdentifierType value) {
        return new JAXBElement<VehicleIdentifierType>(_VehicleRef_QNAME, VehicleIdentifierType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VehicleType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/Vehicle", name = "Vehicle")
    public JAXBElement<VehicleType> createVehicle(VehicleType value) {
        return new JAXBElement<VehicleType>(_Vehicle_QNAME, VehicleType.class, null, value);
    }

}
