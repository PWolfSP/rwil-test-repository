
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.AmountType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.TextType;

import lombok.Generated;


/**
 * <p>Java-Klasse f�r DamageType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DamageType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Kind" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}TextType" minOccurs="0"/>
 *         &lt;element name="Date" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="ReportedBy" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ReportingDealerType" minOccurs="0"/>
 *         &lt;element name="RepairedIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="RepairNote" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}TextType" minOccurs="0"/>
 *         &lt;element name="RepairCost" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}AmountType" minOccurs="0"/>
 *         &lt;element name="DecreaseValue" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}AmountType" minOccurs="0"/>
 *         &lt;element name="AccidentFreeIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DamageType", propOrder = {
    "kind",
    "description",
    "date",
    "reportedBy",
    "repairedIND",
    "repairNote",
    "repairCost",
    "decreaseValue",
    "accidentFreeIND"
})
public class DamageType {

    @XmlElement(name = "Kind")
    protected CodeType kind;
    @XmlElement(name = "Description")
    protected TextType description;
    @XmlElement(name = "Date")
    protected String date;
    @XmlElement(name = "ReportedBy")
    protected ReportingDealerType reportedBy;
    @XmlElement(name = "RepairedIND")
    protected Boolean repairedIND;
    @XmlElement(name = "RepairNote")
    protected TextType repairNote;
    @XmlElement(name = "RepairCost")
    protected AmountType repairCost;
    @XmlElement(name = "DecreaseValue")
    protected AmountType decreaseValue;
    @XmlElement(name = "AccidentFreeIND")
    protected Boolean accidentFreeIND;

    /**
     * Ruft den Wert der kind-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getKind() {
        return kind;
    }

    /**
     * Legt den Wert der kind-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setKind(CodeType value) {
        this.kind = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setDescription(TextType value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der date-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDate() {
        return date;
    }

    /**
     * Legt den Wert der date-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDate(String value) {
        this.date = value;
    }

    /**
     * Ruft den Wert der reportedBy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReportingDealerType }
     *     
     */
    public ReportingDealerType getReportedBy() {
        return reportedBy;
    }

    /**
     * Legt den Wert der reportedBy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReportingDealerType }
     *     
     */
    public void setReportedBy(ReportingDealerType value) {
        this.reportedBy = value;
    }

    /**
     * Ruft den Wert der repairedIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRepairedIND() {
        return repairedIND;
    }

    /**
     * Legt den Wert der repairedIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRepairedIND(Boolean value) {
        this.repairedIND = value;
    }

    /**
     * Ruft den Wert der repairNote-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getRepairNote() {
        return repairNote;
    }

    /**
     * Legt den Wert der repairNote-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setRepairNote(TextType value) {
        this.repairNote = value;
    }

    /**
     * Ruft den Wert der repairCost-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getRepairCost() {
        return repairCost;
    }

    /**
     * Legt den Wert der repairCost-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setRepairCost(AmountType value) {
        this.repairCost = value;
    }

    /**
     * Ruft den Wert der decreaseValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getDecreaseValue() {
        return decreaseValue;
    }

    /**
     * Legt den Wert der decreaseValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setDecreaseValue(AmountType value) {
        this.decreaseValue = value;
    }

    /**
     * Ruft den Wert der accidentFreeIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAccidentFreeIND() {
        return accidentFreeIND;
    }

    /**
     * Legt den Wert der accidentFreeIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAccidentFreeIND(Boolean value) {
        this.accidentFreeIND = value;
    }

}
