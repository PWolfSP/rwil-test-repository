
package com.volkswagenag.xmldefs.dd.vehicle;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.MeasureType;
import com.volkswagenag.xmldefs.dd.businesspartner.PartnerIdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse f�r AdditionalConditionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AdditionalConditionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}WholesalerRef"/>
 *         &lt;element name="Status" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType"/>
 *         &lt;element name="ConfirmedIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType"/>
 *         &lt;element name="TransferIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType"/>
 *         &lt;element name="ContractClass" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType"/>
 *         &lt;element name="ContractType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType"/>
 *         &lt;element name="ContractParty" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ContractPartyType" minOccurs="0"/>
 *         &lt;element name="ContractRef" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ContractRefType" minOccurs="0"/>
 *         &lt;element name="StartDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType"/>
 *         &lt;element name="ExpirationDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="Duration" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *         &lt;element name="StartMilage" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *         &lt;element name="EndMilage" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *         &lt;element name="TotalMilage" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *         &lt;element name="MaxMilage" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *         &lt;element name="Notes" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}NotesType" minOccurs="0"/>
 *         &lt;element name="LastModified" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}QualifiedDateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdditionalConditionType", propOrder = {
    "wholesalerRef",
    "status",
    "confirmedIND",
    "transferIND",
    "contractClass",
    "contractType",
    "contractParty",
    "contractRef",
    "startDate",
    "expirationDate",
    "duration",
    "startMilage",
    "endMilage",
    "totalMilage",
    "maxMilage",
    "notes",
    "lastModified"
})
public class AdditionalConditionType {

    @XmlElement(name = "WholesalerRef", namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", required = true)
    protected PartnerIdentifierType wholesalerRef;
    @XmlElement(name = "Status", required = true)
    protected CodeType status;
    @XmlElement(name = "ConfirmedIND")
    protected boolean confirmedIND;
    @XmlElement(name = "TransferIND")
    protected boolean transferIND;
    @XmlElement(name = "ContractClass", required = true)
    protected CodeType contractClass;
    @XmlElement(name = "ContractType", required = true)
    protected CodeType contractType;
    @XmlElement(name = "ContractParty")
    protected ContractPartyType contractParty;
    @XmlElement(name = "ContractRef")
    protected ContractRefType contractRef;
    @XmlElement(name = "StartDate", required = true)
    protected String startDate;
    @XmlElement(name = "ExpirationDate")
    protected String expirationDate;
    @XmlElement(name = "Duration")
    protected BigDecimal duration;
    @XmlElement(name = "StartMilage")
    protected MeasureType startMilage;
    @XmlElement(name = "EndMilage")
    protected MeasureType endMilage;
    @XmlElement(name = "TotalMilage")
    protected MeasureType totalMilage;
    @XmlElement(name = "MaxMilage")
    protected MeasureType maxMilage;
    @XmlElement(name = "Notes")
    protected NotesType notes;
    @XmlElement(name = "LastModified")
    protected QualifiedDateType lastModified;

    /**
     * Ruft den Wert der wholesalerRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public PartnerIdentifierType getWholesalerRef() {
        return wholesalerRef;
    }

    /**
     * Legt den Wert der wholesalerRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public void setWholesalerRef(PartnerIdentifierType value) {
        this.wholesalerRef = value;
    }

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setStatus(CodeType value) {
        this.status = value;
    }

    /**
     * Ruft den Wert der confirmedIND-Eigenschaft ab.
     * 
     */
    public boolean isConfirmedIND() {
        return confirmedIND;
    }

    /**
     * Legt den Wert der confirmedIND-Eigenschaft fest.
     * 
     */
    public void setConfirmedIND(boolean value) {
        this.confirmedIND = value;
    }

    /**
     * Ruft den Wert der transferIND-Eigenschaft ab.
     * 
     */
    public boolean isTransferIND() {
        return transferIND;
    }

    /**
     * Legt den Wert der transferIND-Eigenschaft fest.
     * 
     */
    public void setTransferIND(boolean value) {
        this.transferIND = value;
    }

    /**
     * Ruft den Wert der contractClass-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getContractClass() {
        return contractClass;
    }

    /**
     * Legt den Wert der contractClass-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setContractClass(CodeType value) {
        this.contractClass = value;
    }

    /**
     * Ruft den Wert der contractType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getContractType() {
        return contractType;
    }

    /**
     * Legt den Wert der contractType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setContractType(CodeType value) {
        this.contractType = value;
    }

    /**
     * Ruft den Wert der contractParty-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ContractPartyType }
     *     
     */
    public ContractPartyType getContractParty() {
        return contractParty;
    }

    /**
     * Legt den Wert der contractParty-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractPartyType }
     *     
     */
    public void setContractParty(ContractPartyType value) {
        this.contractParty = value;
    }

    /**
     * Ruft den Wert der contractRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ContractRefType }
     *     
     */
    public ContractRefType getContractRef() {
        return contractRef;
    }

    /**
     * Legt den Wert der contractRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractRefType }
     *     
     */
    public void setContractRef(ContractRefType value) {
        this.contractRef = value;
    }

    /**
     * Ruft den Wert der startDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Legt den Wert der startDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartDate(String value) {
        this.startDate = value;
    }

    /**
     * Ruft den Wert der expirationDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpirationDate() {
        return expirationDate;
    }

    /**
     * Legt den Wert der expirationDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpirationDate(String value) {
        this.expirationDate = value;
    }

    /**
     * Ruft den Wert der duration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDuration() {
        return duration;
    }

    /**
     * Legt den Wert der duration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDuration(BigDecimal value) {
        this.duration = value;
    }

    /**
     * Ruft den Wert der startMilage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getStartMilage() {
        return startMilage;
    }

    /**
     * Legt den Wert der startMilage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setStartMilage(MeasureType value) {
        this.startMilage = value;
    }

    /**
     * Ruft den Wert der endMilage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getEndMilage() {
        return endMilage;
    }

    /**
     * Legt den Wert der endMilage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setEndMilage(MeasureType value) {
        this.endMilage = value;
    }

    /**
     * Ruft den Wert der totalMilage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getTotalMilage() {
        return totalMilage;
    }

    /**
     * Legt den Wert der totalMilage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setTotalMilage(MeasureType value) {
        this.totalMilage = value;
    }

    /**
     * Ruft den Wert der maxMilage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getMaxMilage() {
        return maxMilage;
    }

    /**
     * Legt den Wert der maxMilage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setMaxMilage(MeasureType value) {
        this.maxMilage = value;
    }

    /**
     * Ruft den Wert der notes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NotesType }
     *     
     */
    public NotesType getNotes() {
        return notes;
    }

    /**
     * Legt den Wert der notes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NotesType }
     *     
     */
    public void setNotes(NotesType value) {
        this.notes = value;
    }

    /**
     * Ruft den Wert der lastModified-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QualifiedDateType }
     *     
     */
    public QualifiedDateType getLastModified() {
        return lastModified;
    }

    /**
     * Legt den Wert der lastModified-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QualifiedDateType }
     *     
     */
    public void setLastModified(QualifiedDateType value) {
        this.lastModified = value;
    }

}
