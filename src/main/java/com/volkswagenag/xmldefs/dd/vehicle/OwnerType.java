
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.businesspartner.PartnerIdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für OwnerType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OwnerType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}BusinessPartnerRef"/>
 *         &lt;element name="Type" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OwnerType", propOrder = {
    "businessPartnerRef",
    "type"
})
public class OwnerType {

    @XmlElement(name = "BusinessPartnerRef", namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", required = true)
    protected PartnerIdentifierType businessPartnerRef;
    @XmlElement(name = "Type")
    protected CodeType type;

    /**
     * Ruft den Wert der businessPartnerRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public PartnerIdentifierType getBusinessPartnerRef() {
        return businessPartnerRef;
    }

    /**
     * Legt den Wert der businessPartnerRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public void setBusinessPartnerRef(PartnerIdentifierType value) {
        this.businessPartnerRef = value;
    }

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setType(CodeType value) {
        this.type = value;
    }

}
