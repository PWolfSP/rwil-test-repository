
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für DriversType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DriversType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Driver" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}DriverType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DriversType", propOrder = {
    "driver"
})



public class DriversType {
	
	
    @XmlElement(name = "Driver", required = true)
    private List<DriverType> driver;


		public void setDriverType(List<DriverType> driver) {
			this.driver = driver;
		}
		
	    public List<DriverType> getDriver() {
	        if (driver == null) {
	            driver = new ArrayList<DriverType>();
	        }
	        return this.driver;
	    }

}
