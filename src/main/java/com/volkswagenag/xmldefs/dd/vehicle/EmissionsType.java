
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für EmissionsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EmissionsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Emission" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}EmissionType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmissionsType", propOrder = {
    "emission"
})



public class EmissionsType {
	
	
    @XmlElement(name = "Emission", required = true)
    private List<EmissionType> emission;


		public void setEmissionType(List<EmissionType> emission) {
			this.emission = emission;
		}
		
	    public List<EmissionType> getEmission() {
	        if (emission == null) {
	            emission = new ArrayList<EmissionType>();
	        }
	        return this.emission;
	    }

}
