
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * Any textual description or information
 * 
 * <p>Java-Klasse für NotesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="NotesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Note" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}NoteType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NotesType", propOrder = {
    "note"
})



public class NotesType {
	
	
    @XmlElement(name = "Note", required = true)
    private List<NoteType> note;


		public void setNoteType(List<NoteType> note) {
			this.note = note;
		}
		
	    public List<NoteType> getNote() {
	        if (note == null) {
	            note = new ArrayList<NoteType>();
	        }
	        return this.note;
	    }

}
