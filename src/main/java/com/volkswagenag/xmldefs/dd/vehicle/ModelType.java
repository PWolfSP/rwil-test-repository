
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.TextType;

import lombok.Generated;


/**
 * <p>Java-Klasse f�r ModelType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ModelType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ModelName" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}TextType" minOccurs="0"/>
 *         &lt;element name="ModelCodes" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ModelCodesType" minOccurs="0"/>
 *         &lt;element name="ModelYear" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}YearType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModelType", propOrder = {
    "modelName",
    "modelCodes",
    "modelYear"
})
public class ModelType {

    @XmlElement(name = "ModelName")
    protected TextType modelName;
    @XmlElement(name = "ModelCodes")
    protected ModelCodesType modelCodes;
    @XmlElement(name = "ModelYear")
    protected String modelYear;

    /**
     * Ruft den Wert der modelName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getModelName() {
        return modelName;
    }

    /**
     * Legt den Wert der modelName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setModelName(TextType value) {
        this.modelName = value;
    }

    /**
     * Ruft den Wert der modelCodes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ModelCodesType }
     *     
     */
    public ModelCodesType getModelCodes() {
        return modelCodes;
    }

    /**
     * Legt den Wert der modelCodes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ModelCodesType }
     *     
     */
    public void setModelCodes(ModelCodesType value) {
        this.modelCodes = value;
    }

    /**
     * Ruft den Wert der modelYear-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelYear() {
        return modelYear;
    }

    /**
     * Legt den Wert der modelYear-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelYear(String value) {
        this.modelYear = value;
    }

}
