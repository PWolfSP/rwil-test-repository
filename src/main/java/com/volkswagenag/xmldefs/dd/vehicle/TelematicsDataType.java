
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für TelematicsDataType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TelematicsDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Key" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType"/>
 *         &lt;element name="IMEI" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType"/>
 *         &lt;element name="ICCID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TelematicsDataType", propOrder = {
    "key",
    "imei",
    "iccid"
})
public class TelematicsDataType {

    @XmlElement(name = "Key", required = true)
    protected IdentifierType key;
    @XmlElement(name = "IMEI", required = true)
    protected IdentifierType imei;
    @XmlElement(name = "ICCID", required = true)
    protected IdentifierType iccid;

    /**
     * Ruft den Wert der key-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getKey() {
        return key;
    }

    /**
     * Legt den Wert der key-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setKey(IdentifierType value) {
        this.key = value;
    }

    /**
     * Ruft den Wert der imei-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getIMEI() {
        return imei;
    }

    /**
     * Legt den Wert der imei-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setIMEI(IdentifierType value) {
        this.imei = value;
    }

    /**
     * Ruft den Wert der iccid-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getICCID() {
        return iccid;
    }

    /**
     * Legt den Wert der iccid-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setICCID(IdentifierType value) {
        this.iccid = value;
    }

}
