
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ProductionDatesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProductionDatesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProductionDate" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ProductionDateType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductionDatesType", propOrder = {
    "productionDate"
})



public class ProductionDatesType {
	
	
    @XmlElement(name = "ProductionDate", required = true)
    private List<ProductionDateType> productionDate;


		public void setProductionDateType(List<ProductionDateType> productionDate) {
			this.productionDate = productionDate;
		}
		
	    public List<ProductionDateType> getProductionDate() {
	        if (productionDate == null) {
	            productionDate = new ArrayList<ProductionDateType>();
	        }
	        return this.productionDate;
	    }

}
