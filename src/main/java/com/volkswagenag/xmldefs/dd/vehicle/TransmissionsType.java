
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * Transmission data.
 * 
 * <p>Java-Klasse für TransmissionsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TransmissionsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Transmission" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}TransmissionType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransmissionsType", propOrder = {
    "transmission"
})



public class TransmissionsType {
	
	
    @XmlElement(name = "Transmission", required = true)
    private List<TransmissionType> transmission;


		public void setTransmissionType(List<TransmissionType> transmission) {
			this.transmission = transmission;
		}
		
	    public List<TransmissionType> getTransmission() {
	        if (transmission == null) {
	            transmission = new ArrayList<TransmissionType>();
	        }
	        return this.transmission;
	    }

}
