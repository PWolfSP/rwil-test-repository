
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für RegistrationsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RegistrationsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Registration" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}RegistrationType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegistrationsType", propOrder = {
    "registration"
})



public class RegistrationsType {
	
	
    @XmlElement(name = "Registration", required = true)
    private List<RegistrationType> registration;


		public void setRegistrationType(List<RegistrationType> registration) {
			this.registration = registration;
		}
		
	    public List<RegistrationType> getRegistration() {
	        if (registration == null) {
	            registration = new ArrayList<RegistrationType>();
	        }
	        return this.registration;
	    }

}
