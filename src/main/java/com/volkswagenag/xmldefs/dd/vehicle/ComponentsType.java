
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ComponentsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ComponentsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Component" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ComponentType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComponentsType", propOrder = {
    "component"
})



public class ComponentsType {
	
	
    @XmlElement(name = "Component", required = true)
    private List<ComponentType> component;


		public void setComponentType(List<ComponentType> component) {
			this.component = component;
		}
		
	    public List<ComponentType> getComponent() {
	        if (component == null) {
	            component = new ArrayList<ComponentType>();
	        }
	        return this.component;
	    }

}
