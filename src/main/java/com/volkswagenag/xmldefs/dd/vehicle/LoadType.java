
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.MeasureType;

import lombok.Generated;


/**
 * <p>Java-Klasse für LoadType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LoadType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CurbWeight" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *         &lt;element name="GVWR" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *         &lt;element name="Payload" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *         &lt;element name="RoofLoad" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}MeasureType" minOccurs="0"/>
 *         &lt;element name="AxleLoad" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}AxleLoadsType" minOccurs="0"/>
 *         &lt;element name="TrailerLoad" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}TrailerLoadsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoadType", propOrder = {
    "curbWeight",
    "gvwr",
    "payload",
    "roofLoad",
    "axleLoad",
    "trailerLoad"
})
public class LoadType {

    @XmlElement(name = "CurbWeight")
    protected MeasureType curbWeight;
    @XmlElement(name = "GVWR")
    protected MeasureType gvwr;
    @XmlElement(name = "Payload")
    protected MeasureType payload;
    @XmlElement(name = "RoofLoad")
    protected MeasureType roofLoad;
    @XmlElement(name = "AxleLoad")
    protected AxleLoadsType axleLoad;
    @XmlElement(name = "TrailerLoad")
    protected TrailerLoadsType trailerLoad;

    /**
     * Ruft den Wert der curbWeight-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getCurbWeight() {
        return curbWeight;
    }

    /**
     * Legt den Wert der curbWeight-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setCurbWeight(MeasureType value) {
        this.curbWeight = value;
    }

    /**
     * Ruft den Wert der gvwr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getGVWR() {
        return gvwr;
    }

    /**
     * Legt den Wert der gvwr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setGVWR(MeasureType value) {
        this.gvwr = value;
    }

    /**
     * Ruft den Wert der payload-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getPayload() {
        return payload;
    }

    /**
     * Legt den Wert der payload-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setPayload(MeasureType value) {
        this.payload = value;
    }

    /**
     * Ruft den Wert der roofLoad-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getRoofLoad() {
        return roofLoad;
    }

    /**
     * Legt den Wert der roofLoad-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setRoofLoad(MeasureType value) {
        this.roofLoad = value;
    }

    /**
     * Ruft den Wert der axleLoad-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AxleLoadsType }
     *     
     */
    public AxleLoadsType getAxleLoad() {
        return axleLoad;
    }

    /**
     * Legt den Wert der axleLoad-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AxleLoadsType }
     *     
     */
    public void setAxleLoad(AxleLoadsType value) {
        this.axleLoad = value;
    }

    /**
     * Ruft den Wert der trailerLoad-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrailerLoadsType }
     *     
     */
    public TrailerLoadsType getTrailerLoad() {
        return trailerLoad;
    }

    /**
     * Legt den Wert der trailerLoad-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrailerLoadsType }
     *     
     */
    public void setTrailerLoad(TrailerLoadsType value) {
        this.trailerLoad = value;
    }

}
