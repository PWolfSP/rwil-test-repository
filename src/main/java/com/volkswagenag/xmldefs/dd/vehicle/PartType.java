
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.sparepart.SparePartIdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für PartType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PartType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/SparePart}SparePartRef" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartType", propOrder = {
    "sparePartRef"
})
public class PartType {

    @XmlElement(name = "SparePartRef", namespace = "http://xmldefs.volkswagenag.com/DD/SparePart")
    protected SparePartIdentifierType sparePartRef;

    /**
     * Ruft den Wert der sparePartRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SparePartIdentifierType }
     *     
     */
    public SparePartIdentifierType getSparePartRef() {
        return sparePartRef;
    }

    /**
     * Legt den Wert der sparePartRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SparePartIdentifierType }
     *     
     */
    public void setSparePartRef(SparePartIdentifierType value) {
        this.sparePartRef = value;
    }

}
