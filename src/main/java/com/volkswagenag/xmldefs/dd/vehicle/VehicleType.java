
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * Number of the IC card.
 * 
 * <p>Java-Klasse für VehicleType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VehicleType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VehicleIdentifier" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}VehicleIdentifierType" minOccurs="0"/>
 *         &lt;element name="Manufacturer" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ManufacturerType" minOccurs="0"/>
 *         &lt;element name="Model" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ModelType" minOccurs="0"/>
 *         &lt;element name="TechnicalData" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}TechnicalDataType" minOccurs="0"/>
 *         &lt;element name="LifeData" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}LifeDataType" minOccurs="0"/>
 *         &lt;element name="BaseProduct" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}BaseProductType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleType", propOrder = {
    "vehicleIdentifier",
    "manufacturer",
    "model",
    "technicalData",
    "lifeData",
    "baseProduct"
})
public class VehicleType {

    @XmlElement(name = "VehicleIdentifier")
    protected VehicleIdentifierType vehicleIdentifier;
    @XmlElement(name = "Manufacturer")
    protected ManufacturerType manufacturer;
    @XmlElement(name = "Model")
    protected ModelType model;
    @XmlElement(name = "TechnicalData")
    protected TechnicalDataType technicalData;
    @XmlElement(name = "LifeData")
    protected LifeDataType lifeData;
    @XmlElement(name = "BaseProduct")
    protected BaseProductType baseProduct;

    /**
     * Ruft den Wert der vehicleIdentifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VehicleIdentifierType }
     *     
     */
    public VehicleIdentifierType getVehicleIdentifier() {
        return vehicleIdentifier;
    }

    /**
     * Legt den Wert der vehicleIdentifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleIdentifierType }
     *     
     */
    public void setVehicleIdentifier(VehicleIdentifierType value) {
        this.vehicleIdentifier = value;
    }

    /**
     * Ruft den Wert der manufacturer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ManufacturerType }
     *     
     */
    public ManufacturerType getManufacturer() {
        return manufacturer;
    }

    /**
     * Legt den Wert der manufacturer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ManufacturerType }
     *     
     */
    public void setManufacturer(ManufacturerType value) {
        this.manufacturer = value;
    }

    /**
     * Ruft den Wert der model-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ModelType }
     *     
     */
    public ModelType getModel() {
        return model;
    }

    /**
     * Legt den Wert der model-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ModelType }
     *     
     */
    public void setModel(ModelType value) {
        this.model = value;
    }

    /**
     * Ruft den Wert der technicalData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalDataType }
     *     
     */
    public TechnicalDataType getTechnicalData() {
        return technicalData;
    }

    /**
     * Legt den Wert der technicalData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalDataType }
     *     
     */
    public void setTechnicalData(TechnicalDataType value) {
        this.technicalData = value;
    }

    /**
     * Ruft den Wert der lifeData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LifeDataType }
     *     
     */
    public LifeDataType getLifeData() {
        return lifeData;
    }

    /**
     * Legt den Wert der lifeData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LifeDataType }
     *     
     */
    public void setLifeData(LifeDataType value) {
        this.lifeData = value;
    }

    /**
     * Ruft den Wert der baseProduct-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BaseProductType }
     *     
     */
    public BaseProductType getBaseProduct() {
        return baseProduct;
    }

    /**
     * Legt den Wert der baseProduct-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseProductType }
     *     
     */
    public void setBaseProduct(BaseProductType value) {
        this.baseProduct = value;
    }

}
