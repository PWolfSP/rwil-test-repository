
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für EquipmentType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EquipmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EquipmentClass" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="CodingType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="EquipmentOptions" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}EquipmentOptionsType" minOccurs="0"/>
 *         &lt;element name="Origin" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentType", propOrder = {
    "equipmentClass",
    "codingType",
    "equipmentOptions",
    "origin"
})
public class EquipmentType {

    @XmlElement(name = "EquipmentClass")
    protected CodeType equipmentClass;
    @XmlElement(name = "CodingType")
    protected CodeType codingType;
    @XmlElement(name = "EquipmentOptions")
    protected EquipmentOptionsType equipmentOptions;
    @XmlElement(name = "Origin")
    protected CodeType origin;

    /**
     * Ruft den Wert der equipmentClass-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getEquipmentClass() {
        return equipmentClass;
    }

    /**
     * Legt den Wert der equipmentClass-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setEquipmentClass(CodeType value) {
        this.equipmentClass = value;
    }

    /**
     * Ruft den Wert der codingType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getCodingType() {
        return codingType;
    }

    /**
     * Legt den Wert der codingType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setCodingType(CodeType value) {
        this.codingType = value;
    }

    /**
     * Ruft den Wert der equipmentOptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EquipmentOptionsType }
     *     
     */
    public EquipmentOptionsType getEquipmentOptions() {
        return equipmentOptions;
    }

    /**
     * Legt den Wert der equipmentOptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipmentOptionsType }
     *     
     */
    public void setEquipmentOptions(EquipmentOptionsType value) {
        this.equipmentOptions = value;
    }

    /**
     * Ruft den Wert der origin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getOrigin() {
        return origin;
    }

    /**
     * Legt den Wert der origin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setOrigin(CodeType value) {
        this.origin = value;
    }

}
