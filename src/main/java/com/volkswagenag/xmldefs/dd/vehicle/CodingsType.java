
package com.volkswagenag.xmldefs.dd.vehicle;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für CodingsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CodingsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Coding" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}CodingType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CodingsType", propOrder = {
    "coding"
})



public class CodingsType {
	
	
    @XmlElement(name = "Coding", required = true)
    private List<CodingType> coding;


		public void setCodingType(List<CodingType> coding) {
			this.coding = coding;
		}
		
	    public List<CodingType> getCoding() {
	        if (coding == null) {
	            coding = new ArrayList<CodingType>();
	        }
	        return this.coding;
	    }

}
