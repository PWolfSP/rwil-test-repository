
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse f�r ContractRefType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ContractRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContractID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType"/>
 *         &lt;element name="Date" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType"/>
 *         &lt;element name="Dealer" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ContactDealerType"/>
 *         &lt;element name="SystemIdent" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType"/>
 *         &lt;element name="UserID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractRefType", propOrder = {
    "contractID",
    "date",
    "dealer",
    "systemIdent",
    "userID"
})
public class ContractRefType {

    @XmlElement(name = "ContractID", required = true)
    protected IdentifierType contractID;
    @XmlElement(name = "Date", required = true)
    protected String date;
    @XmlElement(name = "Dealer", required = true)
    protected ContactDealerType dealer;
    @XmlElement(name = "SystemIdent", required = true)
    protected CodeType systemIdent;
    @XmlElement(name = "UserID", required = true)
    protected IdentifierType userID;

    /**
     * Ruft den Wert der contractID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getContractID() {
        return contractID;
    }

    /**
     * Legt den Wert der contractID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setContractID(IdentifierType value) {
        this.contractID = value;
    }

    /**
     * Ruft den Wert der date-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDate() {
        return date;
    }

    /**
     * Legt den Wert der date-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDate(String value) {
        this.date = value;
    }

    /**
     * Ruft den Wert der dealer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ContactDealerType }
     *     
     */
    public ContactDealerType getDealer() {
        return dealer;
    }

    /**
     * Legt den Wert der dealer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactDealerType }
     *     
     */
    public void setDealer(ContactDealerType value) {
        this.dealer = value;
    }

    /**
     * Ruft den Wert der systemIdent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getSystemIdent() {
        return systemIdent;
    }

    /**
     * Legt den Wert der systemIdent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setSystemIdent(CodeType value) {
        this.systemIdent = value;
    }

    /**
     * Ruft den Wert der userID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getUserID() {
        return userID;
    }

    /**
     * Legt den Wert der userID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setUserID(IdentifierType value) {
        this.userID = value;
    }

}
