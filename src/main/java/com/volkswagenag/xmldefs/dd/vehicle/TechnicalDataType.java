
package com.volkswagenag.xmldefs.dd.vehicle;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für TechnicalDataType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TechnicalDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Dimensions" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}DimensionsType" minOccurs="0"/>
 *         &lt;element name="Load" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}LoadType" minOccurs="0"/>
 *         &lt;element name="ADI" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ADIType" minOccurs="0"/>
 *         &lt;element name="DoorCNT" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *         &lt;element name="SeatCNT" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *         &lt;element name="AxlesCNT" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *         &lt;element name="AxlesDrivenCNT" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *         &lt;element name="Drive" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}DriveType" minOccurs="0"/>
 *         &lt;element name="BodyStyle" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="BuildUpKind" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="StandardTireIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="AutomaticTransmissionIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="Engines" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}EnginesType" minOccurs="0"/>
 *         &lt;element name="Transmissions" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}TransmissionsType" minOccurs="0"/>
 *         &lt;element name="TelematicsData" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}TelematicsDataType" minOccurs="0"/>
 *         &lt;element name="Equipments" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}EquipmentsType" minOccurs="0"/>
 *         &lt;element name="Components" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ComponentsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TechnicalDataType", propOrder = {
    "dimensions",
    "load",
    "adi",
    "doorCNT",
    "seatCNT",
    "axlesCNT",
    "axlesDrivenCNT",
    "drive",
    "bodyStyle",
    "buildUpKind",
    "standardTireIND",
    "automaticTransmissionIND",
    "engines",
    "transmissions",
    "telematicsData",
    "equipments",
    "components"
})
public class TechnicalDataType {

    @XmlElement(name = "Dimensions")
    protected DimensionsType dimensions;
    @XmlElement(name = "Load")
    protected LoadType load;
    @XmlElement(name = "ADI")
    protected ADIType adi;
    @XmlElement(name = "DoorCNT")
    protected BigDecimal doorCNT;
    @XmlElement(name = "SeatCNT")
    protected BigDecimal seatCNT;
    @XmlElement(name = "AxlesCNT")
    protected BigDecimal axlesCNT;
    @XmlElement(name = "AxlesDrivenCNT")
    protected BigDecimal axlesDrivenCNT;
    @XmlElement(name = "Drive")
    protected DriveType drive;
    @XmlElement(name = "BodyStyle")
    protected CodeType bodyStyle;
    @XmlElement(name = "BuildUpKind")
    protected CodeType buildUpKind;
    @XmlElement(name = "StandardTireIND")
    protected Boolean standardTireIND;
    @XmlElement(name = "AutomaticTransmissionIND", defaultValue = "false")
    protected Boolean automaticTransmissionIND;
    @XmlElement(name = "Engines")
    protected EnginesType engines;
    @XmlElement(name = "Transmissions")
    protected TransmissionsType transmissions;
    @XmlElement(name = "TelematicsData")
    protected TelematicsDataType telematicsData;
    @XmlElement(name = "Equipments")
    protected EquipmentsType equipments;
    @XmlElement(name = "Components")
    protected ComponentsType components;

    /**
     * Ruft den Wert der dimensions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DimensionsType }
     *     
     */
    public DimensionsType getDimensions() {
        return dimensions;
    }

    /**
     * Legt den Wert der dimensions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DimensionsType }
     *     
     */
    public void setDimensions(DimensionsType value) {
        this.dimensions = value;
    }

    /**
     * Ruft den Wert der load-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LoadType }
     *     
     */
    public LoadType getLoad() {
        return load;
    }

    /**
     * Legt den Wert der load-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LoadType }
     *     
     */
    public void setLoad(LoadType value) {
        this.load = value;
    }

    /**
     * Ruft den Wert der adi-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ADIType }
     *     
     */
    public ADIType getADI() {
        return adi;
    }

    /**
     * Legt den Wert der adi-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ADIType }
     *     
     */
    public void setADI(ADIType value) {
        this.adi = value;
    }

    /**
     * Ruft den Wert der doorCNT-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDoorCNT() {
        return doorCNT;
    }

    /**
     * Legt den Wert der doorCNT-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDoorCNT(BigDecimal value) {
        this.doorCNT = value;
    }

    /**
     * Ruft den Wert der seatCNT-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSeatCNT() {
        return seatCNT;
    }

    /**
     * Legt den Wert der seatCNT-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSeatCNT(BigDecimal value) {
        this.seatCNT = value;
    }

    /**
     * Ruft den Wert der axlesCNT-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAxlesCNT() {
        return axlesCNT;
    }

    /**
     * Legt den Wert der axlesCNT-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAxlesCNT(BigDecimal value) {
        this.axlesCNT = value;
    }

    /**
     * Ruft den Wert der axlesDrivenCNT-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAxlesDrivenCNT() {
        return axlesDrivenCNT;
    }

    /**
     * Legt den Wert der axlesDrivenCNT-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAxlesDrivenCNT(BigDecimal value) {
        this.axlesDrivenCNT = value;
    }

    /**
     * Ruft den Wert der drive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DriveType }
     *     
     */
    public DriveType getDrive() {
        return drive;
    }

    /**
     * Legt den Wert der drive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DriveType }
     *     
     */
    public void setDrive(DriveType value) {
        this.drive = value;
    }

    /**
     * Ruft den Wert der bodyStyle-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getBodyStyle() {
        return bodyStyle;
    }

    /**
     * Legt den Wert der bodyStyle-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setBodyStyle(CodeType value) {
        this.bodyStyle = value;
    }

    /**
     * Ruft den Wert der buildUpKind-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getBuildUpKind() {
        return buildUpKind;
    }

    /**
     * Legt den Wert der buildUpKind-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setBuildUpKind(CodeType value) {
        this.buildUpKind = value;
    }

    /**
     * Ruft den Wert der standardTireIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStandardTireIND() {
        return standardTireIND;
    }

    /**
     * Legt den Wert der standardTireIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStandardTireIND(Boolean value) {
        this.standardTireIND = value;
    }

    /**
     * Ruft den Wert der automaticTransmissionIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutomaticTransmissionIND() {
        return automaticTransmissionIND;
    }

    /**
     * Legt den Wert der automaticTransmissionIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutomaticTransmissionIND(Boolean value) {
        this.automaticTransmissionIND = value;
    }

    /**
     * Ruft den Wert der engines-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnginesType }
     *     
     */
    public EnginesType getEngines() {
        return engines;
    }

    /**
     * Legt den Wert der engines-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnginesType }
     *     
     */
    public void setEngines(EnginesType value) {
        this.engines = value;
    }

    /**
     * Ruft den Wert der transmissions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionsType }
     *     
     */
    public TransmissionsType getTransmissions() {
        return transmissions;
    }

    /**
     * Legt den Wert der transmissions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionsType }
     *     
     */
    public void setTransmissions(TransmissionsType value) {
        this.transmissions = value;
    }

    /**
     * Ruft den Wert der telematicsData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TelematicsDataType }
     *     
     */
    public TelematicsDataType getTelematicsData() {
        return telematicsData;
    }

    /**
     * Legt den Wert der telematicsData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TelematicsDataType }
     *     
     */
    public void setTelematicsData(TelematicsDataType value) {
        this.telematicsData = value;
    }

    /**
     * Ruft den Wert der equipments-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EquipmentsType }
     *     
     */
    public EquipmentsType getEquipments() {
        return equipments;
    }

    /**
     * Legt den Wert der equipments-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipmentsType }
     *     
     */
    public void setEquipments(EquipmentsType value) {
        this.equipments = value;
    }

    /**
     * Ruft den Wert der components-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ComponentsType }
     *     
     */
    public ComponentsType getComponents() {
        return components;
    }

    /**
     * Legt den Wert der components-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ComponentsType }
     *     
     */
    public void setComponents(ComponentsType value) {
        this.components = value;
    }

}
