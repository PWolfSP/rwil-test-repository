
package com.volkswagenag.xmldefs.dd.vehicle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für AlterationsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AlterationsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Tuning" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}TuningType" minOccurs="0"/>
 *         &lt;element name="ItemAlterations" type="{http://xmldefs.volkswagenag.com/DD/Vehicle}ItemAlterationsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlterationsType", propOrder = {
    "tuning",
    "itemAlterations"
})
public class AlterationsType {

    @XmlElement(name = "Tuning")
    protected TuningType tuning;
    @XmlElement(name = "ItemAlterations")
    protected ItemAlterationsType itemAlterations;

    /**
     * Ruft den Wert der tuning-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TuningType }
     *     
     */
    public TuningType getTuning() {
        return tuning;
    }

    /**
     * Legt den Wert der tuning-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TuningType }
     *     
     */
    public void setTuning(TuningType value) {
        this.tuning = value;
    }

    /**
     * Ruft den Wert der itemAlterations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ItemAlterationsType }
     *     
     */
    public ItemAlterationsType getItemAlterations() {
        return itemAlterations;
    }

    /**
     * Legt den Wert der itemAlterations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemAlterationsType }
     *     
     */
    public void setItemAlterations(ItemAlterationsType value) {
        this.itemAlterations = value;
    }

}
