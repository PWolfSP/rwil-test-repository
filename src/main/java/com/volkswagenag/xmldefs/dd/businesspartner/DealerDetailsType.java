
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * Dealer information
 * 
 * <p>Java-Klasse f�r DealerDetailsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DealerDetailsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PartnerStatus" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="PartnerStatusSince" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="KindOfPartner" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="DealerType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="DealerClassification" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}DealerClassificationType" minOccurs="0"/>
 *         &lt;element name="MaxDeliveriesPerDay" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}MaxDelivieriesPerDayType" minOccurs="0"/>
 *         &lt;element name="MarkOfArea" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="LocationNumber" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *         &lt;element name="AdditionalServices" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}AdditionalServicesType" minOccurs="0"/>
 *         &lt;element name="Certifications" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}CertificationsType" minOccurs="0"/>
 *         &lt;element name="AgencySaleAllowedIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="BonusLevel" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="StatusOfExclusivity" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="DealerChain" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}DealerChainType" minOccurs="0"/>
 *         &lt;element name="TechnicalEquipment" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}TechnicalEquipmentType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DealerDetailsType", propOrder = {
    "partnerStatus",
    "partnerStatusSince",
    "kindOfPartner",
    "dealerType",
    "dealerClassification",
    "maxDeliveriesPerDay",
    "markOfArea",
    "locationNumber",
    "additionalServices",
    "certifications",
    "agencySaleAllowedIND",
    "bonusLevel",
    "statusOfExclusivity",
    "dealerChain",
    "technicalEquipment"
})
public class DealerDetailsType {

    @XmlElement(name = "PartnerStatus")
    protected CodeType partnerStatus;
    @XmlElement(name = "PartnerStatusSince")
    protected String partnerStatusSince;
    @XmlElement(name = "KindOfPartner")
    protected CodeType kindOfPartner;
    @XmlElement(name = "DealerType")
    protected CodeType dealerType;
    @XmlElement(name = "DealerClassification")
    protected DealerClassificationType dealerClassification;
    @XmlElement(name = "MaxDeliveriesPerDay")
    protected MaxDelivieriesPerDayType maxDeliveriesPerDay;
    @XmlElement(name = "MarkOfArea")
    protected CodeType markOfArea;
    @XmlElement(name = "LocationNumber")
    protected BigDecimal locationNumber;
    @XmlElement(name = "AdditionalServices")
    protected AdditionalServicesType additionalServices;
    @XmlElement(name = "Certifications")
    protected CertificationsType certifications;
    @XmlElement(name = "AgencySaleAllowedIND")
    protected Boolean agencySaleAllowedIND;
    @XmlElement(name = "BonusLevel")
    protected CodeType bonusLevel;
    @XmlElement(name = "StatusOfExclusivity")
    protected CodeType statusOfExclusivity;
    @XmlElement(name = "DealerChain")
    protected DealerChainType dealerChain;
    @XmlElement(name = "TechnicalEquipment")
    protected TechnicalEquipmentType technicalEquipment;

    /**
     * Ruft den Wert der partnerStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getPartnerStatus() {
        return partnerStatus;
    }

    /**
     * Legt den Wert der partnerStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setPartnerStatus(CodeType value) {
        this.partnerStatus = value;
    }

    /**
     * Ruft den Wert der partnerStatusSince-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerStatusSince() {
        return partnerStatusSince;
    }

    /**
     * Legt den Wert der partnerStatusSince-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerStatusSince(String value) {
        this.partnerStatusSince = value;
    }

    /**
     * Ruft den Wert der kindOfPartner-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getKindOfPartner() {
        return kindOfPartner;
    }

    /**
     * Legt den Wert der kindOfPartner-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setKindOfPartner(CodeType value) {
        this.kindOfPartner = value;
    }

    /**
     * Ruft den Wert der dealerType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getDealerType() {
        return dealerType;
    }

    /**
     * Legt den Wert der dealerType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setDealerType(CodeType value) {
        this.dealerType = value;
    }

    /**
     * Ruft den Wert der dealerClassification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DealerClassificationType }
     *     
     */
    public DealerClassificationType getDealerClassification() {
        return dealerClassification;
    }

    /**
     * Legt den Wert der dealerClassification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DealerClassificationType }
     *     
     */
    public void setDealerClassification(DealerClassificationType value) {
        this.dealerClassification = value;
    }

    /**
     * Ruft den Wert der maxDeliveriesPerDay-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MaxDelivieriesPerDayType }
     *     
     */
    public MaxDelivieriesPerDayType getMaxDeliveriesPerDay() {
        return maxDeliveriesPerDay;
    }

    /**
     * Legt den Wert der maxDeliveriesPerDay-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MaxDelivieriesPerDayType }
     *     
     */
    public void setMaxDeliveriesPerDay(MaxDelivieriesPerDayType value) {
        this.maxDeliveriesPerDay = value;
    }

    /**
     * Ruft den Wert der markOfArea-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getMarkOfArea() {
        return markOfArea;
    }

    /**
     * Legt den Wert der markOfArea-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setMarkOfArea(CodeType value) {
        this.markOfArea = value;
    }

    /**
     * Ruft den Wert der locationNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLocationNumber() {
        return locationNumber;
    }

    /**
     * Legt den Wert der locationNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLocationNumber(BigDecimal value) {
        this.locationNumber = value;
    }

    /**
     * Ruft den Wert der additionalServices-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AdditionalServicesType }
     *     
     */
    public AdditionalServicesType getAdditionalServices() {
        return additionalServices;
    }

    /**
     * Legt den Wert der additionalServices-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AdditionalServicesType }
     *     
     */
    public void setAdditionalServices(AdditionalServicesType value) {
        this.additionalServices = value;
    }

    /**
     * Ruft den Wert der certifications-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CertificationsType }
     *     
     */
    public CertificationsType getCertifications() {
        return certifications;
    }

    /**
     * Legt den Wert der certifications-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CertificationsType }
     *     
     */
    public void setCertifications(CertificationsType value) {
        this.certifications = value;
    }

    /**
     * Ruft den Wert der agencySaleAllowedIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAgencySaleAllowedIND() {
        return agencySaleAllowedIND;
    }

    /**
     * Legt den Wert der agencySaleAllowedIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAgencySaleAllowedIND(Boolean value) {
        this.agencySaleAllowedIND = value;
    }

    /**
     * Ruft den Wert der bonusLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getBonusLevel() {
        return bonusLevel;
    }

    /**
     * Legt den Wert der bonusLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setBonusLevel(CodeType value) {
        this.bonusLevel = value;
    }

    /**
     * Ruft den Wert der statusOfExclusivity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getStatusOfExclusivity() {
        return statusOfExclusivity;
    }

    /**
     * Legt den Wert der statusOfExclusivity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setStatusOfExclusivity(CodeType value) {
        this.statusOfExclusivity = value;
    }

    /**
     * Ruft den Wert der dealerChain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DealerChainType }
     *     
     */
    public DealerChainType getDealerChain() {
        return dealerChain;
    }

    /**
     * Legt den Wert der dealerChain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DealerChainType }
     *     
     */
    public void setDealerChain(DealerChainType value) {
        this.dealerChain = value;
    }

    /**
     * Ruft den Wert der technicalEquipment-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalEquipmentType }
     *     
     */
    public TechnicalEquipmentType getTechnicalEquipment() {
        return technicalEquipment;
    }

    /**
     * Legt den Wert der technicalEquipment-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalEquipmentType }
     *     
     */
    public void setTechnicalEquipment(TechnicalEquipmentType value) {
        this.technicalEquipment = value;
    }

}
