
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für PartnerIdentifierType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PartnerIdentifierType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PartnerUID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="ApplicationIdentifiers" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}ApplicationIdentifiersType" minOccurs="0"/>
 *         &lt;element name="BusinessIdentifiers" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}BusinessIdentifiersType" minOccurs="0"/>
 *         &lt;element name="DealerIdentifier" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}DealerIdentifierType" minOccurs="0"/>
 *         &lt;element name="CustomerIdentifier" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}CustomerIdentifierType" minOccurs="0"/>
 *         &lt;element name="SalesPersonIdentifier" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}SalesPersonIdentifierType" minOccurs="0"/>
 *         &lt;element name="WholesalerIdentifier" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}WholesalerIdentifierType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartnerIdentifierType", propOrder = {
    "partnerUID",
    "applicationIdentifiers",
    "businessIdentifiers",
    "dealerIdentifier",
    "customerIdentifier",
    "salesPersonIdentifier",
    "wholesalerIdentifier"
})
public class PartnerIdentifierType {

    @XmlElement(name = "PartnerUID")
    protected IdentifierType partnerUID;
    @XmlElement(name = "ApplicationIdentifiers")
    protected ApplicationIdentifiersType applicationIdentifiers;
    @XmlElement(name = "BusinessIdentifiers")
    protected BusinessIdentifiersType businessIdentifiers;
    @XmlElement(name = "DealerIdentifier")
    protected DealerIdentifierType dealerIdentifier;
    @XmlElement(name = "CustomerIdentifier")
    protected CustomerIdentifierType customerIdentifier;
    @XmlElement(name = "SalesPersonIdentifier")
    protected SalesPersonIdentifierType salesPersonIdentifier;
    @XmlElement(name = "WholesalerIdentifier")
    protected WholesalerIdentifierType wholesalerIdentifier;

    /**
     * Ruft den Wert der partnerUID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getPartnerUID() {
        return partnerUID;
    }

    /**
     * Legt den Wert der partnerUID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setPartnerUID(IdentifierType value) {
        this.partnerUID = value;
    }

    /**
     * Ruft den Wert der applicationIdentifiers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationIdentifiersType }
     *     
     */
    public ApplicationIdentifiersType getApplicationIdentifiers() {
        return applicationIdentifiers;
    }

    /**
     * Legt den Wert der applicationIdentifiers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationIdentifiersType }
     *     
     */
    public void setApplicationIdentifiers(ApplicationIdentifiersType value) {
        this.applicationIdentifiers = value;
    }

    /**
     * Ruft den Wert der businessIdentifiers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BusinessIdentifiersType }
     *     
     */
    public BusinessIdentifiersType getBusinessIdentifiers() {
        return businessIdentifiers;
    }

    /**
     * Legt den Wert der businessIdentifiers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessIdentifiersType }
     *     
     */
    public void setBusinessIdentifiers(BusinessIdentifiersType value) {
        this.businessIdentifiers = value;
    }

    /**
     * Ruft den Wert der dealerIdentifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DealerIdentifierType }
     *     
     */
    public DealerIdentifierType getDealerIdentifier() {
        return dealerIdentifier;
    }

    /**
     * Legt den Wert der dealerIdentifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DealerIdentifierType }
     *     
     */
    public void setDealerIdentifier(DealerIdentifierType value) {
        this.dealerIdentifier = value;
    }

    /**
     * Ruft den Wert der customerIdentifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CustomerIdentifierType }
     *     
     */
    public CustomerIdentifierType getCustomerIdentifier() {
        return customerIdentifier;
    }

    /**
     * Legt den Wert der customerIdentifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerIdentifierType }
     *     
     */
    public void setCustomerIdentifier(CustomerIdentifierType value) {
        this.customerIdentifier = value;
    }

    /**
     * Ruft den Wert der salesPersonIdentifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SalesPersonIdentifierType }
     *     
     */
    public SalesPersonIdentifierType getSalesPersonIdentifier() {
        return salesPersonIdentifier;
    }

    /**
     * Legt den Wert der salesPersonIdentifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesPersonIdentifierType }
     *     
     */
    public void setSalesPersonIdentifier(SalesPersonIdentifierType value) {
        this.salesPersonIdentifier = value;
    }

    /**
     * Ruft den Wert der wholesalerIdentifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WholesalerIdentifierType }
     *     
     */
    public WholesalerIdentifierType getWholesalerIdentifier() {
        return wholesalerIdentifier;
    }

    /**
     * Legt den Wert der wholesalerIdentifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WholesalerIdentifierType }
     *     
     */
    public void setWholesalerIdentifier(WholesalerIdentifierType value) {
        this.wholesalerIdentifier = value;
    }

}
