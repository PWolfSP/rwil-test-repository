
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.volkswagenag.xmldefs.dd.basictypes.CountryCodeType;

import lombok.Generated;


/**
 * Details about a tax number.
 * 
 * <p>Java-Klasse f�r TaxNumberType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TaxNumberType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TaxNumberOriginCountry" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CountryCodeType" minOccurs="0"/>
 *         &lt;element name="TaxNumberOriginRegion" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="TaxNumberType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="TaxNumberValue" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="ValidFrom" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="ValidTo" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxNumberType", propOrder = {
    "taxNumberOriginCountry",
    "taxNumberOriginRegion",
    "taxNumberType",
    "taxNumberValue",
    "validFrom",
    "validTo"
})
public class TaxNumberType {

    @XmlElement(name = "TaxNumberOriginCountry")
    protected CountryCodeType taxNumberOriginCountry;
    @XmlElement(name = "TaxNumberOriginRegion")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String taxNumberOriginRegion;
    @XmlElement(name = "TaxNumberType")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String taxNumberType;
    @XmlElement(name = "TaxNumberValue")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String taxNumberValue;
    @XmlElement(name = "ValidFrom")
    protected String validFrom;
    @XmlElement(name = "ValidTo")
    protected String validTo;

    /**
     * Ruft den Wert der taxNumberOriginCountry-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CountryCodeType }
     *     
     */
    public CountryCodeType getTaxNumberOriginCountry() {
        return taxNumberOriginCountry;
    }

    /**
     * Legt den Wert der taxNumberOriginCountry-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryCodeType }
     *     
     */
    public void setTaxNumberOriginCountry(CountryCodeType value) {
        this.taxNumberOriginCountry = value;
    }

    /**
     * Ruft den Wert der taxNumberOriginRegion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxNumberOriginRegion() {
        return taxNumberOriginRegion;
    }

    /**
     * Legt den Wert der taxNumberOriginRegion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxNumberOriginRegion(String value) {
        this.taxNumberOriginRegion = value;
    }

    /**
     * Ruft den Wert der taxNumberType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxNumberType() {
        return taxNumberType;
    }

    /**
     * Legt den Wert der taxNumberType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxNumberType(String value) {
        this.taxNumberType = value;
    }

    /**
     * Ruft den Wert der taxNumberValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxNumberValue() {
        return taxNumberValue;
    }

    /**
     * Legt den Wert der taxNumberValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxNumberValue(String value) {
        this.taxNumberValue = value;
    }

    /**
     * Ruft den Wert der validFrom-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidFrom() {
        return validFrom;
    }

    /**
     * Legt den Wert der validFrom-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidFrom(String value) {
        this.validFrom = value;
    }

    /**
     * Ruft den Wert der validTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidTo() {
        return validTo;
    }

    /**
     * Legt den Wert der validTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidTo(String value) {
        this.validTo = value;
    }

}
