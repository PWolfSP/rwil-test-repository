
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * Details about the tax.
 * 
 * <p>Java-Klasse für TaxDataType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TaxDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TaxNumbers" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}TaxNumbersType" minOccurs="0"/>
 *         &lt;element name="TaxExceptions" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}TaxExceptionsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxDataType", propOrder = {
    "taxNumbers",
    "taxExceptions"
})
public class TaxDataType {

    @XmlElement(name = "TaxNumbers")
    protected TaxNumbersType taxNumbers;
    @XmlElement(name = "TaxExceptions")
    protected TaxExceptionsType taxExceptions;

    /**
     * Ruft den Wert der taxNumbers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TaxNumbersType }
     *     
     */
    public TaxNumbersType getTaxNumbers() {
        return taxNumbers;
    }

    /**
     * Legt den Wert der taxNumbers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxNumbersType }
     *     
     */
    public void setTaxNumbers(TaxNumbersType value) {
        this.taxNumbers = value;
    }

    /**
     * Ruft den Wert der taxExceptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TaxExceptionsType }
     *     
     */
    public TaxExceptionsType getTaxExceptions() {
        return taxExceptions;
    }

    /**
     * Legt den Wert der taxExceptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxExceptionsType }
     *     
     */
    public void setTaxExceptions(TaxExceptionsType value) {
        this.taxExceptions = value;
    }

}
