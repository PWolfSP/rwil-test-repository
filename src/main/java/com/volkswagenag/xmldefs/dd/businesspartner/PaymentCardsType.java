
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für PaymentCardsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PaymentCardsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaymentCard" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PaymentCardType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentCardsType", propOrder = {
    "paymentCard"
})



public class PaymentCardsType {
	
	
    @XmlElement(name = "PaymentCard", required = true)
    private List<PaymentCardType> paymentCard;


		public void setPaymentCardType(List<PaymentCardType> paymentCard) {
			this.paymentCard = paymentCard;
		}
		
	    public List<PaymentCardType> getPaymentCard() {
	        if (paymentCard == null) {
	            paymentCard = new ArrayList<PaymentCardType>();
	        }
	        return this.paymentCard;
	    }

}
