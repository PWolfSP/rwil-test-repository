
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ContactPersonType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ContactPersonType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Functions" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}FunctionsType" minOccurs="0"/>
 *         &lt;element name="Department" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="MainAddress" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}MainAddressType" minOccurs="0"/>
 *         &lt;element name="Influence" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="JobTitle" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactPersonType", propOrder = {
    "functions",
    "department",
    "mainAddress",
    "influence",
    "jobTitle"
})
public class ContactPersonType {

    @XmlElement(name = "Functions")
    protected FunctionsType functions;
    @XmlElement(name = "Department")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String department;
    @XmlElement(name = "MainAddress")
    protected MainAddressType mainAddress;
    @XmlElement(name = "Influence")
    protected CodeType influence;
    @XmlElement(name = "JobTitle")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String jobTitle;

    /**
     * Ruft den Wert der functions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FunctionsType }
     *     
     */
    public FunctionsType getFunctions() {
        return functions;
    }

    /**
     * Legt den Wert der functions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FunctionsType }
     *     
     */
    public void setFunctions(FunctionsType value) {
        this.functions = value;
    }

    /**
     * Ruft den Wert der department-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartment() {
        return department;
    }

    /**
     * Legt den Wert der department-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartment(String value) {
        this.department = value;
    }

    /**
     * Ruft den Wert der mainAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MainAddressType }
     *     
     */
    public MainAddressType getMainAddress() {
        return mainAddress;
    }

    /**
     * Legt den Wert der mainAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MainAddressType }
     *     
     */
    public void setMainAddress(MainAddressType value) {
        this.mainAddress = value;
    }

    /**
     * Ruft den Wert der influence-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getInfluence() {
        return influence;
    }

    /**
     * Legt den Wert der influence-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setInfluence(CodeType value) {
        this.influence = value;
    }

    /**
     * Ruft den Wert der jobTitle-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     * Legt den Wert der jobTitle-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobTitle(String value) {
        this.jobTitle = value;
    }

}
