
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für AdditionalServicesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AdditionalServicesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AdditionalService" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdditionalServicesType", propOrder = {
    "additionalService"
})



public class AdditionalServicesType {
	
	
    @XmlElement(name = "AdditionalService", required = true)
    private List<CodeType> additionalService;


		public void setCodeType(List<CodeType> additionalService) {
			this.additionalService = additionalService;
		}
		
	    public List<CodeType> getAdditionalService() {
	        if (additionalService == null) {
	            additionalService = new ArrayList<CodeType>();
	        }
	        return this.additionalService;
	    }

}
