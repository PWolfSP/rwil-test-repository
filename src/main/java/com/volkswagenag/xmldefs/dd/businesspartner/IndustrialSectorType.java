
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * Industrial sector
 * 
 * <p>Java-Klasse für IndustrialSectorType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="IndustrialSectorType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Trade" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="IndustrySector" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="IndustrySectorOrderSignOne" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="IndustrySectorOrderSignTwo" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IndustrialSectorType", propOrder = {
    "trade",
    "industrySector",
    "industrySectorOrderSignOne",
    "industrySectorOrderSignTwo"
})
public class IndustrialSectorType {

    @XmlElement(name = "Trade")
    protected CodeType trade;
    @XmlElement(name = "IndustrySector")
    protected CodeType industrySector;
    @XmlElement(name = "IndustrySectorOrderSignOne")
    protected CodeType industrySectorOrderSignOne;
    @XmlElement(name = "IndustrySectorOrderSignTwo")
    protected CodeType industrySectorOrderSignTwo;

    /**
     * Ruft den Wert der trade-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getTrade() {
        return trade;
    }

    /**
     * Legt den Wert der trade-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setTrade(CodeType value) {
        this.trade = value;
    }

    /**
     * Ruft den Wert der industrySector-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getIndustrySector() {
        return industrySector;
    }

    /**
     * Legt den Wert der industrySector-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setIndustrySector(CodeType value) {
        this.industrySector = value;
    }

    /**
     * Ruft den Wert der industrySectorOrderSignOne-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getIndustrySectorOrderSignOne() {
        return industrySectorOrderSignOne;
    }

    /**
     * Legt den Wert der industrySectorOrderSignOne-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setIndustrySectorOrderSignOne(CodeType value) {
        this.industrySectorOrderSignOne = value;
    }

    /**
     * Ruft den Wert der industrySectorOrderSignTwo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getIndustrySectorOrderSignTwo() {
        return industrySectorOrderSignTwo;
    }

    /**
     * Legt den Wert der industrySectorOrderSignTwo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setIndustrySectorOrderSignTwo(CodeType value) {
        this.industrySectorOrderSignTwo = value;
    }

}
