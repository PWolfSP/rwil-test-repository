
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse f�r MarketingBlockadeType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MarketingBlockadeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BlockType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="BlockedIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *         &lt;element name="LastChangeDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="LastChangeBy" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}LastChangeByType" minOccurs="0"/>
 *         &lt;element name="InboundChannel" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="DealerRef" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PartnerIdentifierType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketingBlockadeType", propOrder = {
    "blockType",
    "blockedIND",
    "lastChangeDate",
    "lastChangeBy",
    "inboundChannel",
    "dealerRef"
})
public class MarketingBlockadeType {

    @XmlElement(name = "BlockType")
    protected CodeType blockType;
    @XmlElement(name = "BlockedIND")
    protected Boolean blockedIND;
    @XmlElement(name = "LastChangeDate")
    protected String lastChangeDate;
    @XmlElement(name = "LastChangeBy")
    protected LastChangeByType lastChangeBy;
    @XmlElement(name = "InboundChannel")
    protected CodeType inboundChannel;
    @XmlElement(name = "DealerRef")
    protected PartnerIdentifierType dealerRef;

    /**
     * Ruft den Wert der blockType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getBlockType() {
        return blockType;
    }

    /**
     * Legt den Wert der blockType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setBlockType(CodeType value) {
        this.blockType = value;
    }

    /**
     * Ruft den Wert der blockedIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBlockedIND() {
        return blockedIND;
    }

    /**
     * Legt den Wert der blockedIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBlockedIND(Boolean value) {
        this.blockedIND = value;
    }

    /**
     * Ruft den Wert der lastChangeDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastChangeDate() {
        return lastChangeDate;
    }

    /**
     * Legt den Wert der lastChangeDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastChangeDate(String value) {
        this.lastChangeDate = value;
    }

    /**
     * Ruft den Wert der lastChangeBy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LastChangeByType }
     *     
     */
    public LastChangeByType getLastChangeBy() {
        return lastChangeBy;
    }

    /**
     * Legt den Wert der lastChangeBy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LastChangeByType }
     *     
     */
    public void setLastChangeBy(LastChangeByType value) {
        this.lastChangeBy = value;
    }

    /**
     * Ruft den Wert der inboundChannel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getInboundChannel() {
        return inboundChannel;
    }

    /**
     * Legt den Wert der inboundChannel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setInboundChannel(CodeType value) {
        this.inboundChannel = value;
    }

    /**
     * Ruft den Wert der dealerRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public PartnerIdentifierType getDealerRef() {
        return dealerRef;
    }

    /**
     * Legt den Wert der dealerRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public void setDealerRef(PartnerIdentifierType value) {
        this.dealerRef = value;
    }

}
