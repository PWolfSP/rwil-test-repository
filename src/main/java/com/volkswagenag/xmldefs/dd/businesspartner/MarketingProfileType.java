
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * List of marketing attributes.
 * 
 * <p>Java-Klasse für MarketingProfileType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MarketingProfileType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProfileAttribute" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}MarketingProfileAttributeType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketingProfileType", propOrder = {
    "profileAttribute"
})



public class MarketingProfileType {
	
	
    @XmlElement(name = "ProfileAttribute", required = true)
    private List<MarketingProfileAttributeType> profileAttribute;


		public void setMarketingProfileAttributeType(List<MarketingProfileAttributeType> profileAttribute) {
			this.profileAttribute = profileAttribute;
		}
		
	    public List<MarketingProfileAttributeType> getProfileAttribute() {
	        if (profileAttribute == null) {
	            profileAttribute = new ArrayList<MarketingProfileAttributeType>();
	        }
	        return this.profileAttribute;
	    }

}
