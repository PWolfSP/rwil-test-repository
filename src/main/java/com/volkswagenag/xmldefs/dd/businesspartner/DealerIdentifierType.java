
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für DealerIdentifierType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DealerIdentifierType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PartnerKey" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PartnerKeyType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DealerIdentifierType", propOrder = {
    "partnerKey"
})
public class DealerIdentifierType {

    @XmlElement(name = "PartnerKey")
    protected PartnerKeyType partnerKey;

    /**
     * Ruft den Wert der partnerKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerKeyType }
     *     
     */
    public PartnerKeyType getPartnerKey() {
        return partnerKey;
    }

    /**
     * Legt den Wert der partnerKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerKeyType }
     *     
     */
    public void setPartnerKey(PartnerKeyType value) {
        this.partnerKey = value;
    }

}
