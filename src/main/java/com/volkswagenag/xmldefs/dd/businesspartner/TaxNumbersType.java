
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für TaxNumbersType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TaxNumbersType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TaxNumber" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}TaxNumberType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxNumbersType", propOrder = {
    "taxNumber"
})



public class TaxNumbersType {
	
	
    @XmlElement(name = "TaxNumber", required = true)
    private List<TaxNumberType> taxNumber;


		public void setTaxNumberType(List<TaxNumberType> taxNumber) {
			this.taxNumber = taxNumber;
		}
		
	    public List<TaxNumberType> getTaxNumber() {
	        if (taxNumber == null) {
	            taxNumber = new ArrayList<TaxNumberType>();
	        }
	        return this.taxNumber;
	    }

}
