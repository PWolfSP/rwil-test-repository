
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für PartnerKindChoiceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PartnerKindChoiceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PartnerKind" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PartnerKindChoiceCodelist"/>
 *         &lt;element name="Person" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PersonType" minOccurs="0"/>
 *         &lt;element name="Organization" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}OrganizationType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartnerKindChoiceType", propOrder = {
    "partnerKind",
    "person",
    "organization"
})
public class PartnerKindChoiceType {

    @XmlElement(name = "PartnerKind", required = true)
    @XmlSchemaType(name = "string")
    protected PartnerKindChoiceCodelist partnerKind;
    @XmlElement(name = "Person")
    protected PersonType person;
    @XmlElement(name = "Organization")
    protected OrganizationType organization;

    /**
     * Ruft den Wert der partnerKind-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerKindChoiceCodelist }
     *     
     */
    public PartnerKindChoiceCodelist getPartnerKind() {
        return partnerKind;
    }

    /**
     * Legt den Wert der partnerKind-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerKindChoiceCodelist }
     *     
     */
    public void setPartnerKind(PartnerKindChoiceCodelist value) {
        this.partnerKind = value;
    }

    /**
     * Ruft den Wert der person-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PersonType }
     *     
     */
    public PersonType getPerson() {
        return person;
    }

    /**
     * Legt den Wert der person-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonType }
     *     
     */
    public void setPerson(PersonType value) {
        this.person = value;
    }

    /**
     * Ruft den Wert der organization-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationType }
     *     
     */
    public OrganizationType getOrganization() {
        return organization;
    }

    /**
     * Legt den Wert der organization-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationType }
     *     
     */
    public void setOrganization(OrganizationType value) {
        this.organization = value;
    }

}
