
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * Marketing Permission which describes if contact to the BusinessPartner is allowed or
 *           denied.
 * 
 * <p>Java-Klasse für MarketingAllowancesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MarketingAllowancesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MarketingAllowance" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}MarketingAllowanceType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketingAllowancesType", propOrder = {
    "marketingAllowance"
})



public class MarketingAllowancesType {
	
	
    @XmlElement(name = "MarketingAllowance", required = true)
    private List<MarketingAllowanceType> marketingAllowance;


		public void setMarketingAllowanceType(List<MarketingAllowanceType> marketingAllowance) {
			this.marketingAllowance = marketingAllowance;
		}
		
	    public List<MarketingAllowanceType> getMarketingAllowance() {
	        if (marketingAllowance == null) {
	            marketingAllowance = new ArrayList<MarketingAllowanceType>();
	        }
	        return this.marketingAllowance;
	    }

}
