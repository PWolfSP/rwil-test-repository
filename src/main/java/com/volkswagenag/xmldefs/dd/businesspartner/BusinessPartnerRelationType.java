
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * Business partner relation. The 'RelationType' reflects the type of the
 *           relation.
 * 
 * <p>Java-Klasse f�r BusinessPartnerRelationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BusinessPartnerRelationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BusinessPartnerRelationIdentifier" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}BusinessPartnerRelationIdentifierType" minOccurs="0"/>
 *         &lt;element name="RelationTypeChoice" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}RelationTypeChoice" minOccurs="0"/>
 *         &lt;element name="RelationType" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="PartnerOne" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PartnerIdentifierType" minOccurs="0"/>
 *         &lt;element name="PartnerTwo" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PartnerIdentifierType" minOccurs="0"/>
 *         &lt;element name="ValidFromDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="ValidToDate" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}DateType" minOccurs="0"/>
 *         &lt;element name="CommunicationChannels" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}CommunicationChannelsType" minOccurs="0"/>
 *         &lt;element name="ProcessingInformation" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}BPRelationProcessingInformationType" minOccurs="0"/>
 *         &lt;element name="AdditionalNotes" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}AdditionalNotesType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessPartnerRelationType", propOrder = {
    "businessPartnerRelationIdentifier",
    "relationTypeChoice",
    "relationType",
    "partnerOne",
    "partnerTwo",
    "validFromDate",
    "validToDate",
    "communicationChannels",
    "processingInformation",
    "additionalNotes"
})
public class BusinessPartnerRelationType {

    @XmlElement(name = "BusinessPartnerRelationIdentifier")
    protected BusinessPartnerRelationIdentifierType businessPartnerRelationIdentifier;
    @XmlElement(name = "RelationTypeChoice")
    protected RelationTypeChoice relationTypeChoice;
    @XmlElement(name = "RelationType")
    protected CodeType relationType;
    @XmlElement(name = "PartnerOne")
    protected PartnerIdentifierType partnerOne;
    @XmlElement(name = "PartnerTwo")
    protected PartnerIdentifierType partnerTwo;
    @XmlElement(name = "ValidFromDate")
    protected String validFromDate;
    @XmlElement(name = "ValidToDate")
    protected String validToDate;
    @XmlElement(name = "CommunicationChannels")
    protected CommunicationChannelsType communicationChannels;
    @XmlElement(name = "ProcessingInformation")
    protected BPRelationProcessingInformationType processingInformation;
    @XmlElement(name = "AdditionalNotes")
    protected AdditionalNotesType additionalNotes;

    /**
     * Ruft den Wert der businessPartnerRelationIdentifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BusinessPartnerRelationIdentifierType }
     *     
     */
    public BusinessPartnerRelationIdentifierType getBusinessPartnerRelationIdentifier() {
        return businessPartnerRelationIdentifier;
    }

    /**
     * Legt den Wert der businessPartnerRelationIdentifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessPartnerRelationIdentifierType }
     *     
     */
    public void setBusinessPartnerRelationIdentifier(BusinessPartnerRelationIdentifierType value) {
        this.businessPartnerRelationIdentifier = value;
    }

    /**
     * Ruft den Wert der relationTypeChoice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RelationTypeChoice }
     *     
     */
    public RelationTypeChoice getRelationTypeChoice() {
        return relationTypeChoice;
    }

    /**
     * Legt den Wert der relationTypeChoice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RelationTypeChoice }
     *     
     */
    public void setRelationTypeChoice(RelationTypeChoice value) {
        this.relationTypeChoice = value;
    }

    /**
     * Ruft den Wert der relationType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getRelationType() {
        return relationType;
    }

    /**
     * Legt den Wert der relationType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setRelationType(CodeType value) {
        this.relationType = value;
    }

    /**
     * Ruft den Wert der partnerOne-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public PartnerIdentifierType getPartnerOne() {
        return partnerOne;
    }

    /**
     * Legt den Wert der partnerOne-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public void setPartnerOne(PartnerIdentifierType value) {
        this.partnerOne = value;
    }

    /**
     * Ruft den Wert der partnerTwo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public PartnerIdentifierType getPartnerTwo() {
        return partnerTwo;
    }

    /**
     * Legt den Wert der partnerTwo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerIdentifierType }
     *     
     */
    public void setPartnerTwo(PartnerIdentifierType value) {
        this.partnerTwo = value;
    }

    /**
     * Ruft den Wert der validFromDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidFromDate() {
        return validFromDate;
    }

    /**
     * Legt den Wert der validFromDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidFromDate(String value) {
        this.validFromDate = value;
    }

    /**
     * Ruft den Wert der validToDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidToDate() {
        return validToDate;
    }

    /**
     * Legt den Wert der validToDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidToDate(String value) {
        this.validToDate = value;
    }

    /**
     * Ruft den Wert der communicationChannels-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationChannelsType }
     *     
     */
    public CommunicationChannelsType getCommunicationChannels() {
        return communicationChannels;
    }

    /**
     * Legt den Wert der communicationChannels-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationChannelsType }
     *     
     */
    public void setCommunicationChannels(CommunicationChannelsType value) {
        this.communicationChannels = value;
    }

    /**
     * Ruft den Wert der processingInformation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BPRelationProcessingInformationType }
     *     
     */
    public BPRelationProcessingInformationType getProcessingInformation() {
        return processingInformation;
    }

    /**
     * Legt den Wert der processingInformation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BPRelationProcessingInformationType }
     *     
     */
    public void setProcessingInformation(BPRelationProcessingInformationType value) {
        this.processingInformation = value;
    }

    /**
     * Ruft den Wert der additionalNotes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AdditionalNotesType }
     *     
     */
    public AdditionalNotesType getAdditionalNotes() {
        return additionalNotes;
    }

    /**
     * Legt den Wert der additionalNotes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AdditionalNotesType }
     *     
     */
    public void setAdditionalNotes(AdditionalNotesType value) {
        this.additionalNotes = value;
    }

}
