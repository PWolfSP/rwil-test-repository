
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für SubStatusType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubStatusType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubStatus" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="SubStatusValidityRange" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}ValidityRangeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubStatusType", propOrder = {
    "subStatus",
    "subStatusValidityRange"
})
public class SubStatusType {

    @XmlElement(name = "SubStatus")
    protected CodeType subStatus;
    @XmlElement(name = "SubStatusValidityRange")
    protected ValidityRangeType subStatusValidityRange;

    /**
     * Ruft den Wert der subStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getSubStatus() {
        return subStatus;
    }

    /**
     * Legt den Wert der subStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setSubStatus(CodeType value) {
        this.subStatus = value;
    }

    /**
     * Ruft den Wert der subStatusValidityRange-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ValidityRangeType }
     *     
     */
    public ValidityRangeType getSubStatusValidityRange() {
        return subStatusValidityRange;
    }

    /**
     * Legt den Wert der subStatusValidityRange-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidityRangeType }
     *     
     */
    public void setSubStatusValidityRange(ValidityRangeType value) {
        this.subStatusValidityRange = value;
    }

}
