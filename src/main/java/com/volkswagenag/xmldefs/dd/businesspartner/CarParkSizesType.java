
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für CarParkSizesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CarParkSizesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CarParkSize" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}CarParkSizeType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CarParkSizesType", propOrder = {
    "carParkSize"
})



public class CarParkSizesType {
	
	
    @XmlElement(name = "CarParkSize", required = true)
    private List<CarParkSizeType> carParkSize;


		public void setCarParkSizeType(List<CarParkSizeType> carParkSize) {
			this.carParkSize = carParkSize;
		}
		
	    public List<CarParkSizeType> getCarParkSize() {
	        if (carParkSize == null) {
	            carParkSize = new ArrayList<CarParkSizeType>();
	        }
	        return this.carParkSize;
	    }

}
