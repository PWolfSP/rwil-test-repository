
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * Customer details.
 * 
 * <p>Java-Klasse für CustomerDetailsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CustomerDetailsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MarketingAllowances" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}MarketingAllowancesType" minOccurs="0"/>
 *         &lt;element name="FleetCustomerDetails" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}FleetCustomerDetailsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerDetailsType", propOrder = {
    "marketingAllowances",
    "fleetCustomerDetails"
})
public class CustomerDetailsType {

    @XmlElement(name = "MarketingAllowances")
    protected MarketingAllowancesType marketingAllowances;
    @XmlElement(name = "FleetCustomerDetails")
    protected FleetCustomerDetailsType fleetCustomerDetails;

    /**
     * Ruft den Wert der marketingAllowances-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MarketingAllowancesType }
     *     
     */
    public MarketingAllowancesType getMarketingAllowances() {
        return marketingAllowances;
    }

    /**
     * Legt den Wert der marketingAllowances-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MarketingAllowancesType }
     *     
     */
    public void setMarketingAllowances(MarketingAllowancesType value) {
        this.marketingAllowances = value;
    }

    /**
     * Ruft den Wert der fleetCustomerDetails-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FleetCustomerDetailsType }
     *     
     */
    public FleetCustomerDetailsType getFleetCustomerDetails() {
        return fleetCustomerDetails;
    }

    /**
     * Legt den Wert der fleetCustomerDetails-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FleetCustomerDetailsType }
     *     
     */
    public void setFleetCustomerDetails(FleetCustomerDetailsType value) {
        this.fleetCustomerDetails = value;
    }

}
