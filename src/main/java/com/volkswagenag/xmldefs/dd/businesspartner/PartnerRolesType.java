
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für PartnerRolesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PartnerRolesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Role" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PartnerRoleType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartnerRolesType", propOrder = {
    "role"
})



public class PartnerRolesType {
	
	
    @XmlElement(name = "Role", required = true)
    private List<PartnerRoleType> role;


		public void setPartnerRoleType(List<PartnerRoleType> role) {
			this.role = role;
		}
		
	    public List<PartnerRoleType> getRole() {
	        if (role == null) {
	            role = new ArrayList<PartnerRoleType>();
	        }
	        return this.role;
	    }

}
