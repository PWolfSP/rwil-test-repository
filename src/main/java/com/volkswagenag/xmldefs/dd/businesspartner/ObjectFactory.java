
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import lombok.Generated;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.volkswagenag.xmldefs.dd.businesspartner package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@Generated
@XmlRegistry
public class ObjectFactory {

    private final static QName _Dealer_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/BusinessPartner", "Dealer");
    private final static QName _CustomerRef_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/BusinessPartner", "CustomerRef");
    private final static QName _Employee_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/BusinessPartner", "Employee");
    private final static QName _SalesPersonRef_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/BusinessPartner", "SalesPersonRef");
    private final static QName _BusinessPartnerCompound_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/BusinessPartner", "BusinessPartnerCompound");
    private final static QName _Wholesaler_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/BusinessPartner", "Wholesaler");
    private final static QName _BusinessPartnerRelation_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/BusinessPartner", "BusinessPartnerRelation");
    private final static QName _DealerRef_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/BusinessPartner", "DealerRef");
    private final static QName _EmployeeRef_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/BusinessPartner", "EmployeeRef");
    private final static QName _Customer_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/BusinessPartner", "Customer");
    private final static QName _SalesPerson_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/BusinessPartner", "SalesPerson");
    private final static QName _BusinessPartnerBasic_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/BusinessPartner", "BusinessPartnerBasic");
    private final static QName _BusinessPartnerRef_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/BusinessPartner", "BusinessPartnerRef");
    private final static QName _WholesalerRef_QNAME = new QName("http://xmldefs.volkswagenag.com/DD/BusinessPartner", "WholesalerRef");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.volkswagenag.xmldefs.dd.businesspartner
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PartnerIdentifierType }
     * 
     */
    public PartnerIdentifierType createPartnerIdentifierType() {
        return new PartnerIdentifierType();
    }

    /**
     * Create an instance of {@link CustomerType }
     * 
     */
    public CustomerType createCustomerType() {
        return new CustomerType();
    }

    /**
     * Create an instance of {@link SalesPersonType }
     * 
     */
    public SalesPersonType createSalesPersonType() {
        return new SalesPersonType();
    }

    /**
     * Create an instance of {@link EmployeeType }
     * 
     */
    public EmployeeType createEmployeeType() {
        return new EmployeeType();
    }

    /**
     * Create an instance of {@link WholesalerType }
     * 
     */
    public WholesalerType createWholesalerType() {
        return new WholesalerType();
    }

    /**
     * Create an instance of {@link BusinessPartnerRelationType }
     * 
     */
    public BusinessPartnerRelationType createBusinessPartnerRelationType() {
        return new BusinessPartnerRelationType();
    }

    /**
     * Create an instance of {@link BusinessPartnerCompoundType }
     * 
     */
    public BusinessPartnerCompoundType createBusinessPartnerCompoundType() {
        return new BusinessPartnerCompoundType();
    }

    /**
     * Create an instance of {@link BusinessPartnerBasicType }
     * 
     */
    public BusinessPartnerBasicType createBusinessPartnerBasicType() {
        return new BusinessPartnerBasicType();
    }

    /**
     * Create an instance of {@link DealerType }
     * 
     */
    public DealerType createDealerType() {
        return new DealerType();
    }

    /**
     * Create an instance of {@link CertificationType }
     * 
     */
    public CertificationType createCertificationType() {
        return new CertificationType();
    }

    /**
     * Create an instance of {@link AdditionalNoteType }
     * 
     */
    public AdditionalNoteType createAdditionalNoteType() {
        return new AdditionalNoteType();
    }

    /**
     * Create an instance of {@link ReleaseDatesType }
     * 
     */
    public ReleaseDatesType createReleaseDatesType() {
        return new ReleaseDatesType();
    }

    /**
     * Create an instance of {@link ValidityRangeType }
     * 
     */
    public ValidityRangeType createValidityRangeType() {
        return new ValidityRangeType();
    }

    /**
     * Create an instance of {@link DrivingLicenceType }
     * 
     */
    public DrivingLicenceType createDrivingLicenceType() {
        return new DrivingLicenceType();
    }

    /**
     * Create an instance of {@link BankingAccountType }
     * 
     */
    public BankingAccountType createBankingAccountType() {
        return new BankingAccountType();
    }

    /**
     * Create an instance of {@link SalesPersonIdentifierType }
     * 
     */
    public SalesPersonIdentifierType createSalesPersonIdentifierType() {
        return new SalesPersonIdentifierType();
    }

    /**
     * Create an instance of {@link PersonType }
     * 
     */
    public PersonType createPersonType() {
        return new PersonType();
    }

    /**
     * Create an instance of {@link CommunicationChannelsType }
     * 
     */
    public CommunicationChannelsType createCommunicationChannelsType() {
        return new CommunicationChannelsType();
    }

    /**
     * Create an instance of {@link ControlDataType }
     * 
     */
    public ControlDataType createControlDataType() {
        return new ControlDataType();
    }

    /**
     * Create an instance of {@link BankAccountsType }
     * 
     */
    public BankAccountsType createBankAccountsType() {
        return new BankAccountsType();
    }

    /**
     * Create an instance of {@link RegistryEntryType }
     * 
     */
    public RegistryEntryType createRegistryEntryType() {
        return new RegistryEntryType();
    }

    /**
     * Create an instance of {@link DrivingLicenceClassType }
     * 
     */
    public DrivingLicenceClassType createDrivingLicenceClassType() {
        return new DrivingLicenceClassType();
    }

    /**
     * Create an instance of {@link BankAccountUsagesType }
     * 
     */
    public BankAccountUsagesType createBankAccountUsagesType() {
        return new BankAccountUsagesType();
    }

    /**
     * Create an instance of {@link RegistryEntriesType }
     * 
     */
    public RegistryEntriesType createRegistryEntriesType() {
        return new RegistryEntriesType();
    }

    /**
     * Create an instance of {@link PaymentCardType }
     * 
     */
    public PaymentCardType createPaymentCardType() {
        return new PaymentCardType();
    }

    /**
     * Create an instance of {@link LastChangeByType }
     * 
     */
    public LastChangeByType createLastChangeByType() {
        return new LastChangeByType();
    }

    /**
     * Create an instance of {@link AddressUsageType }
     * 
     */
    public AddressUsageType createAddressUsageType() {
        return new AddressUsageType();
    }

    /**
     * Create an instance of {@link OrganizationNameType }
     * 
     */
    public OrganizationNameType createOrganizationNameType() {
        return new OrganizationNameType();
    }

    /**
     * Create an instance of {@link CarParkSizeType }
     * 
     */
    public CarParkSizeType createCarParkSizeType() {
        return new CarParkSizeType();
    }

    /**
     * Create an instance of {@link AdditionalServicesType }
     * 
     */
    public AdditionalServicesType createAdditionalServicesType() {
        return new AdditionalServicesType();
    }

    /**
     * Create an instance of {@link AdditionalNotesType }
     * 
     */
    public AdditionalNotesType createAdditionalNotesType() {
        return new AdditionalNotesType();
    }

    /**
     * Create an instance of {@link OrganizationalDataType }
     * 
     */
    public OrganizationalDataType createOrganizationalDataType() {
        return new OrganizationalDataType();
    }

    /**
     * Create an instance of {@link PaymentCardsType }
     * 
     */
    public PaymentCardsType createPaymentCardsType() {
        return new PaymentCardsType();
    }

    /**
     * Create an instance of {@link MaxDelivieriesPerDayType }
     * 
     */
    public MaxDelivieriesPerDayType createMaxDelivieriesPerDayType() {
        return new MaxDelivieriesPerDayType();
    }

    /**
     * Create an instance of {@link DealerChainType }
     * 
     */
    public DealerChainType createDealerChainType() {
        return new DealerChainType();
    }

    /**
     * Create an instance of {@link DealerSalesAuthorisationType }
     * 
     */
    public DealerSalesAuthorisationType createDealerSalesAuthorisationType() {
        return new DealerSalesAuthorisationType();
    }

    /**
     * Create an instance of {@link CarParkSizesType }
     * 
     */
    public CarParkSizesType createCarParkSizesType() {
        return new CarParkSizesType();
    }

    /**
     * Create an instance of {@link TaxNumbersType }
     * 
     */
    public TaxNumbersType createTaxNumbersType() {
        return new TaxNumbersType();
    }

    /**
     * Create an instance of {@link MarketingProfileAttributeType }
     * 
     */
    public MarketingProfileAttributeType createMarketingProfileAttributeType() {
        return new MarketingProfileAttributeType();
    }

    /**
     * Create an instance of {@link ImportancesType }
     * 
     */
    public ImportancesType createImportancesType() {
        return new ImportancesType();
    }

    /**
     * Create an instance of {@link TimestampsType }
     * 
     */
    public TimestampsType createTimestampsType() {
        return new TimestampsType();
    }

    /**
     * Create an instance of {@link MarketingBlockadesType }
     * 
     */
    public MarketingBlockadesType createMarketingBlockadesType() {
        return new MarketingBlockadesType();
    }

    /**
     * Create an instance of {@link FunctionsType }
     * 
     */
    public FunctionsType createFunctionsType() {
        return new FunctionsType();
    }

    /**
     * Create an instance of {@link BusinessIdentifiersType }
     * 
     */
    public BusinessIdentifiersType createBusinessIdentifiersType() {
        return new BusinessIdentifiersType();
    }

    /**
     * Create an instance of {@link TaxExceptionsType }
     * 
     */
    public TaxExceptionsType createTaxExceptionsType() {
        return new TaxExceptionsType();
    }

    /**
     * Create an instance of {@link PartnerKeyType }
     * 
     */
    public PartnerKeyType createPartnerKeyType() {
        return new PartnerKeyType();
    }

    /**
     * Create an instance of {@link BusinessIdentifierType }
     * 
     */
    public BusinessIdentifierType createBusinessIdentifierType() {
        return new BusinessIdentifierType();
    }

    /**
     * Create an instance of {@link PersonNameType }
     * 
     */
    public PersonNameType createPersonNameType() {
        return new PersonNameType();
    }

    /**
     * Create an instance of {@link SubStatusType }
     * 
     */
    public SubStatusType createSubStatusType() {
        return new SubStatusType();
    }

    /**
     * Create an instance of {@link MaxDelivieryPerDayType }
     * 
     */
    public MaxDelivieryPerDayType createMaxDelivieryPerDayType() {
        return new MaxDelivieryPerDayType();
    }

    /**
     * Create an instance of {@link OwningWholesalerType }
     * 
     */
    public OwningWholesalerType createOwningWholesalerType() {
        return new OwningWholesalerType();
    }

    /**
     * Create an instance of {@link ImportanceType }
     * 
     */
    public ImportanceType createImportanceType() {
        return new ImportanceType();
    }

    /**
     * Create an instance of {@link DealerIdentifierType }
     * 
     */
    public DealerIdentifierType createDealerIdentifierType() {
        return new DealerIdentifierType();
    }

    /**
     * Create an instance of {@link MarketingAllowancesType }
     * 
     */
    public MarketingAllowancesType createMarketingAllowancesType() {
        return new MarketingAllowancesType();
    }

    /**
     * Create an instance of {@link PartnerRolesType }
     * 
     */
    public PartnerRolesType createPartnerRolesType() {
        return new PartnerRolesType();
    }

    /**
     * Create an instance of {@link TechnicalEquipmentType }
     * 
     */
    public TechnicalEquipmentType createTechnicalEquipmentType() {
        return new TechnicalEquipmentType();
    }

    /**
     * Create an instance of {@link MarketingBlockadeType }
     * 
     */
    public MarketingBlockadeType createMarketingBlockadeType() {
        return new MarketingBlockadeType();
    }

    /**
     * Create an instance of {@link IndustrialSectorType }
     * 
     */
    public IndustrialSectorType createIndustrialSectorType() {
        return new IndustrialSectorType();
    }

    /**
     * Create an instance of {@link MarketingProfileType }
     * 
     */
    public MarketingProfileType createMarketingProfileType() {
        return new MarketingProfileType();
    }

    /**
     * Create an instance of {@link CommunicationChannelType }
     * 
     */
    public CommunicationChannelType createCommunicationChannelType() {
        return new CommunicationChannelType();
    }

    /**
     * Create an instance of {@link AddressType }
     * 
     */
    public AddressType createAddressType() {
        return new AddressType();
    }

    /**
     * Create an instance of {@link PartnerType }
     * 
     */
    public PartnerType createPartnerType() {
        return new PartnerType();
    }

    /**
     * Create an instance of {@link ProcessingInformationType }
     * 
     */
    public ProcessingInformationType createProcessingInformationType() {
        return new ProcessingInformationType();
    }

    /**
     * Create an instance of {@link BusinessPartnerRelationIdentifierType }
     * 
     */
    public BusinessPartnerRelationIdentifierType createBusinessPartnerRelationIdentifierType() {
        return new BusinessPartnerRelationIdentifierType();
    }

    /**
     * Create an instance of {@link BankAccountUsageType }
     * 
     */
    public BankAccountUsageType createBankAccountUsageType() {
        return new BankAccountUsageType();
    }

    /**
     * Create an instance of {@link MailType }
     * 
     */
    public MailType createMailType() {
        return new MailType();
    }

    /**
     * Create an instance of {@link PartnerKindChoiceType }
     * 
     */
    public PartnerKindChoiceType createPartnerKindChoiceType() {
        return new PartnerKindChoiceType();
    }

    /**
     * Create an instance of {@link BPRelationTimestampsType }
     * 
     */
    public BPRelationTimestampsType createBPRelationTimestampsType() {
        return new BPRelationTimestampsType();
    }

    /**
     * Create an instance of {@link CustomerIdentifierType }
     * 
     */
    public CustomerIdentifierType createCustomerIdentifierType() {
        return new CustomerIdentifierType();
    }

    /**
     * Create an instance of {@link MarketingAllowanceType }
     * 
     */
    public MarketingAllowanceType createMarketingAllowanceType() {
        return new MarketingAllowanceType();
    }

    /**
     * Create an instance of {@link ChannelTypeChoiceType }
     * 
     */
    public ChannelTypeChoiceType createChannelTypeChoiceType() {
        return new ChannelTypeChoiceType();
    }

    /**
     * Create an instance of {@link InactiveStatusType }
     * 
     */
    public InactiveStatusType createInactiveStatusType() {
        return new InactiveStatusType();
    }

    /**
     * Create an instance of {@link ValidationStatusType }
     * 
     */
    public ValidationStatusType createValidationStatusType() {
        return new ValidationStatusType();
    }

    /**
     * Create an instance of {@link WholesalerIdentifierType }
     * 
     */
    public WholesalerIdentifierType createWholesalerIdentifierType() {
        return new WholesalerIdentifierType();
    }

    /**
     * Create an instance of {@link AuthorityIssuingType }
     * 
     */
    public AuthorityIssuingType createAuthorityIssuingType() {
        return new AuthorityIssuingType();
    }

    /**
     * Create an instance of {@link PostBoxAddressType }
     * 
     */
    public PostBoxAddressType createPostBoxAddressType() {
        return new PostBoxAddressType();
    }

    /**
     * Create an instance of {@link DealerClassificationType }
     * 
     */
    public DealerClassificationType createDealerClassificationType() {
        return new DealerClassificationType();
    }

    /**
     * Create an instance of {@link PhoneType }
     * 
     */
    public PhoneType createPhoneType() {
        return new PhoneType();
    }

    /**
     * Create an instance of {@link RelationTypeChoice }
     * 
     */
    public RelationTypeChoice createRelationTypeChoice() {
        return new RelationTypeChoice();
    }

    /**
     * Create an instance of {@link CertificationsType }
     * 
     */
    public CertificationsType createCertificationsType() {
        return new CertificationsType();
    }

    /**
     * Create an instance of {@link PartnerRoleType }
     * 
     */
    public PartnerRoleType createPartnerRoleType() {
        return new PartnerRoleType();
    }

    /**
     * Create an instance of {@link TaxDataType }
     * 
     */
    public TaxDataType createTaxDataType() {
        return new TaxDataType();
    }

    /**
     * Create an instance of {@link AddressesType }
     * 
     */
    public AddressesType createAddressesType() {
        return new AddressesType();
    }

    /**
     * Create an instance of {@link LanguagesType }
     * 
     */
    public LanguagesType createLanguagesType() {
        return new LanguagesType();
    }

    /**
     * Create an instance of {@link SalesPersonDetailsType }
     * 
     */
    public SalesPersonDetailsType createSalesPersonDetailsType() {
        return new SalesPersonDetailsType();
    }

    /**
     * Create an instance of {@link FleetCustomerDetailsType }
     * 
     */
    public FleetCustomerDetailsType createFleetCustomerDetailsType() {
        return new FleetCustomerDetailsType();
    }

    /**
     * Create an instance of {@link BPRelationProcessingInformationType }
     * 
     */
    public BPRelationProcessingInformationType createBPRelationProcessingInformationType() {
        return new BPRelationProcessingInformationType();
    }

    /**
     * Create an instance of {@link IndustrialSectorsType }
     * 
     */
    public IndustrialSectorsType createIndustrialSectorsType() {
        return new IndustrialSectorsType();
    }

    /**
     * Create an instance of {@link LanguageType }
     * 
     */
    public LanguageType createLanguageType() {
        return new LanguageType();
    }

    /**
     * Create an instance of {@link StreetAddressType }
     * 
     */
    public StreetAddressType createStreetAddressType() {
        return new StreetAddressType();
    }

    /**
     * Create an instance of {@link DealerDetailsType }
     * 
     */
    public DealerDetailsType createDealerDetailsType() {
        return new DealerDetailsType();
    }

    /**
     * Create an instance of {@link MainAddressType }
     * 
     */
    public MainAddressType createMainAddressType() {
        return new MainAddressType();
    }

    /**
     * Create an instance of {@link ApplicationIdentifiersType }
     * 
     */
    public ApplicationIdentifiersType createApplicationIdentifiersType() {
        return new ApplicationIdentifiersType();
    }

    /**
     * Create an instance of {@link OrganizationType }
     * 
     */
    public OrganizationType createOrganizationType() {
        return new OrganizationType();
    }

    /**
     * Create an instance of {@link AddressKindChoiceType }
     * 
     */
    public AddressKindChoiceType createAddressKindChoiceType() {
        return new AddressKindChoiceType();
    }

    /**
     * Create an instance of {@link OwningDealerType }
     * 
     */
    public OwningDealerType createOwningDealerType() {
        return new OwningDealerType();
    }

    /**
     * Create an instance of {@link DrivingLicenceClassesType }
     * 
     */
    public DrivingLicenceClassesType createDrivingLicenceClassesType() {
        return new DrivingLicenceClassesType();
    }

    /**
     * Create an instance of {@link CustomerDetailsType }
     * 
     */
    public CustomerDetailsType createCustomerDetailsType() {
        return new CustomerDetailsType();
    }

    /**
     * Create an instance of {@link ContactPersonType }
     * 
     */
    public ContactPersonType createContactPersonType() {
        return new ContactPersonType();
    }

    /**
     * Create an instance of {@link AddressUsagesType }
     * 
     */
    public AddressUsagesType createAddressUsagesType() {
        return new AddressUsagesType();
    }

    /**
     * Create an instance of {@link TaxNumberType }
     * 
     */
    public TaxNumberType createTaxNumberType() {
        return new TaxNumberType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DealerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", name = "Dealer")
    public JAXBElement<DealerType> createDealer(DealerType value) {
        return new JAXBElement<DealerType>(_Dealer_QNAME, DealerType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartnerIdentifierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", name = "CustomerRef")
    public JAXBElement<PartnerIdentifierType> createCustomerRef(PartnerIdentifierType value) {
        return new JAXBElement<PartnerIdentifierType>(_CustomerRef_QNAME, PartnerIdentifierType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmployeeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", name = "Employee")
    public JAXBElement<EmployeeType> createEmployee(EmployeeType value) {
        return new JAXBElement<EmployeeType>(_Employee_QNAME, EmployeeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartnerIdentifierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", name = "SalesPersonRef")
    public JAXBElement<PartnerIdentifierType> createSalesPersonRef(PartnerIdentifierType value) {
        return new JAXBElement<PartnerIdentifierType>(_SalesPersonRef_QNAME, PartnerIdentifierType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessPartnerCompoundType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", name = "BusinessPartnerCompound")
    public JAXBElement<BusinessPartnerCompoundType> createBusinessPartnerCompound(BusinessPartnerCompoundType value) {
        return new JAXBElement<BusinessPartnerCompoundType>(_BusinessPartnerCompound_QNAME, BusinessPartnerCompoundType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WholesalerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", name = "Wholesaler")
    public JAXBElement<WholesalerType> createWholesaler(WholesalerType value) {
        return new JAXBElement<WholesalerType>(_Wholesaler_QNAME, WholesalerType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessPartnerRelationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", name = "BusinessPartnerRelation")
    public JAXBElement<BusinessPartnerRelationType> createBusinessPartnerRelation(BusinessPartnerRelationType value) {
        return new JAXBElement<BusinessPartnerRelationType>(_BusinessPartnerRelation_QNAME, BusinessPartnerRelationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartnerIdentifierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", name = "DealerRef")
    public JAXBElement<PartnerIdentifierType> createDealerRef(PartnerIdentifierType value) {
        return new JAXBElement<PartnerIdentifierType>(_DealerRef_QNAME, PartnerIdentifierType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartnerIdentifierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", name = "EmployeeRef")
    public JAXBElement<PartnerIdentifierType> createEmployeeRef(PartnerIdentifierType value) {
        return new JAXBElement<PartnerIdentifierType>(_EmployeeRef_QNAME, PartnerIdentifierType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", name = "Customer")
    public JAXBElement<CustomerType> createCustomer(CustomerType value) {
        return new JAXBElement<CustomerType>(_Customer_QNAME, CustomerType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesPersonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", name = "SalesPerson")
    public JAXBElement<SalesPersonType> createSalesPerson(SalesPersonType value) {
        return new JAXBElement<SalesPersonType>(_SalesPerson_QNAME, SalesPersonType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessPartnerBasicType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", name = "BusinessPartnerBasic")
    public JAXBElement<BusinessPartnerBasicType> createBusinessPartnerBasic(BusinessPartnerBasicType value) {
        return new JAXBElement<BusinessPartnerBasicType>(_BusinessPartnerBasic_QNAME, BusinessPartnerBasicType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartnerIdentifierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", name = "BusinessPartnerRef")
    public JAXBElement<PartnerIdentifierType> createBusinessPartnerRef(PartnerIdentifierType value) {
        return new JAXBElement<PartnerIdentifierType>(_BusinessPartnerRef_QNAME, PartnerIdentifierType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartnerIdentifierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/DD/BusinessPartner", name = "WholesalerRef")
    public JAXBElement<PartnerIdentifierType> createWholesalerRef(PartnerIdentifierType value) {
        return new JAXBElement<PartnerIdentifierType>(_WholesalerRef_QNAME, PartnerIdentifierType.class, null, value);
    }

}
