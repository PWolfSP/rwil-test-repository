
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für BankAccountUsagesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BankAccountUsagesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Usage" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}BankAccountUsageType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankAccountUsagesType", propOrder = {
    "usage"
})



public class BankAccountUsagesType {
	
	
    @XmlElement(name = "Usage", required = true)
    private List<BankAccountUsageType> usage;


		public void setBankAccountUsageType(List<BankAccountUsageType> usage) {
			this.usage = usage;
		}
		
	    public List<BankAccountUsageType> getUsage() {
	        if (usage == null) {
	            usage = new ArrayList<BankAccountUsageType>();
	        }
	        return this.usage;
	    }

}
