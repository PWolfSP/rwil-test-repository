
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ControlDataType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ControlDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OriginOfData" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="OriginOfDataDetail" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="AuthorizationGroup" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="ToBeArchivedIND" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IndicatorType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ControlDataType", propOrder = {
    "originOfData",
    "originOfDataDetail",
    "authorizationGroup",
    "toBeArchivedIND"
})
public class ControlDataType {

    @XmlElement(name = "OriginOfData")
    protected CodeType originOfData;
    @XmlElement(name = "OriginOfDataDetail")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String originOfDataDetail;
    @XmlElement(name = "AuthorizationGroup")
    protected CodeType authorizationGroup;
    @XmlElement(name = "ToBeArchivedIND")
    protected Boolean toBeArchivedIND;

    /**
     * Ruft den Wert der originOfData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getOriginOfData() {
        return originOfData;
    }

    /**
     * Legt den Wert der originOfData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setOriginOfData(CodeType value) {
        this.originOfData = value;
    }

    /**
     * Ruft den Wert der originOfDataDetail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginOfDataDetail() {
        return originOfDataDetail;
    }

    /**
     * Legt den Wert der originOfDataDetail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginOfDataDetail(String value) {
        this.originOfDataDetail = value;
    }

    /**
     * Ruft den Wert der authorizationGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getAuthorizationGroup() {
        return authorizationGroup;
    }

    /**
     * Legt den Wert der authorizationGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setAuthorizationGroup(CodeType value) {
        this.authorizationGroup = value;
    }

    /**
     * Ruft den Wert der toBeArchivedIND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isToBeArchivedIND() {
        return toBeArchivedIND;
    }

    /**
     * Legt den Wert der toBeArchivedIND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setToBeArchivedIND(Boolean value) {
        this.toBeArchivedIND = value;
    }

}
