
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für PartnerKindChoiceCodelist.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="PartnerKindChoiceCodelist">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Person"/>
 *     &lt;enumeration value="Organization"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@Generated
@XmlType(name = "PartnerKindChoiceCodelist")
@XmlEnum
public enum PartnerKindChoiceCodelist {

    @XmlEnumValue("Person")
    PERSON("Person"),
    @XmlEnumValue("Organization")
    ORGANIZATION("Organization");
    private final String value;

    PartnerKindChoiceCodelist(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PartnerKindChoiceCodelist fromValue(String v) {
        for (PartnerKindChoiceCodelist c: PartnerKindChoiceCodelist.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
