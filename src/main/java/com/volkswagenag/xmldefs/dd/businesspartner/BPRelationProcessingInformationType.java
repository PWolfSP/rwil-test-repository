
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für BPRelationProcessingInformationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BPRelationProcessingInformationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Timestamps" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}BPRelationTimestampsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BPRelationProcessingInformationType", propOrder = {
    "timestamps"
})
public class BPRelationProcessingInformationType {

    @XmlElement(name = "Timestamps")
    protected BPRelationTimestampsType timestamps;

    /**
     * Ruft den Wert der timestamps-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BPRelationTimestampsType }
     *     
     */
    public BPRelationTimestampsType getTimestamps() {
        return timestamps;
    }

    /**
     * Legt den Wert der timestamps-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BPRelationTimestampsType }
     *     
     */
    public void setTimestamps(BPRelationTimestampsType value) {
        this.timestamps = value;
    }

}
