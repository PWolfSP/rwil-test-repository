
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.basictypes.IdentifierType;

import lombok.Generated;


/**
 * <p>Java-Klasse für BusinessPartnerRelationIdentifierType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BusinessPartnerRelationIdentifierType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BusinessPartnerRelationUID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}IdentifierType" minOccurs="0"/>
 *         &lt;element name="ApplicationIdentifiers" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}ApplicationIdentifiersType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessPartnerRelationIdentifierType", propOrder = {
    "businessPartnerRelationUID",
    "applicationIdentifiers"
})
public class BusinessPartnerRelationIdentifierType {

    @XmlElement(name = "BusinessPartnerRelationUID")
    protected IdentifierType businessPartnerRelationUID;
    @XmlElement(name = "ApplicationIdentifiers")
    protected ApplicationIdentifiersType applicationIdentifiers;

    /**
     * Ruft den Wert der businessPartnerRelationUID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getBusinessPartnerRelationUID() {
        return businessPartnerRelationUID;
    }

    /**
     * Legt den Wert der businessPartnerRelationUID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setBusinessPartnerRelationUID(IdentifierType value) {
        this.businessPartnerRelationUID = value;
    }

    /**
     * Ruft den Wert der applicationIdentifiers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationIdentifiersType }
     *     
     */
    public ApplicationIdentifiersType getApplicationIdentifiers() {
        return applicationIdentifiers;
    }

    /**
     * Legt den Wert der applicationIdentifiers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationIdentifiersType }
     *     
     */
    public void setApplicationIdentifiers(ApplicationIdentifiersType value) {
        this.applicationIdentifiers = value;
    }

}
