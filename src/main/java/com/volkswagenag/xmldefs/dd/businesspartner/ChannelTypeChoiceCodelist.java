
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * <p>Java-Klasse für ChannelTypeChoiceCodelist.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ChannelTypeChoiceCodelist">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Mail"/>
 *     &lt;enumeration value="Phone"/>
 *     &lt;enumeration value="Mobile"/>
 *     &lt;enumeration value="SMS"/>
 *     &lt;enumeration value="Fax"/>
 *     &lt;enumeration value="Email"/>
 *     &lt;enumeration value="URL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@Generated
@XmlType(name = "ChannelTypeChoiceCodelist")
@XmlEnum
public enum ChannelTypeChoiceCodelist {

    @XmlEnumValue("Mail")
    MAIL("Mail"),
    @XmlEnumValue("Phone")
    PHONE("Phone"),
    @XmlEnumValue("Mobile")
    MOBILE("Mobile"),
    SMS("SMS"),
    @XmlEnumValue("Fax")
    FAX("Fax"),
    @XmlEnumValue("Email")
    EMAIL("Email"),
    URL("URL");
    private final String value;

    ChannelTypeChoiceCodelist(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ChannelTypeChoiceCodelist fromValue(String v) {
        for (ChannelTypeChoiceCodelist c: ChannelTypeChoiceCodelist.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
