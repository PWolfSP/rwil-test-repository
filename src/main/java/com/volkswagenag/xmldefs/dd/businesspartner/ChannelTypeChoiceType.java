
package com.volkswagenag.xmldefs.dd.businesspartner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import lombok.Generated;


/**
 * <p>Java-Klasse für ChannelTypeChoiceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ChannelTypeChoiceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChannelType" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}ChannelTypeChoiceCodelist"/>
 *         &lt;element name="Mail" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}MailType" minOccurs="0"/>
 *         &lt;element name="Phone" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PhoneType" minOccurs="0"/>
 *         &lt;element name="Mobile" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PhoneType" minOccurs="0"/>
 *         &lt;element name="SMS" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PhoneType" minOccurs="0"/>
 *         &lt;element name="Fax" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}PhoneType" minOccurs="0"/>
 *         &lt;element name="Email" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="URL" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChannelTypeChoiceType", propOrder = {
    "channelType",
    "mail",
    "phone",
    "mobile",
    "sms",
    "fax",
    "email",
    "url"
})
public class ChannelTypeChoiceType {

    @XmlElement(name = "ChannelType", required = true)
    @XmlSchemaType(name = "string")
    protected ChannelTypeChoiceCodelist channelType;
    @XmlElement(name = "Mail")
    protected MailType mail;
    @XmlElement(name = "Phone")
    protected PhoneType phone;
    @XmlElement(name = "Mobile")
    protected PhoneType mobile;
    @XmlElement(name = "SMS")
    protected PhoneType sms;
    @XmlElement(name = "Fax")
    protected PhoneType fax;
    @XmlElement(name = "Email")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String email;
    @XmlElement(name = "URL")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String url;

    /**
     * Ruft den Wert der channelType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ChannelTypeChoiceCodelist }
     *     
     */
    public ChannelTypeChoiceCodelist getChannelType() {
        return channelType;
    }

    /**
     * Legt den Wert der channelType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelTypeChoiceCodelist }
     *     
     */
    public void setChannelType(ChannelTypeChoiceCodelist value) {
        this.channelType = value;
    }

    /**
     * Ruft den Wert der mail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MailType }
     *     
     */
    public MailType getMail() {
        return mail;
    }

    /**
     * Legt den Wert der mail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MailType }
     *     
     */
    public void setMail(MailType value) {
        this.mail = value;
    }

    /**
     * Ruft den Wert der phone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PhoneType }
     *     
     */
    public PhoneType getPhone() {
        return phone;
    }

    /**
     * Legt den Wert der phone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PhoneType }
     *     
     */
    public void setPhone(PhoneType value) {
        this.phone = value;
    }

    /**
     * Ruft den Wert der mobile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PhoneType }
     *     
     */
    public PhoneType getMobile() {
        return mobile;
    }

    /**
     * Legt den Wert der mobile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PhoneType }
     *     
     */
    public void setMobile(PhoneType value) {
        this.mobile = value;
    }

    /**
     * Ruft den Wert der sms-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PhoneType }
     *     
     */
    public PhoneType getSMS() {
        return sms;
    }

    /**
     * Legt den Wert der sms-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PhoneType }
     *     
     */
    public void setSMS(PhoneType value) {
        this.sms = value;
    }

    /**
     * Ruft den Wert der fax-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PhoneType }
     *     
     */
    public PhoneType getFax() {
        return fax;
    }

    /**
     * Legt den Wert der fax-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PhoneType }
     *     
     */
    public void setFax(PhoneType value) {
        this.fax = value;
    }

    /**
     * Ruft den Wert der email-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Legt den Wert der email-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Ruft den Wert der url-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getURL() {
        return url;
    }

    /**
     * Legt den Wert der url-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setURL(String value) {
        this.url = value;
    }

}
