
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.volkswagenag.xmldefs.dd.basictypes.CodeType;

import lombok.Generated;


/**
 * <p>Java-Klasse für DealerClassificationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DealerClassificationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RegistrationRegion" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *         &lt;element name="MarketArea" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}CodeType" minOccurs="0"/>
 *         &lt;element name="SalesRegion" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *         &lt;element name="SalesArea" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}NumericType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DealerClassificationType", propOrder = {
    "registrationRegion",
    "marketArea",
    "salesRegion",
    "salesArea"
})
public class DealerClassificationType {

    @XmlElement(name = "RegistrationRegion")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String registrationRegion;
    @XmlElement(name = "MarketArea")
    protected CodeType marketArea;
    @XmlElement(name = "SalesRegion")
    protected BigDecimal salesRegion;
    @XmlElement(name = "SalesArea")
    protected BigDecimal salesArea;

    /**
     * Ruft den Wert der registrationRegion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistrationRegion() {
        return registrationRegion;
    }

    /**
     * Legt den Wert der registrationRegion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistrationRegion(String value) {
        this.registrationRegion = value;
    }

    /**
     * Ruft den Wert der marketArea-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getMarketArea() {
        return marketArea;
    }

    /**
     * Legt den Wert der marketArea-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setMarketArea(CodeType value) {
        this.marketArea = value;
    }

    /**
     * Ruft den Wert der salesRegion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSalesRegion() {
        return salesRegion;
    }

    /**
     * Legt den Wert der salesRegion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSalesRegion(BigDecimal value) {
        this.salesRegion = value;
    }

    /**
     * Ruft den Wert der salesArea-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSalesArea() {
        return salesArea;
    }

    /**
     * Legt den Wert der salesArea-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSalesArea(BigDecimal value) {
        this.salesArea = value;
    }

}
