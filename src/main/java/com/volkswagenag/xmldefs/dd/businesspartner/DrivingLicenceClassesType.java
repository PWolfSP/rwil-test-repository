
package com.volkswagenag.xmldefs.dd.businesspartner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Generated;


/**
 * List of driving license classes.
 * 
 * <p>Java-Klasse für DrivingLicenceClassesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DrivingLicenceClassesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Class" type="{http://xmldefs.volkswagenag.com/DD/BusinessPartner}DrivingLicenceClassType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DrivingLicenceClassesType", propOrder = {
    "clazz"
})



public class DrivingLicenceClassesType {
	
	
    @XmlElement(name = "Class")
    private List<DrivingLicenceClassType> clazz;


		public void setDrivingLicenceClassType(List<DrivingLicenceClassType> clazz) {
			this.clazz = clazz;
		}
		
	    public List<DrivingLicenceClassType> getClazz() {
	        if (clazz == null) {
	            clazz = new ArrayList<DrivingLicenceClassType>();
	        }
	        return this.clazz;
	    }

}
