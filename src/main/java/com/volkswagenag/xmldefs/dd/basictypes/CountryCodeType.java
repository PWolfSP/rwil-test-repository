
package com.volkswagenag.xmldefs.dd.basictypes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import lombok.Generated;


/**
 * 
 *             character string identifies a country (e.g. DE, US). The encoding ISO3166-1-2alpha
 *           is preferred, ISO3166-1-3alpha is supported as well. The code list is identified by a 'listID'. The default is
 *           'ISO3166-1-2alpha'. Here, the use of 'listID' is optional. However, when using ISO3166-1-3alpha codes, the 'listID'
 *           attribute _must_ be used
 *             
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ann:CodeList xmlns:ann="http://xmldefs.volkswagenag.com/Technical/Annotation/V1" xmlns:com="http://xmldefs.volkswagenag.com/DD/Commons" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://xmldefs.volkswagenag.com/DD/BasicTypes" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;ISO3166-1-2alpha&lt;/ann:CodeList&gt;
 * </pre>
 * 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ann:CodeList xmlns:ann="http://xmldefs.volkswagenag.com/Technical/Annotation/V1" xmlns:com="http://xmldefs.volkswagenag.com/DD/Commons" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://xmldefs.volkswagenag.com/DD/BasicTypes" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;ISO3166-1-3alpha&lt;/ann:CodeList&gt;
 * </pre>
 * 
 * 
 * <p>Java-Klasse für CountryCodeType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CountryCodeType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://xmldefs.volkswagenag.com/DD/BasicTypes>String">
 *       &lt;attribute name="listID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" />
 *       &lt;attribute name="listVersionID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" />
 *       &lt;attribute name="name" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" />
 *       &lt;attribute name="languageID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}LanguageTagCodeType" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CountryCodeType", propOrder = {
    "value"
})
public class CountryCodeType {

    @XmlValue
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String value;
    @XmlAttribute(name = "listID")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String listID;
    @XmlAttribute(name = "listVersionID")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String listVersionID;
    @XmlAttribute(name = "name")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String name;
    @XmlAttribute(name = "languageID")
    protected String languageID;

    /**
     * Ruft den Wert der value-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Legt den Wert der value-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Ruft den Wert der listID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListID() {
        return listID;
    }

    /**
     * Legt den Wert der listID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListID(String value) {
        this.listID = value;
    }

    /**
     * Ruft den Wert der listVersionID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListVersionID() {
        return listVersionID;
    }

    /**
     * Legt den Wert der listVersionID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListVersionID(String value) {
        this.listVersionID = value;
    }

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der languageID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguageID() {
        return languageID;
    }

    /**
     * Legt den Wert der languageID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguageID(String value) {
        this.languageID = value;
    }

}
