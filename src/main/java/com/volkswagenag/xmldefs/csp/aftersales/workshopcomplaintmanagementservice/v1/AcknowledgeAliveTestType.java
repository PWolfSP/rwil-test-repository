
package com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.volkswagenag.xmldefs.dd.commons.AliveTestAcknowledgementType;

import lombok.Generated;


/**
 * <p>Java-Klasse für AcknowledgeAliveTestType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AcknowledgeAliveTestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmldefs.volkswagenag.com/DD/Commons}AliveTestAcknowledgement"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcknowledgeAliveTestType", propOrder = {
    "aliveTestAcknowledgement"
})
public class AcknowledgeAliveTestType {

    @XmlElement(name = "AliveTestAcknowledgement", namespace = "http://xmldefs.volkswagenag.com/DD/Commons", required = true)
    protected AliveTestAcknowledgementType aliveTestAcknowledgement;

    /**
     * Ruft den Wert der aliveTestAcknowledgement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AliveTestAcknowledgementType }
     *     
     */
    public AliveTestAcknowledgementType getAliveTestAcknowledgement() {
        return aliveTestAcknowledgement;
    }

    /**
     * Legt den Wert der aliveTestAcknowledgement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AliveTestAcknowledgementType }
     *     
     */
    public void setAliveTestAcknowledgement(AliveTestAcknowledgementType value) {
        this.aliveTestAcknowledgement = value;
    }

}
