
package com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import lombok.Generated;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@Generated
@XmlRegistry
public class ObjectFactory {

    private final static QName _ProcessAliveTest_QNAME = new QName("http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1", "ProcessAliveTest");
    private final static QName _ProcessWorkshopComplaint_QNAME = new QName("http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1", "ProcessWorkshopComplaint");
    private final static QName _GetWorkshopComplaints_QNAME = new QName("http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1", "GetWorkshopComplaints");
    private final static QName _GetWorkshopComplaintsInfo_QNAME = new QName("http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1", "GetWorkshopComplaintsInfo");
    private final static QName _ShowWorkshopComplaints_QNAME = new QName("http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1", "ShowWorkshopComplaints");
    private final static QName _ShowWorkshopComplaintsInfo_QNAME = new QName("http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1", "ShowWorkshopComplaintsInfo");
    private final static QName _AcknowledgeAliveTest_QNAME = new QName("http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1", "AcknowledgeAliveTest");
    private final static QName _AcknowledgeWorkshopComplaint_QNAME = new QName("http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1", "AcknowledgeWorkshopComplaint");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProcessAliveTestType }
     * 
     */
    public ProcessAliveTestType createProcessAliveTestType() {
        return new ProcessAliveTestType();
    }

    /**
     * Create an instance of {@link ProcessWorkshopComplaintType }
     * 
     */
    public ProcessWorkshopComplaintType createProcessWorkshopComplaintType() {
        return new ProcessWorkshopComplaintType();
    }

    /**
     * Create an instance of {@link AcknowledgeWorkshopComplaintType }
     * 
     */
    public AcknowledgeWorkshopComplaintType createAcknowledgeWorkshopComplaintType() {
        return new AcknowledgeWorkshopComplaintType();
    }

    /**
     * Create an instance of {@link ShowWorkshopComplaintsType }
     * 
     */
    public ShowWorkshopComplaintsType createShowWorkshopComplaintsType() {
        return new ShowWorkshopComplaintsType();
    }

    /**
     * Create an instance of {@link GetWorkshopComplaintsType }
     * 
     */
    public GetWorkshopComplaintsType createGetWorkshopComplaintsType() {
        return new GetWorkshopComplaintsType();
    }

    /**
     * Create an instance of {@link AcknowledgeAliveTestType }
     * 
     */
    public AcknowledgeAliveTestType createAcknowledgeAliveTestType() {
        return new AcknowledgeAliveTestType();
    }

    /**
     * Create an instance of {@link EFACodesType }
     * 
     */
    public EFACodesType createEFACodesType() {
        return new EFACodesType();
    }

    /**
     * Create an instance of {@link VehiclesType }
     * 
     */
    public VehiclesType createVehiclesType() {
        return new VehiclesType();
    }

    /**
     * Create an instance of {@link WorkshopComplaintsType }
     * 
     */
    public WorkshopComplaintsType createWorkshopComplaintsType() {
        return new WorkshopComplaintsType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessAliveTestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1", name = "ProcessAliveTest")
    public JAXBElement<ProcessAliveTestType> createProcessAliveTest(ProcessAliveTestType value) {
        return new JAXBElement<ProcessAliveTestType>(_ProcessAliveTest_QNAME, ProcessAliveTestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessWorkshopComplaintType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1", name = "ProcessWorkshopComplaint")
    public JAXBElement<ProcessWorkshopComplaintType> createProcessWorkshopComplaint(ProcessWorkshopComplaintType value) {
        return new JAXBElement<ProcessWorkshopComplaintType>(_ProcessWorkshopComplaint_QNAME, ProcessWorkshopComplaintType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWorkshopComplaintsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1", name = "GetWorkshopComplaints")
    public JAXBElement<GetWorkshopComplaintsType> createGetWorkshopComplaints(GetWorkshopComplaintsType value) {
        return new JAXBElement<GetWorkshopComplaintsType>(_GetWorkshopComplaints_QNAME, GetWorkshopComplaintsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWorkshopComplaintsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1", name = "GetWorkshopComplaintsInfo")
    public JAXBElement<GetWorkshopComplaintsType> createGetWorkshopComplaintsInfo(GetWorkshopComplaintsType value) {
        return new JAXBElement<GetWorkshopComplaintsType>(_GetWorkshopComplaintsInfo_QNAME, GetWorkshopComplaintsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShowWorkshopComplaintsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1", name = "ShowWorkshopComplaints")
    public JAXBElement<ShowWorkshopComplaintsType> createShowWorkshopComplaints(ShowWorkshopComplaintsType value) {
        return new JAXBElement<ShowWorkshopComplaintsType>(_ShowWorkshopComplaints_QNAME, ShowWorkshopComplaintsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShowWorkshopComplaintsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1", name = "ShowWorkshopComplaintsInfo")
    public JAXBElement<ShowWorkshopComplaintsType> createShowWorkshopComplaintsInfo(ShowWorkshopComplaintsType value) {
        return new JAXBElement<ShowWorkshopComplaintsType>(_ShowWorkshopComplaintsInfo_QNAME, ShowWorkshopComplaintsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcknowledgeAliveTestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1", name = "AcknowledgeAliveTest")
    public JAXBElement<AcknowledgeAliveTestType> createAcknowledgeAliveTest(AcknowledgeAliveTestType value) {
        return new JAXBElement<AcknowledgeAliveTestType>(_AcknowledgeAliveTest_QNAME, AcknowledgeAliveTestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcknowledgeWorkshopComplaintType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1", name = "AcknowledgeWorkshopComplaint")
    public JAXBElement<AcknowledgeWorkshopComplaintType> createAcknowledgeWorkshopComplaint(AcknowledgeWorkshopComplaintType value) {
        return new JAXBElement<AcknowledgeWorkshopComplaintType>(_AcknowledgeWorkshopComplaint_QNAME, AcknowledgeWorkshopComplaintType.class, null, value);
    }

}
