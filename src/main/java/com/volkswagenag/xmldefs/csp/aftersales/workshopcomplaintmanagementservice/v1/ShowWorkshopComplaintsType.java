
package com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import lombok.Generated;


/**
 * <p>Java-Klasse für ShowWorkshopComplaintsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ShowWorkshopComplaintsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WorkshopComplaints" type="{http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1}WorkshopComplaintsType" minOccurs="0"/>
 *         &lt;element name="EFACodes" type="{http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1}EFACodesType" minOccurs="0"/>
 *         &lt;element name="Vehicles" type="{http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1}VehiclesType" minOccurs="0"/>
 *         &lt;element name="ChunkID" type="{http://xmldefs.volkswagenag.com/DD/BasicTypes}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@Generated
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShowWorkshopComplaintsType", propOrder = {
    "workshopComplaints",
    "efaCodes",
    "vehicles",
    "chunkID"
})
public class ShowWorkshopComplaintsType {

    @XmlElement(name = "WorkshopComplaints")
    protected WorkshopComplaintsType workshopComplaints;
    @XmlElement(name = "EFACodes")
    protected EFACodesType efaCodes;
    @XmlElement(name = "Vehicles")
    protected VehiclesType vehicles;
    @XmlElement(name = "ChunkID")
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String chunkID;

    /**
     * Ruft den Wert der workshopComplaints-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WorkshopComplaintsType }
     *     
     */
    public WorkshopComplaintsType getWorkshopComplaints() {
        return workshopComplaints;
    }

    /**
     * Legt den Wert der workshopComplaints-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkshopComplaintsType }
     *     
     */
    public void setWorkshopComplaints(WorkshopComplaintsType value) {
        this.workshopComplaints = value;
    }

    /**
     * Ruft den Wert der efaCodes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EFACodesType }
     *     
     */
    public EFACodesType getEFACodes() {
        return efaCodes;
    }

    /**
     * Legt den Wert der efaCodes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EFACodesType }
     *     
     */
    public void setEFACodes(EFACodesType value) {
        this.efaCodes = value;
    }

    /**
     * Ruft den Wert der vehicles-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VehiclesType }
     *     
     */
    public VehiclesType getVehicles() {
        return vehicles;
    }

    /**
     * Legt den Wert der vehicles-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VehiclesType }
     *     
     */
    public void setVehicles(VehiclesType value) {
        this.vehicles = value;
    }

    /**
     * Ruft den Wert der chunkID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChunkID() {
        return chunkID;
    }

    /**
     * Legt den Wert der chunkID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChunkID(String value) {
        this.chunkID = value;
    }

}
