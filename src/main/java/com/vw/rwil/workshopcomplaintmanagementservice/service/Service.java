package com.vw.rwil.workshopcomplaintmanagementservice.service;
import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.*;

import org.springframework.http.ResponseEntity;

/**
 * @author rwil.support.vwag.r.wob@volkswagen.de
 * @company Volkswagen AG
 */
public interface Service {

	public ResponseEntity<ShowWorkshopComplaintsType> getWorkshopComplaints (GetWorkshopComplaintsType getWorkshopComplaintsType, String partnerkey, String system) throws Exception; 
	public ResponseEntity<ShowWorkshopComplaintsType> getWorkshopComplaintsInfo (GetWorkshopComplaintsType getWorkshopComplaintsType, String partnerkey, String system) throws Exception; 
	public ResponseEntity<AcknowledgeWorkshopComplaintType> processWorkshopComplaint (ProcessWorkshopComplaintType processWorkshopComplaintType, String partnerkey, String system) throws Exception; 
    public ResponseEntity<AcknowledgeAliveTestType> processAliveTest (String partnerkey, String system) throws Exception;
}