package com.vw.rwil.workshopcomplaintmanagementservice.service.soap;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import lombok.Generated;
import lombok.extern.slf4j.Slf4j;

/**
 * @author rwil.support.vwag.r.wob@volkswagen.de
 * @company Volkswagen AG
 */
@Slf4j
@Generated
public class SOAPHeaderHandler implements SOAPHandler<SOAPMessageContext> {

	public interface MessageCallback {
        public void getCallback(String uuid, String requestUUID);
    }

	private String country;
	private String brand;
	private String stage;
	private String system;
	private String method;
	private MessageCallback callback;

	public SOAPHeaderHandler(String country, String brand, String stage, String system, String method, MessageCallback getUUID) {
		this.country = country;
		this.brand = brand;
		// TODO: quick fix for mapping issue from rwil to ia stage values
		this.stage = stage.equals("PROD") ? "Production" : "QA";
		this.system = system;
		this.method = method;
		callback = getUUID;

	}

	public boolean handleMessage(SOAPMessageContext context) {
		Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		if (outboundProperty.booleanValue()) {
			try {
				SOAPEnvelope envelope = context.getMessage().getSOAPPart().getEnvelope();

				SOAPHeader header = envelope.getHeader();

				// remove the headers because they got the wrong namespaces
				header.removeContents();
				
				envelope.addNamespaceDeclaration("vwa", "http://xmldefs.volkswagenag.com/Technical/Addressing/V1");
				envelope.addNamespaceDeclaration("wsa", "http://www.w3.org/2005/08/addressing");
				envelope.addNamespaceDeclaration("wsse",
						"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
				envelope.addNamespaceDeclaration("vwsu", "vwsu");

				QName action = new QName("http://www.w3.org/2005/08/addressing", "Action", "wsa");
				QName country = new QName("http://xmldefs.volkswagenag.com/Technical/Addressing/V1", "Country", "vwa");
				QName brand = new QName("http://xmldefs.volkswagenag.com/Technical/Addressing/V1", "Brand", "vwa");
				QName stage = new QName("http://xmldefs.volkswagenag.com/Technical/Addressing/V1", "Stage", "vwa");
				QName system = new QName("http://xmldefs.volkswagenag.com/Technical/Addressing/V1", "System", "vwa");
				QName messageId = new QName("http://www.w3.org/2005/08/addressing", "MessageID", "wsa");
				QName to = new QName("http://www.w3.org/2005/08/addressing", "To", "wsa");

				header.addHeaderElement(country).setTextContent(this.country);
				header.addHeaderElement(brand).setTextContent(this.brand);
				header.addHeaderElement(stage).setTextContent(this.stage);
				header.addHeaderElement(system).setTextContent(this.system);

				header.addHeaderElement(action).setTextContent("http://xmldefs.volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1/WorkshopComplaintManagementServicePortType/" + this.method);

				UUID uuid = UUID.randomUUID();
                header.addHeaderElement(messageId).setTextContent("uuid:" + uuid.toString());
				header.addHeaderElement(to).setTextContent("ws://volkswagenag.com/CSP/AfterSales/WorkshopComplaintManagementService/V1");
				
				context.getMessage().saveChanges();

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		else {
			// inbound
			
		}
		return true;

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Set<QName> getHeaders() {
		return new TreeSet();
	}

	public boolean handleFault(SOAPMessageContext context) {
        Object header = context.get("com.sun.xml.internal.ws.api.addressing.messageId");
        Object requestID = context.get("com.sun.xml.internal.ws.addressing.WsaPropertyBag.MessageIdFromRequest");
        
        if(header != null) {
        	System.out.println(header.toString());
        } else {
        	header = "http://www.w3.org/2005/08/addressing";
        }
        if(requestID != null) {
        	System.out.println(requestID.toString());
        }
        
        String relatesTo = getRelatesTo(context);
        System.out.println(relatesTo);
        callback.getCallback((String) header, relatesTo);
        return false;
    }

    private String getRelatesTo(SOAPMessageContext smc) {
        int offset = 15;
        SOAPMessage message = smc.getMessage();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        String relatesToID = "";
        try {
            message.writeTo(byteArrayOutputStream);
            String logString = byteArrayOutputStream.toString("UTF-8");

            if (logString.contains("<wsa:RelatesTo>")) {
                int startIDX = logString.indexOf("<wsa:RelatesTo>");
                int endIDX = logString.indexOf("</wsa:RelatesTo>");
                relatesToID = logString.substring(startIDX + offset, endIDX);
            }

        } catch (SOAPException | IOException e) {
            log.error(e.getMessage());
        }

        return relatesToID;
    }

	public void close(MessageContext context) {
		//
	}
}