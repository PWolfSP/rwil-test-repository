package com.vw.rwil.workshopcomplaintmanagementservice.service;

import com.vw.rwil.rwilutils.common.RWILUtil;
import com.vw.rwil.rwilutils.patterns.ia.utils.WSDLUtil;

import com.vw.rwil.rwilutils.patterns.ia.common.IABaseService;
import com.vw.rwil.workshopcomplaintmanagementservice.service.soap.SOAPHeaderHandler;

import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.*;
import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.VehiclesType;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.Holder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

/**
 * @author rwil.support.vwag.r.wob@volkswagen.de
 * @company Volkswagen AG
 */
@Slf4j
@org.springframework.stereotype.Service
public class ServiceImpl extends IABaseService implements Service{
	
	@Autowired
	private WorkshopComplaintManagementServicePortType port;
	
	private BindingProvider bindingProvider;
	private BindingProvider getBindingProvider() throws GeneralSecurityException, IOException {
		if(bindingProvider == null) {
			SSLContext sslcontext = SSLContext.getInstance("TLS");

			X509TrustManager trustmanager = trustManagerForCertificates();
			KeyManagerFactory keyManagers = createKeyStoreManager();

			if (isTruststoreEnabled) {
			log.info("*** Setting SSL context with Trust manager");
			sslcontext.init(keyManagers.getKeyManagers(), new TrustManager[] { trustmanager }, null);
			} else {
			log.info("*** Setting SSL context without Trust manager");
			sslcontext.init(keyManagers.getKeyManagers(), null, null);
		    }
		    
		    log.info("*** Binding the context");
			bindingProvider = (BindingProvider) port;
			bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, soapEndpoint);
			bindingProvider.getRequestContext().put("com.sun.xml.internal.ws.transport.https.client.SSLSocketFactory", sslcontext.getSocketFactory());
			
			log.info("*** Done Init");
		}
		
		return bindingProvider;
	}
	
	public ServiceImpl() throws IOException, GeneralSecurityException {
		super();
    	
	}
	      
    
    @SuppressWarnings("rawtypes")
	@Override
	public ResponseEntity<ShowWorkshopComplaintsType> getWorkshopComplaints (GetWorkshopComplaintsType inputType, String partnerkey, String system) throws Exception { 
		List<Handler> handlerChain = getBindingProvider().getBinding().getHandlerChain();
		handlerChain.clear();
		handlerChain.add(new SOAPHeaderHandler(WSDLUtil.getCountry(partnerkey), WSDLUtil.getBrand(partnerkey), RWILUtil.getStage(), system, "GetWorkshopComplaints", (p, r) -> {
					messageID = p;
					requestMessageID = r;
				}));	
		handlerChain.add(soapLoggingHandler);
		getBindingProvider().getBinding().setHandlerChain(handlerChain);
 		ShowWorkshopComplaintsType responseType = new ShowWorkshopComplaintsType();

		Holder<WorkshopComplaintsType> workshopComplaints = new Holder<WorkshopComplaintsType>();

		Holder<EFACodesType> eFACodes = new Holder<EFACodesType>();

		Holder<VehiclesType> vehicles = new Holder<VehiclesType>();

		Holder<String> chunkID = new Holder<String>(inputType.getChunkID());
		logRequestData(WSDLUtil.getCountry(partnerkey), WSDLUtil.getBrand(partnerkey), system);
		try {
			port.getWorkshopComplaints(inputType.getWorkshopComplaint(), inputType.getLanguageID(), chunkID, workshopComplaints, eFACodes, vehicles);
			responseType.setWorkshopComplaints(workshopComplaints.value);
			responseType.setEFACodes(eFACodes.value);
			responseType.setVehicles(vehicles.value);
			responseType.setChunkID(chunkID.value);
		} catch (FaultMessage fault) {
			processFault(fault);
		}
		return new ResponseEntity<ShowWorkshopComplaintsType>(responseType, HttpStatus.CREATED);
	}
   
    
    @SuppressWarnings("rawtypes")
	@Override
	public ResponseEntity<ShowWorkshopComplaintsType> getWorkshopComplaintsInfo (GetWorkshopComplaintsType inputType, String partnerkey, String system) throws Exception { 
		List<Handler> handlerChain = getBindingProvider().getBinding().getHandlerChain();
		handlerChain.clear();
		handlerChain.add(new SOAPHeaderHandler(WSDLUtil.getCountry(partnerkey), WSDLUtil.getBrand(partnerkey), RWILUtil.getStage(), system, "GetWorkshopComplaintsInfo", (p, r) -> {
					messageID = p;
					requestMessageID = r;
				}));	
		handlerChain.add(soapLoggingHandler);
		getBindingProvider().getBinding().setHandlerChain(handlerChain);
 		ShowWorkshopComplaintsType responseType = new ShowWorkshopComplaintsType();

		Holder<WorkshopComplaintsType> workshopComplaints = new Holder<WorkshopComplaintsType>();

		Holder<EFACodesType> eFACodes = new Holder<EFACodesType>();

		Holder<VehiclesType> vehicles = new Holder<VehiclesType>();

		Holder<String> chunkID = new Holder<String>(inputType.getChunkID());
		logRequestData(WSDLUtil.getCountry(partnerkey), WSDLUtil.getBrand(partnerkey), system);
		try {
		port.getWorkshopComplaintsInfo(inputType.getWorkshopComplaint(), inputType.getLanguageID(), chunkID, workshopComplaints, eFACodes, vehicles);
		responseType.setWorkshopComplaints(workshopComplaints.value);
		responseType.setEFACodes(eFACodes.value);
		responseType.setVehicles(vehicles.value);
		responseType.setChunkID(chunkID.value);
		} catch (FaultMessage fault) {
			processFault(fault);
		}
		return new ResponseEntity<ShowWorkshopComplaintsType>(responseType, HttpStatus.CREATED);
	}
   
    
    @SuppressWarnings("rawtypes")
	@Override
	public ResponseEntity<AcknowledgeWorkshopComplaintType> processWorkshopComplaint (ProcessWorkshopComplaintType inputType, String partnerkey, String system) throws Exception { 
		List<Handler> handlerChain = getBindingProvider().getBinding().getHandlerChain();
		handlerChain.clear();
		handlerChain.add(new SOAPHeaderHandler(WSDLUtil.getCountry(partnerkey), WSDLUtil.getBrand(partnerkey), RWILUtil.getStage(), system, "ProcessWorkshopComplaint", (p, r) -> {
					messageID = p;
					requestMessageID = r;
				}));	
		handlerChain.add(soapLoggingHandler);
		getBindingProvider().getBinding().setHandlerChain(handlerChain);
 		AcknowledgeWorkshopComplaintType responseType = new AcknowledgeWorkshopComplaintType();
			logRequestData(WSDLUtil.getCountry(partnerkey), WSDLUtil.getBrand(partnerkey), system);
		try {
 		responseType.setWorkshopComplaintRef(port.processWorkshopComplaint(inputType.getDealerRef(), inputType.getWorkshopComplaint()));
		} catch (FaultMessage fault) {
			processFault(fault);
		}
		return new ResponseEntity<AcknowledgeWorkshopComplaintType>(responseType, HttpStatus.CREATED);
	}
   
    
    @SuppressWarnings("rawtypes")
	@Override
    public ResponseEntity<AcknowledgeAliveTestType> processAliveTest (String partnerkey, String system) throws Exception { 
		List<Handler> handlerChain = getBindingProvider().getBinding().getHandlerChain();
		handlerChain.clear();
		handlerChain.add(new SOAPHeaderHandler(WSDLUtil.getCountry(partnerkey), WSDLUtil.getBrand(partnerkey), RWILUtil.getStage(), system, "ProcessAliveTest", (p, r) -> {
					messageID = p;
					requestMessageID = r;
				}));	
		handlerChain.add(soapLoggingHandler);
		getBindingProvider().getBinding().setHandlerChain(handlerChain);
 		AcknowledgeAliveTestType responseType = new AcknowledgeAliveTestType();
			logRequestData(WSDLUtil.getCountry(partnerkey), WSDLUtil.getBrand(partnerkey), system);
		try {
 		responseType.setAliveTestAcknowledgement(port.processAliveTest());
		} catch (FaultMessage fault) {
			processFault(fault);
		}
		return new ResponseEntity<AcknowledgeAliveTestType>(responseType, HttpStatus.OK);    	
	}
   
    
    /**
     * TODO: If the entity is named FaultMessage instead of Fault please adjust this method and its occurencies.
     * 
	 * @param fault
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	protected void processFault(FaultMessage fault) throws JsonParseException, JsonMappingException, IOException {
		String jsonString = new Gson().toJson(fault);
		Gson gson = new Gson();
		com.vw.rwil.rwilutils.patterns.ia.model.Fault jsonFault = gson.fromJson(jsonString, com.vw.rwil.rwilutils.patterns.ia.model.Fault.class);
		WSDLUtil.processFault(jsonFault);
	}
}