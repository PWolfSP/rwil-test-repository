package com.vw.rwil.workshopcomplaintmanagementservice;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.vw.rwil.rwilutils.RWILSpringApplication;

import lombok.Generated;

/**
 * @title WorkshopComplaintManagementService
 * @author rwil.support.vwag.r.wob@volkswagen.de
 * @company Volkswagen AG
 */
@Generated
@SpringBootApplication
public class Application{

	public static void main(String[] args) {
		RWILSpringApplication.run();
	}
}