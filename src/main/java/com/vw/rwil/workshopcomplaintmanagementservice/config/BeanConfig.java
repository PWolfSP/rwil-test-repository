package com.vw.rwil.workshopcomplaintmanagementservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.vw.rwil.rwilutils.patterns.ia.handler.exception.IAExceptionResponseHandler;

import lombok.Generated;

@Generated
@Configuration
public class BeanConfig {

	@Bean
	@Primary
	public IAExceptionResponseHandler createIAExceptionResponseHandler() {
		
		return new IAExceptionResponseHandler();
	}
}