package com.vw.rwil.workshopcomplaintmanagementservice.config;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.ws.soap.AddressingFeature;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.WorkshopComplaintManagementService;
import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.WorkshopComplaintManagementServicePortType;

import lombok.Generated;

@Generated
@Configuration
public class ServicePortTypeConfig {

	@Bean
	@ConditionalOnMissingBean(value = WorkshopComplaintManagementServicePortType.class)
	public WorkshopComplaintManagementServicePortType defaultServicePortType() throws MalformedURLException {
		URL wsdlUrl = new URL("classpath:wsdl/WorkshopComplaintManagementService_V1.wsdl");
		WorkshopComplaintManagementService service = new WorkshopComplaintManagementService(wsdlUrl);
		return  service.getWorkshopComplaintManagementServicePort(new AddressingFeature(true, true));
	}
}
