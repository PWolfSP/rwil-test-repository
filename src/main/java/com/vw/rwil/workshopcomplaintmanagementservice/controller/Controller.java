package com.vw.rwil.workshopcomplaintmanagementservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.AcknowledgeAliveTestType;
import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.AcknowledgeWorkshopComplaintType;
import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.GetWorkshopComplaintsType;
import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.ProcessWorkshopComplaintType;
import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.ShowWorkshopComplaintsType;
import com.vw.rwil.workshopcomplaintmanagementservice.service.Service;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


@RestController
@Api(value="workshopcomplaintmanagementservice",  description="Microservice generated with WSDLGenerator to contact the IA-Services.")
public class Controller {

	@Autowired
	private Service service;
	
    @ApiOperation(value = "The operation GetWorkshopComplaints delivers the WorkshopComplaint data. The input WorkshopComplaint      object is used as a filter")
    	@RequestMapping(value = {"{partnerkey}/getWorkshopComplaints"}, 
    							method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseStatus(code = HttpStatus.CREATED)
	public ResponseEntity<ShowWorkshopComplaintsType> getWorkshopComplaints (
		 @ApiParam(required=true, value="Example of the request body")
		 @RequestBody GetWorkshopComplaintsType getWorkshopComplaintsType, 
		 @ApiParam(required=true, example="DEU74623V", value="The partnerKey is build up from three elements: Country, PartnerNumber and Brand. Country: The country the dealer works in. Using ISO3166-1- 3alpha codes. Example: DEU. The Partner number is a 5-digit alphanumeric string given by the importer and only valid in the context of a brand and country. The PartnerNumber is stored in the Group DealerPartner System KVPS as Partner Number. Example: 74623. Brands inside Volkswagen (1-digit). Example: V")
		 @PathVariable String partnerkey
		 // TODO: Remove this comments when the IA service backend did not use the parameter 'system'
		 // @ApiParam(required=false, example="MCC", value="System matching the stage  (example: MCC)") 
		 // @PathVariable Optional<String> system
		 ) throws Exception{
		 // TODO: Remove this comments when the IA service backend did not use the parameter 'system'
		 // return service.getWorkshopComplaints(getWorkshopComplaintsType, partnerkey, system.isPresent() ? system.get() : null); 
		 return service.getWorkshopComplaints(getWorkshopComplaintsType, partnerkey, null); 
	}
    @ApiOperation(value = "The operation GetWorkshopComplaintsInfo delivers the basic information about WorkshopComplaint. The      input WorkshopComplaint object is used as a filter")
    	@RequestMapping(value = {"{partnerkey}/getWorkshopComplaintsInfo"}, 
    							method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseStatus(code = HttpStatus.CREATED)
	public ResponseEntity<ShowWorkshopComplaintsType> getWorkshopComplaintsInfo (
		 @ApiParam(required=true, value="Example of the request body")
		 @RequestBody GetWorkshopComplaintsType getWorkshopComplaintsType, 
		 @ApiParam(required=true, example="DEU74623V", value="The partnerKey is build up from three elements: Country, PartnerNumber and Brand. Country: The country the dealer works in. Using ISO3166-1- 3alpha codes. Example: DEU. The Partner number is a 5-digit alphanumeric string given by the importer and only valid in the context of a brand and country. The PartnerNumber is stored in the Group DealerPartner System KVPS as Partner Number. Example: 74623. Brands inside Volkswagen (1-digit). Example: V")
		 @PathVariable String partnerkey
		 // TODO: Remove this comments when the IA service backend did not use the parameter 'system'
		 // @ApiParam(required=false, example="MCC", value="System matching the stage  (example: MCC)") 
		 // @PathVariable Optional<String> system
		 ) throws Exception{
		 // TODO: Remove this comments when the IA service backend did not use the parameter 'system'
		 // return service.getWorkshopComplaintsInfo(getWorkshopComplaintsType, partnerkey, system.isPresent() ? system.get() : null); 
		 return service.getWorkshopComplaintsInfo(getWorkshopComplaintsType, partnerkey, null); 
	}
    @ApiOperation(value = "The operation is used to initiate the processing of a workshop complaint. A new workshop complaint      will be created or an existing one will be modified as the result.")
    	@RequestMapping(value = {"{partnerkey}/processWorkshopComplaint"}, 
    							method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseStatus(code = HttpStatus.CREATED)
	public ResponseEntity<AcknowledgeWorkshopComplaintType> processWorkshopComplaint (
		 @ApiParam(required=true, value="Example of the request body")
		 @RequestBody ProcessWorkshopComplaintType processWorkshopComplaintType, 
		 @ApiParam(required=true, example="DEU74623V", value="The partnerKey is build up from three elements: Country, PartnerNumber and Brand. Country: The country the dealer works in. Using ISO3166-1- 3alpha codes. Example: DEU. The Partner number is a 5-digit alphanumeric string given by the importer and only valid in the context of a brand and country. The PartnerNumber is stored in the Group DealerPartner System KVPS as Partner Number. Example: 74623. Brands inside Volkswagen (1-digit). Example: V")
		 @PathVariable String partnerkey
		 // TODO: Remove this comments when the IA service backend did not use the parameter 'system'
		 // @ApiParam(required=false, example="MCC", value="System matching the stage  (example: MCC)") 
		 // @PathVariable Optional<String> system
		 ) throws Exception{
		 // TODO: Remove this comments when the IA service backend did not use the parameter 'system'
		 // return service.processWorkshopComplaint(processWorkshopComplaintType, partnerkey, system.isPresent() ? system.get() : null); 
		 return service.processWorkshopComplaint(processWorkshopComplaintType, partnerkey, null); 
	}
    @ApiOperation(value = "")
    	@RequestMapping(value = {"{partnerkey}/processAliveTest"}, 
    							method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AcknowledgeAliveTestType> processAliveTest ( 
		 @ApiParam(required=true, example="DEU74623V", value="The partnerKey is build up from three elements: Country, PartnerNumber and Brand. Country: The country the dealer works in. Using ISO3166-1- 3alpha codes. Example: DEU. The Partner number is a 5-digit alphanumeric string given by the importer and only valid in the context of a brand and country. The PartnerNumber is stored in the Group DealerPartner System KVPS as Partner Number. Example: 74623. Brands inside Volkswagen (1-digit). Example: V")
		 @PathVariable String partnerkey
		 // TODO: Remove this comments when the IA service backend did not use the parameter 'system'
  		 // @ApiParam(required=false, example="MCC", value="System matching the stage  (example: MCC)") 
		 // @PathVariable Optional<String> system
		 ) throws Exception{
		 // TODO: Remove this comments when the IA service backend did not use the parameter 'system'
		 // return service.processAliveTest(partnerkey, system.isPresent() ? system.get() : null); 
		 return service.processAliveTest(partnerkey, null); 
	}
}