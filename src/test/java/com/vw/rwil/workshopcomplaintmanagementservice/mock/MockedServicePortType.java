package com.vw.rwil.workshopcomplaintmanagementservice.mock;

import java.util.HashMap;
import java.util.Map;

import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.EndpointReference;
import javax.xml.ws.Holder;

import org.apache.commons.lang.NotImplementedException;

import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.*;
import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.VehiclesType;
import com.volkswagenag.xmldefs.dd.businesspartner.*;
import com.volkswagenag.xmldefs.dd.commons.*;
import com.volkswagenag.xmldefs.dd.workshopcomplaint.*;

/**
 * @author rwil.support.vwag.r.wob@volkswagen.de
 * @company Volkswagen AG
 */
public class MockedServicePortType implements WorkshopComplaintManagementServicePortType, BindingProvider {

	Map<String, Object> requestContext = new HashMap<String, Object>();
	Map<String, Object> responseContext = new HashMap<String, Object>();
	MockBinding binding = new MockBinding();
	
	//@Override
    public WorkshopComplaintIdentifierType processWorkshopComplaint (PartnerIdentifierType dealerRef, WorkshopComplaintType workshopComplaint) {
		throw new NotImplementedException();
	}
	
	//@Override
    public AliveTestAcknowledgementType processAliveTest () {
		return new AliveTestAcknowledgementType();
	}
   
	
	@Override
	public Map<String, Object> getRequestContext() {
		return requestContext;
	}

	@Override
	public Map<String, Object> getResponseContext() {
		return responseContext;
	}

	@Override
	public Binding getBinding() {
		return binding;
	}

	@Override
	public EndpointReference getEndpointReference() {
		return null;
	}

	@Override
	public <T extends EndpointReference> T getEndpointReference(Class<T> clazz) {
		return null;
	}



	@Override
	public void getWorkshopComplaints(WorkshopComplaintType workshopComplaint, String languageID,
			Holder<String> chunkID, Holder<WorkshopComplaintsType> workshopComplaints, Holder<EFACodesType> efaCodes,
			Holder<VehiclesType> vehicles) throws FaultMessage {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void getWorkshopComplaintsInfo(WorkshopComplaintType workshopComplaint, String languageID,
			Holder<String> chunkID, Holder<WorkshopComplaintsType> workshopComplaints, Holder<EFACodesType> efaCodes,
			Holder<VehiclesType> vehicles) throws FaultMessage {
		// TODO Auto-generated method stub
		
	}

}
