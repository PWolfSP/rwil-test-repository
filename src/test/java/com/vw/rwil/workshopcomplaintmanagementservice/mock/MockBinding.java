package com.vw.rwil.workshopcomplaintmanagementservice.mock;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.Binding;
import javax.xml.ws.handler.Handler;

/**
 * @author rwil.support.vwag.r.wob@volkswagen.de
 * @company Volkswagen AG
 */
public class MockBinding implements Binding {

	@SuppressWarnings("rawtypes")
	List<Handler> handlers = new ArrayList<Handler>();
	
	@SuppressWarnings("rawtypes")
	@Override
	public List<Handler> getHandlerChain() {
		// TODO Auto-generated method stub
		return handlers;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void setHandlerChain(List<Handler> chain) {
		handlers = chain;
		
	}

	@Override
	public String getBindingID() {
		// TODO Auto-generated method stub
		return "TestID";
	}

}
