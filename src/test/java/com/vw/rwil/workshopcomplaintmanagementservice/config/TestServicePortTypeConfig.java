package com.vw.rwil.workshopcomplaintmanagementservice.config;

import java.net.MalformedURLException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.WorkshopComplaintManagementServicePortType;
import com.vw.rwil.workshopcomplaintmanagementservice.mock.MockedServicePortType;

@Configuration
public class TestServicePortTypeConfig {

	@Bean
	public WorkshopComplaintManagementServicePortType mockedServicePortType() throws MalformedURLException {
		
		return new MockedServicePortType();
	}
}
