package com.vw.rwil.workshopcomplaintmanagementservice;

import static org.assertj.core.api.Assertions.assertThat;

import java.net.UnknownHostException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.ProcessWorkshopComplaintType;
import com.volkswagenag.xmldefs.csp.aftersales.workshopcomplaintmanagementservice.v1.ShowWorkshopComplaintsType;
import com.vw.rwil.rwilutils.initializer.DevStackInitializer;
import com.vw.rwil.workshopcomplaintmanagementservice.config.TestServicePortTypeConfig;

@RunWith(SpringRunner.class)
@TestPropertySource(locations="classpath:application.yml")
public class ServiceTest {
	
	private static int serverPort;
	private static RestTemplate restTemplate;
	
	@BeforeClass
	public static void before() {
		serverPort = 8080;
		restTemplate = new RestTemplate();

		SpringApplicationBuilder builder1 = new SpringApplicationBuilder(TestServicePortTypeConfig.class, DevStackInitializer.class, Application.class);
		builder1.run(new String[] { "" });
	}
	
	@Test
	public void processAliveTest_UT() throws Exception {
		
		//Test
		//ResponseEntity<AcknowledgeAliveTestType> result = restTemplate.getForEntity(getAbsoluteURLPath("/DEU00000V/processAliveTest"), AcknowledgeAliveTestType.class);
		ResponseEntity<Object> result = restTemplate.getForEntity(getAbsoluteURLPath("/DEU00000V/processAliveTest"), Object.class);
		
		//Check
		assertThat(result != null);
		assertThat(result.getStatusCodeValue() == 200);
		assertThat(result.getBody() != null);
		
	}
	
	@Test
	public void getWorkshopComplaints_UT() throws Exception {

		ShowWorkshopComplaintsType body = new ShowWorkshopComplaintsType();
		//Test
		ResponseEntity<Object> result = restTemplate.postForEntity(getAbsoluteURLPath("/DEU00000V/getWorkshopComplaints"), body, Object.class);
		
		//Check
		assertThat(result != null);
		assertThat(result.getStatusCodeValue() == 201);
		assertThat(result.getBody() != null);
		
	}
	
	@Test
	public void getWorkshopComplaintsInfo_UT() throws Exception {

		ShowWorkshopComplaintsType body = new ShowWorkshopComplaintsType();
		//Test
		ResponseEntity<Object> result = restTemplate.postForEntity(getAbsoluteURLPath("/DEU00000V/getWorkshopComplaintsInfo"), body, Object.class);
		
		//Check
		assertThat(result != null);
		assertThat(result.getStatusCodeValue() == 201);
		assertThat(result.getBody() != null);
		
	}
	
	@Test(expected = HttpServerErrorException.class)
	public void processWorkshopComplaint_UT() throws Exception {

		ProcessWorkshopComplaintType body = new ProcessWorkshopComplaintType();
		//Test
		ResponseEntity<Object> result = restTemplate.postForEntity(getAbsoluteURLPath("/DEU00000V/processWorkshopComplaint"), body, Object.class);
		
		//Check
		assertThat(result != null);
		assertThat(result.getStatusCodeValue() == 500);
		assertThat(result.getBody() != null);
		
	}
	
	private String getAbsoluteURLPath(String urlRelativePath) throws UnknownHostException {
		return "http://localhost:" + serverPort + urlRelativePath;
	}
}
