# Workshop Complaint Management Service - IA-Service Connector

The purpose of the service is to manage the customer complaints information. The operations of the service allows requesting of the existing complaint data as well as processing of new or changed complaints.

PartnerKey: `DEU74623V`

* `getWorkshopComplaints`

**Example Request**

```json
{
    "workshopComplaint": {
        "workshopOrderRef": {
            "workshopOrderUID": {
                "value": "7464401"
            },
            "dealer": {
                "dealerRef": {
                    "dealerIdentifier": {
                        "partnerKey": {
                            "country": {
                                "value": "DEU"
                            },
                            "brand": {
                                "value": "V"
                            },
                            "partnerNumber": "74623"
                        }
                    }
                }
            },
            "acceptanceDate": "2019-03-12"
        },
        "vehicleRef": {
            "vin": {
                "value": "WVWZZZ3CZ6E206295"
            }
        }
    }
}
```